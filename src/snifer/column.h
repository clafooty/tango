#ifndef COLUMN_H_
#define COLUMN_H_

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>

#include "misc.h"
#include "bam.h"
#include "misc.h"



typedef struct
{
  int position;
  char *reference_name;
  char reference_base;
  int depth;
  char *nucleotides;
  char *qualities;
  unsigned int *flags;
  unsigned int *positions_in_read;
  unsigned int *read_length;
  int depth_strandF;
  int depth_strandR;
  int count_nucleotides[ALPHABET_SIZE];
  int count_nucleotides_strandF[ALPHABET_SIZE];
  int count_nucleotides_strandR[ALPHABET_SIZE];
  char dominant_base;
  bool mismatch;
  float variation_nucleotides[ALPHABET_SIZE];
  float average_qual_nucleotides[ALPHABET_SIZE];
  float average_qual;
  int nb_positions_strandF[ALPHABET_SIZE];
  int nb_positions_strandR[ALPHABET_SIZE];
  int max_same_position_strandF[ALPHABET_SIZE];
  int max_same_position_strandR[ALPHABET_SIZE];
  int min_position_strandF[ALPHABET_SIZE];
  int min_position_strandR[ALPHABET_SIZE];
  int max_position_strandF[ALPHABET_SIZE];
  int max_position_strandR[ALPHABET_SIZE];
  float average_qual_left;
  float average_qual_right;
  float average_depth_left;
  float average_depth_right;
}column_t;


// General functions
void print_usage(char**);
void print_help(void);
void split_file_name(char*, char*);

// Structure column related functions
void make_column(column_t*, const int, char*, char, const int, char*, char*, unsigned int*, unsigned int*, unsigned int*, float, float, float, float);
void empty_column(column_t*);
void print_column(column_t*, FILE*);
void count_nucleotides(column_t*);
void count_nucleotides_strand(column_t*);
void get_dominant_base(column_t*);
void has_mismatch(column_t*);
void calculate_variation(column_t*);
void calculate_average_qual(column_t*);
void calculate_average_qual_nucleotides(column_t*);
void calculate_depth_strand(column_t*);
void count_position_read_strand(column_t*);

#endif /*COLUMN_H_*/
