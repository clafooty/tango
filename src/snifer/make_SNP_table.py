#!/usr/bin/env python

"""
Generate a file containing the alleles of all strains at all SNPs positions. If a SNP position is not covered in a strain, the allele is set to N or the SNP is discarded, depending on the -N option.
Inputs: -d Directory containing the depth of all strains at all positions
        -a Diectory containing the ambiguous bases of all strains
        -r File containing the reference sequence in fasta
        -s Directory containing the output files of the script call-snp.py (whatever version), one file per strain
        -N 'y' or 'n' depending whether positions with Ns should be considered or discarded
"""

import getopt, sys, traceback, os, re


# ---------------------------------------------------------------------------
# FUNCTIONS
# ---------------------------------------------------------------------------

# Function to print help message
def print_help():
    '''
    Print a short help message and exit.
    '''
    print '\n-h\thelp\n-d\tDirectory with files with coverage at all positions by strain\n-r\tFile containing the reference sequence in fasta\n-s\tInput directory containing one file per strain with detected SNPs\n-N\ty or n depending whether positions with Ns should be considered or discarded\n\n'
    sys.exit(1)

# Function to print usage
def print_usage(program_name):
    '''
    Print usage and exit.
    '''
    print '%s -d <depth_dir> -a <ambiguous_dir> -s <SNP_dir> -r <reference.fasta> -N <y/n> -m <min_depth>' % program_name
    sys.exit(1)


# Function to parse the command line
def parse_cmd_line(argv):

    # Initialise variables
    depth_dir = 'undef'
    reference_file = 'undef'
    snp_dir = 'undef'
    print_Ns = 'undef'

    # Get arguments or exit and print help message if it fails
    try:
        options, arguments = getopt.getopt(argv, "hd:r:s:N:", ["help", "depth_dir=", "reference_file=", "snp_directory=", "print_Ns="])
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)

    # Store arguments in variables
    for option, argument in options:
        if option in ("-h", "--help"):
            print_help()                     
        elif option in ("-d", "--depth_dir"): 
            depth_dir = argument
        elif option in ("-r", "--reference_file"): 
            reference_file = argument
        elif option in ("-s", "--snp_directory"): 
            snp_dir = argument
        elif option in ("-N", "--print_Ns"): 
            print_Ns = argument.strip()

    # Return arguments
    return ( depth_dir, reference_file, snp_dir, print_Ns )


# Function to parse the file containing the depth of the strain at all SNPs positions
def parse_fasta_file(reference_fd):

   # Declare vairables
   reference = []

   # Open and parse reference file
   for line in reference_fd:
       if not (re.search('^>', line)):
           #print reference
           #line_elts = (line.strip()).split('')
           reference.extend(list(line.strip()))
           #print reference[0]
           #print reference[10]
   # Return a list containing the reference sequence
   return (reference)



# ---------------------------------------------------------------------------
# MAIN
# ---------------------------------------------------------------------------

def main(argv=None):
      
   # Read and parse system's command line 
   depth_dir, reference_file, snp_dir, print_Ns =  parse_cmd_line( sys.argv[1:] )
   print 'argv0: %s' % sys.argv[0]
   # Check that arguments are defined and correct
   if ( (depth_dir is 'undef') or (reference_file is 'undef') or (snp_dir is 'undef') or (print_Ns is 'undef') ):
      print_usage(sys.argv[0])
      sys.exit(2)
   if not ( (print_Ns is 'y') or (print_Ns is 'n') ):
      print_usage(sys.argv[0])
      sys.exit(2)

   try:
      # Define variables.
      depth_strains = {}
      snp_positions = {}
      ref_base = {}
      all_strains = []

      # Get the reference sequence
      reference_fd = open (reference_file, 'r')
      reference = parse_fasta_file(reference_fd)

      # Go through the SNP directory
      for snp_file in os.listdir(snp_dir):
          # Process all SNP files (files with the extension 'snp')
          if (re.search('_Var.txt', snp_file)):
              # Create a list with the name of all the strains
              strain = snp_file.split('_')[0]
              #print '%s' % strain
              all_strains.append(strain)
              # Get SNPs alleles
              # Parse SNPs files
              snp_fd = open ( ('%s/%s' % (snp_dir, snp_file)), 'r' )
              for line in snp_fd:
                  line_elts = (line.strip()).split('\t')
                  # Line to uncomment for genes
                  #line_elts = (line.strip()).split(' ')
                  # Select lines containing SNPs (starting by position)
                  if (re.search('^(\d+)', line_elts[0])):
                      position = int(line_elts[0])
                      ref_base[position] = line_elts[1]
                      mutation = line_elts[2]
                      # Store strain name and mutation (as a tuple) in a dictionary
                      try:
                      ## () defines a list that can't be modified
                          (snp_positions[position]).append( (strain, mutation) )
                      except KeyError:
                          snp_positions[position] = [(strain, mutation)]


      # Go through the depth directory 
      for depth_file in os.listdir(depth_dir):
          # Process all SNP files (files with the extension 'position.txt')
          if (re.search('_position.txt', depth_file)):
              # Get the depth of all strains at each position
              depth_fd = open ( ('%s/%s' % (depth_dir, depth_file)), 'r')
              strain = depth_file.split('_')[0]
              #print '%s' % strain
              for line in depth_fd:
                  line_elts = (line.strip()).split('\t')
                  position = int(line_elts[1])
                  depth = int(line_elts[2])
                  if position in snp_positions.keys():
                      #print '%s\t%d\t%d' % ( strain, position, depth)
                      # Store strain name and mutation (as a tuple) in a dictionary
                      try:
                          ##() defines a list that can't be modified
                          (depth_strains[(strain, position)]).append( (depth) )
                      except KeyError:
                          depth_strains[(strain, position)] = [(depth)]
      


      # Print header
      sorted_all_strains = sorted(all_strains)
      print '%s\t%s\t%s' % ('position' , '\t'.join(sorted_all_strains), 'Ref')
      
      # Create dictionary containing a nucleotide for all SNPs positions
      for pos in sorted (snp_positions.keys()):
         allele = {}
         contain_Ns = 0
         # Assign the mutation to all polymorphic strains
         for value in snp_positions[pos]:
             allele[value[0]] = value[1]
         # Check all strains
         for key_strain in sorted_all_strains:
             # The strain is not polymorphic at the considered position
             if not (key_strain in  allele):
                 # The considered position is not enough covered to detect a SNP, assign a 'N'
                 if (depth_strains[(strain, pos)] < 3):
                     allele[key_strain] = 'N'
                     contain_Ns = 1
                 # The considered position is enough covered to detect a SNP, assign the reference base
                 else:
                     allele[key_strain] = ref_base[pos]

         # Print alleles
         if (print_Ns is 'y'):
             print '%d\t%s\t%s' % ( pos, '\t'.join ([ allele[key_strain] for key_strain in sorted_all_strains ]), reference[pos-1] )
         elif (print_Ns is 'n'):
             if (contain_Ns != 1):
                 print '%d\t%s\t%s' % ( pos, '\t'.join ([ allele[key_strain] for key_strain in sorted_all_strains ]), reference[pos-1] )

      snp_fd.close()
      depth_fd.close()
   except:
      traceback.print_exc()

if __name__ == "__main__":
   main ()
