#!/usr/bin/env python

"""
Describe the SNPs and return a file containing informative SNPs only
Returns numbers of: SNPs, SNPs detected in all strains, mono-allelic SNPs, bi-allelic SNPs, tri-allelic SNPs, SNPs without those detected in all strains, informative SNPs.
Input: -i Input file containing SNPs alleles of all strains
       -o Name of the output file containing informative SNPs only
"""


import getopt, sys, traceback, os, re


# ---------------------------------------------------------------------------
# FUNCTIONS
# ---------------------------------------------------------------------------

# Function to print help message
def usage():
    '''
    Print a short help message, then exit.
    '''
    print '\n-h\thelp\n-i\tInput file containing SNPs alleles of all strains\n-o\tOutput file name\n'
    sys.exit(1)


# Function to parse the command line
def parse_cmd_line(argv):
    # Initialise variable
    allele_file = 'undef'
    output_file = 'undef'
    # Get arguments or exit and print help message if it fails
    try:
        options, arguments = getopt.getopt(argv, "hi:o:", ["help", "allele_file=", "output_file="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    # Store arguments in variables
    for option, argument in options:
        if option in ("-h", "--help"):
            usage()                     
        elif option in ("-i", "--allele_file"): 
            allele_file = argument
        elif option in ("-o", "--output_file"): 
            output_file = argument
    # Return argument
    return (allele_file, output_file)


# Function to calculate the number of alleles (N is not an allele)
def calculate_alleles(allele):
    # Define variables
    uncovered = 0
    nb_alleles = 0
    # Calculate number of alleles
    for key in allele:
        if key == 'N':
            uncovered = 1
    nb_alleles = len(allele) - uncovered
    return (nb_alleles)

# Function to check whether a SNP is covered
def is_covered(allele):
    # Define variable
    covered = 1
    # Check whether the list of alleles contain Ns
    for key in allele:
        if key == 'N':
            covered = 0
    return (covered)

# Function to check whether a SNP is mono-allelic
def is_mono(nb_alleles):
    # Define variable
    mono = 0
    if nb_alleles == 1:
        mono = 1
    return (mono)

# Function to check whether a SNP is bi-allelic
def is_bi(nb_alleles):
    # Define variable
    bi = 0
    if nb_alleles == 2:
        bi = 1
    return (bi)

# Function to check whether a SNP is tri-allelic
def is_tri(nb_alleles):
    # Define variable
    tri = 0
    if nb_alleles == 3:
        tri = 1
    return (tri)

# Function to check whether a SNP is detected in all strains
def is_in_all(allele, nb_strains):
    # Define variable
    in_all = 0
    for key in allele:
        if allele[key] == nb_strains:
            in_all = 1
    return (in_all)

# Function to check whether a bi-allelic SNP is informative
def is_bi_informative(allele, nb_alleles):
    # Define variable
    informative = 1
    if nb_alleles == len(allele):
        for key in allele:
            if allele[key] == 1:
                informative = 0
    else:
        for key in allele:
            if not key == 'N':
                if allele[key] == 1:
                    informative = 0
    return (informative)

# Function to check whether a tri-allelic SNP is informative
def is_tri_informative(allele, nb_alleles):
    # Define variables
    informative = 1
    nb_mono_strain = 0
    for key in allele:
        if not key == 'N':
            if allele[key] == 1:
                nb_mono_strain += 1
    if nb_mono_strain == 2:
        informative = 0
    return (informative)


# ---------------------------------------------------------------------------
# MAIN
# ---------------------------------------------------------------------------

def main(argv=None):
      
   # Read and parse system's command line 
   allele_file, output_file =  parse_cmd_line( sys.argv[1:] )
    
   # Check that arguments are defined
   if ( (allele_file is 'undef') or (output_file is 'undef') ):
      usage()
      sys.exit(3)
      
   try:
      # Define variables.
      nb_strains = 0
      nb_snps = 0
      nb_covered_snps = 0
      index = 0
      allele = {}
      nb_alleles = 0
      nb_mono = 0
      nb_bi = 0
      nb_tri = 0
      nb_in_all = 0
      nb_bi_informative = 0
      nb_tri_informative = 0
      info_snps = {}
      
      # Open and parse input file
      allele_fd = open (allele_file, 'r')
      output_fd = open (output_file, 'w')
      for line in allele_fd:
          line_elts = (line.strip()).split('\t')
          # Count the number of strains
          if not (re.search('^(\d+)', line_elts[0])):
              output_fd.write(line)
              nb_strains = len(line_elts) - 1
          # Get SNPs alleles
          else:
              allele.clear()
              #print line
              for index in range (1, nb_strains+1):
                  if not line_elts[index] in allele:
                      allele[line_elts[index]] = 1
                  else:
                      allele[line_elts[index]] += 1

              nb_snps += 1
              nb_alleles = calculate_alleles(allele)
              nb_covered_snps += is_covered(allele)
              nb_mono += is_mono(nb_alleles)
              if is_bi(nb_alleles) == 1:
                  nb_bi += 1
                  if is_bi_informative(allele, nb_alleles) == 1:
                      nb_bi_informative += 1
                      # Write informative SNPs in output file
                      output_fd.write(line)
              if is_tri(nb_alleles) == 1:
                  nb_tri += 1
                  if is_tri_informative(allele, nb_alleles) == 1:
                      nb_tri_informative += 1
                      # Write informative SNPs in output file
                      output_fd.write(line)
              nb_in_all += is_in_all(allele, nb_strains)

      # Print results
      print 'SNPs: %d' % nb_snps
      print 'SNPs in all strains: %d' % nb_in_all
      print 'Mono-allelic SNPs: %d' % nb_mono
      print 'Real SNPs: %d' % (nb_snps - nb_in_all)
      print 'Real mono-allelic SNPs: %d' % (nb_mono - nb_in_all)
      print 'Bi-allelic SNPs: %d' % nb_bi
      print 'Tri-allelic SNPs: %d' % nb_tri
      print 'Not informative bi-allelic SNPs: %d' % (nb_bi - nb_bi_informative)
      print 'Not informative tri-allelic SNPs: %d' % ( nb_tri - nb_tri_informative)
      print 'Informative SNPs: %d' % (nb_bi_informative + nb_tri_informative)
      print 'Covered SNPs: %d' % nb_covered_snps
      
      allele_fd.close()
      output_fd.close()
   except:
      traceback.print_exc()

if __name__ == "__main__":
   main ()
