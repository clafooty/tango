#include "column.h"


// Function to prin how to use the program
void print_usage(char **argv)
{
  printf("Usage: %s -b <aln.bam> -f <ref.fasta> [-w <int>]\n", argv[0]);
}


// Function to prin how to use the program
void print_help(void)
{
  printf("\tHELP\n-b\tAlignment file in BAM format\n-f\tReference file in FASTA format\n-w\tWindow size used to calculate the average quality and depth around the mismatches\n");
}


// Function to remove extensions .bam from file name
void split_file_name(char *file_name, char *output_name)
{
  int i = strlen(file_name) - 1;
  if ( (file_name[i] == 'm') && (file_name[i-1] == 'a') && (file_name[i-2] == 'b') && (file_name[i-3] == '.') )
  {
    output_name[i-3] = '\0';
  }
  else
  {
    fprintf(stderr, "error: input file name %s has not an appropriate extension (.bam)\n", file_name);
    exit(8);
  }
}


// Function to initialise a structure column
void make_column(column_t *col, const int position, char *target, char reference_base, const int depth, char *all_bases, char *all_qual, unsigned int *all_flag, unsigned int *all_position_in_read, unsigned int *all_read_length, float average_qual_left, float average_qual_right, float average_depth_left, float average_depth_right)
{
  col->position = position;
  col->reference_name = strdup(target);
  col->reference_base = reference_base;
  col->depth = depth;
  col->nucleotides = all_bases;
  col->qualities = all_qual;
  col->flags = all_flag;
  col->positions_in_read = all_position_in_read;
  col->read_length = all_read_length;
  col->depth_strandF = 0;
  col->depth_strandR = 0;
  memset(col->count_nucleotides, 0, sizeof(col->count_nucleotides));
  memset(col->count_nucleotides_strandF, 0, sizeof(col->count_nucleotides_strandF));
  memset(col->count_nucleotides_strandR, 0, sizeof(col->count_nucleotides_strandR));
  col->dominant_base = ' ';
  col->mismatch = 0;
  memset(col->variation_nucleotides, 0, sizeof(col->variation_nucleotides));
  memset(col->average_qual_nucleotides, 0, sizeof(col->average_qual_nucleotides));
  col->average_qual = 0;
  memset(col->nb_positions_strandF, 0, sizeof(col->nb_positions_strandF));
  memset(col->nb_positions_strandR, 0, sizeof(col->nb_positions_strandR));
  memset(col->max_same_position_strandF, 0, sizeof(col->max_same_position_strandF));
  memset(col->max_same_position_strandR, 0, sizeof(col->max_same_position_strandR));
  memset(col->min_position_strandF, 0, sizeof(col->min_position_strandF));
  memset(col->min_position_strandR, 0, sizeof(col->min_position_strandR));
  memset(col->max_position_strandF, 0, sizeof(col->max_position_strandF));
  memset(col->max_position_strandR, 0, sizeof(col->max_position_strandR));
  col->average_qual_left = average_qual_left;
  col->average_qual_right = average_qual_right;
  col->average_depth_left = average_depth_left;
  col->average_depth_right = average_depth_right;
}

// Function to empty a structure column
void empty_column(column_t *col)
{
  col->position = 0;
  if (col->reference_name != NULL)
    free(col->reference_name);
  col->reference_name = NULL;
  col->reference_base = ' ';
  col->depth = 0;
  if (col->nucleotides != NULL)
    free(col->nucleotides);
  col->nucleotides = NULL;
  if (col->qualities != NULL)
    free(col->qualities);
  col->qualities = NULL;
  if (col->flags != NULL)
    free(col->flags);
  col->flags = NULL;
  if (col->positions_in_read != NULL)
    free(col->positions_in_read);
  col->positions_in_read = NULL;
  if (col->read_length != NULL)
    free(col->read_length);
  col->read_length = NULL;
  col->depth_strandF = 0;
  col->depth_strandR = 0;
  memset(col->count_nucleotides, 0, sizeof(col->count_nucleotides));
  memset(col->count_nucleotides_strandF, 0, sizeof(col->count_nucleotides_strandF));
  memset(col->count_nucleotides_strandR, 0, sizeof(col->count_nucleotides_strandR));
  col->dominant_base = ' ';
  col->mismatch = 0;
  memset(col->variation_nucleotides, 0, sizeof(col->variation_nucleotides));
  memset(col->average_qual_nucleotides, 0, sizeof(col->average_qual_nucleotides));
  col->average_qual = 0;
  memset(col->nb_positions_strandF, 0, sizeof(col->nb_positions_strandF));
  memset(col->nb_positions_strandR, 0, sizeof(col->nb_positions_strandR));
  memset(col->max_same_position_strandF, 0, sizeof(col->max_same_position_strandF));
  memset(col->max_same_position_strandR, 0, sizeof(col->max_same_position_strandR));
  memset(col->min_position_strandF, 0, sizeof(col->min_position_strandF));
  memset(col->min_position_strandR, 0, sizeof(col->min_position_strandR));
  memset(col->max_position_strandF, 0, sizeof(col->max_position_strandF));
  memset(col->max_position_strandR, 0, sizeof(col->max_position_strandR));
  col->average_qual_left = 0;
  col->average_qual_right = 0;
  col->average_depth_left = 0;
  col->average_depth_right = 0;
}


// Function to print information on the column
void print_column(column_t *col, FILE *fd)
{
  fprintf(fd, "%s\t", col->reference_name);
  fprintf(fd, "%d\t", col->position);
  fprintf(fd, "%d\t", col->depth);
  fprintf(fd, "%d\t%d\t", col->depth_strandF, col->depth_strandR);
  fprintf(fd, "%c\t", col->reference_base);
  //printf("%s\t", col->nucleotides);
  //printf("%s\t", col->qualities);
  fprintf(fd, "%c\t", col->dominant_base);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->count_nucleotides[convert_char_index['A']], col->count_nucleotides[convert_char_index['T']], col->count_nucleotides[convert_char_index['G']], col->count_nucleotides[convert_char_index['C']], col->count_nucleotides[convert_char_index['N']]);
  fprintf(fd, "%.2f:%.2f:%.2f:%.2f:%.2f\t", col->variation_nucleotides[convert_char_index['A']], col->variation_nucleotides[convert_char_index['T']], col->variation_nucleotides[convert_char_index['G']], col->variation_nucleotides[convert_char_index['C']], col->variation_nucleotides[convert_char_index['N']]);
  fprintf(fd, "%.2f:%.2f:%.2f:%.2f:%.2f\t", col->average_qual_nucleotides[convert_char_index['A']], col->average_qual_nucleotides[convert_char_index['T']], col->average_qual_nucleotides[convert_char_index['G']], col->average_qual_nucleotides[convert_char_index['C']], col->average_qual_nucleotides[convert_char_index['N']]);
  fprintf(fd, "%.2f\t", col->average_qual);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->count_nucleotides_strandF[convert_char_index['A']], col->count_nucleotides_strandF[convert_char_index['T']], col->count_nucleotides_strandF[convert_char_index['G']], col->count_nucleotides_strandF[convert_char_index['C']], col->count_nucleotides_strandF[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->max_position_strandF[convert_char_index['A']], col->max_position_strandF[convert_char_index['T']], col->max_position_strandF[convert_char_index['G']], col->max_position_strandF[convert_char_index['C']], col->max_position_strandF[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->min_position_strandF[convert_char_index['A']], col->min_position_strandF[convert_char_index['T']], col->min_position_strandF[convert_char_index['G']], col->min_position_strandF[convert_char_index['C']], col->min_position_strandF[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->nb_positions_strandF[convert_char_index['A']], col->nb_positions_strandF[convert_char_index['T']], col->nb_positions_strandF[convert_char_index['G']], col->nb_positions_strandF[convert_char_index['C']], col->nb_positions_strandF[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->max_same_position_strandF[convert_char_index['A']], col->max_same_position_strandF[convert_char_index['T']], col->max_same_position_strandF[convert_char_index['G']], col->max_same_position_strandF[convert_char_index['C']], col->max_same_position_strandF[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->count_nucleotides_strandR[convert_char_index['A']], col->count_nucleotides_strandR[convert_char_index['T']], col->count_nucleotides_strandR[convert_char_index['G']], col->count_nucleotides_strandR[convert_char_index['C']], col->count_nucleotides_strandR[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->max_position_strandR[convert_char_index['A']], col->max_position_strandR[convert_char_index['T']], col->max_position_strandR[convert_char_index['G']], col->max_position_strandR[convert_char_index['C']], col->max_position_strandR[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->min_position_strandR[convert_char_index['A']], col->min_position_strandR[convert_char_index['T']], col->min_position_strandR[convert_char_index['G']], col->min_position_strandR[convert_char_index['C']], col->min_position_strandR[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->nb_positions_strandR[convert_char_index['A']], col->nb_positions_strandR[convert_char_index['T']], col->nb_positions_strandR[convert_char_index['G']], col->nb_positions_strandR[convert_char_index['C']], col->nb_positions_strandR[convert_char_index['N']]);
  fprintf(fd, "%d:%d:%d:%d:%d\t", col->max_same_position_strandR[convert_char_index['A']], col->max_same_position_strandR[convert_char_index['T']], col->max_same_position_strandR[convert_char_index['G']], col->max_same_position_strandR[convert_char_index['C']], col->max_same_position_strandR[convert_char_index['N']]);
  fprintf(fd, "%.2f:%.2f\t%.2f:%.2f\n", col->average_qual_left, col->average_qual_right, col->average_depth_left, col->average_depth_right);
}


// Function to count the number of A, T, G, C and N
void count_nucleotides(column_t *col)
{
  int i = 0;
  for (i = 0; i < col->depth; i++)
  {
    if (col->nucleotides[i] == 'A')
      col->count_nucleotides[convert_char_index['A']]++;
    if (col->nucleotides[i] == 'T')
      col->count_nucleotides[convert_char_index['T']]++;
    if (col->nucleotides[i] == 'G')
      col->count_nucleotides[convert_char_index['G']]++;
    if (col->nucleotides[i] == 'C')
      col->count_nucleotides[convert_char_index['C']]++;
    if (col->nucleotides[i] == 'N')
      col->count_nucleotides[convert_char_index['N']]++;
  }
}


// Function to count the number of A, T, G, C and N by strand
void count_nucleotides_strand(column_t *col)
{
  int i = 0;
  for (i = 0; i < col->depth; i++)
  {
    if (col->nucleotides[i] == 'A')
    {
      if (col->flags[i] & BAM_FREVERSE)
	col->count_nucleotides_strandR[convert_char_index['A']]++;
      else
	col->count_nucleotides_strandF[convert_char_index['A']]++;
    }
    if (col->nucleotides[i] == 'T')
    {
      if (col->flags[i] & BAM_FREVERSE)
	col->count_nucleotides_strandR[convert_char_index['T']]++;
      else
	col->count_nucleotides_strandF[convert_char_index['T']]++;
    }
    if (col->nucleotides[i] == 'G')
    {
      if (col->flags[i] & BAM_FREVERSE)
	col->count_nucleotides_strandR[convert_char_index['G']]++;
      else
	col->count_nucleotides_strandF[convert_char_index['G']]++;
    }
    if (col->nucleotides[i] == 'C')
    {
      if (col->flags[i] & BAM_FREVERSE)
	col->count_nucleotides_strandR[convert_char_index['C']]++;
      else
	col->count_nucleotides_strandF[convert_char_index['C']]++;
    }
    if (col->nucleotides[i] == 'N')
    {
      if (col->flags[i] & BAM_FREVERSE)
	col->count_nucleotides_strandR[convert_char_index['N']]++;
      else
	col->count_nucleotides_strandF[convert_char_index['N']]++;
    }
  }
}


// Function to get the dominant base
void get_dominant_base(column_t *col)
{
  col->dominant_base = convert_index_char[get_maximum_index(col->count_nucleotides, ALPHABET_SIZE)];
}


// Function to check whether there is a mismatch in the column
void has_mismatch(column_t *col)
{
  int i = 0;
  for (i = 0; i < ALPHABET_SIZE; i++)
  {
    if ((convert_index_char[i] != col->reference_base) && (col->count_nucleotides[i] != 0))
      col->mismatch = 1;
  }
}


// Function to calculate the variation of each nucleotide (occurrence / depth)
void calculate_variation(column_t *col)
{
  int i = 0;
  for (i = 0; i < ALPHABET_SIZE; i++)
  {
    if (col->depth != 0)
      col->variation_nucleotides[i] = (float)col->count_nucleotides[i] / (float)col->depth;
    else
      col->variation_nucleotides[i] = 0;
  }
}


// Function to calculate the average quality
void calculate_average_qual(column_t *col)
{
  int i = 0;
  for (i = 0; i < col->depth; i++)
  {
    col->average_qual += PHRED_QUAL(col->qualities[i]);
  }
  if (col->depth != 0)
    col->average_qual = col->average_qual / (float)col->depth;
  else
    col->average_qual = 0;
}


// Function to calculate the average quality of each nucleotide
void calculate_average_qual_nucleotides(column_t *col)
{
  int i = 0;
  int j = 0;
  for (i = 0; i < col->depth; i++)
  {
    if (col->nucleotides[i] == 'A')
      col->average_qual_nucleotides[convert_char_index['A']] += PHRED_QUAL(col->qualities[i]);
    if (col->nucleotides[i] == 'T')
      col->average_qual_nucleotides[convert_char_index['T']] += PHRED_QUAL(col->qualities[i]);
    if (col->nucleotides[i] == 'G')
      col->average_qual_nucleotides[convert_char_index['G']] += PHRED_QUAL(col->qualities[i]);
    if (col->nucleotides[i] == 'C')
      col->average_qual_nucleotides[convert_char_index['C']] += PHRED_QUAL(col->qualities[i]);
    if (col->nucleotides[i] == 'N')
      col->average_qual_nucleotides[convert_char_index['N']] += PHRED_QUAL(col->qualities[i]);
  }
  for (j = 0; j < ALPHABET_SIZE; j++)
  {
    if (col->count_nucleotides[j] != 0)
      col->average_qual_nucleotides[j] = col->average_qual_nucleotides[j] / (float)col->count_nucleotides[j];
    else
      col->average_qual_nucleotides[j] = 0;
  }
}


// Function to calculate the depth by strand
void calculate_depth_strand(column_t *col)
{
  int i = 0;
  for (i = 0; i < col->depth; i++)
  {
    if (col->flags[i] & BAM_FREVERSE)
      col->depth_strandR++;
    else
      col->depth_strandF++;   
  }
}


// Function to calculate the number of positions, minimum and maximum positions at which each nucleotides occurs
void count_position_read_strand(column_t *col)
{
  int i = 0;
  int j = 0;
  int *positions_strandF[ALPHABET_SIZE] = {0};
  for (j = 0; j < ALPHABET_SIZE; j++)
  {
    positions_strandF[j] = calloc((col->depth), sizeof(int));
    if (positions_strandF[j] == NULL)
    {
      fprintf(stderr, "Out of memory");
      exit(2);
    }
  }
  int *positions_strandR[ALPHABET_SIZE];
  for (j = 0; j < ALPHABET_SIZE; j++)
  {
    positions_strandR[j] = calloc((col->depth), sizeof(int));
    if (positions_strandR[j] == NULL)
    {
      fprintf(stderr, "Out of memory");
      exit(2);
    }
  }
  int i_strandF[ALPHABET_SIZE] = {0};
  int i_strandR[ALPHABET_SIZE] = {0};

  // Fill in arrays of positions for each nucleotide
  for (i = 0; i < col->depth; i++)
  {
    // Nucleotide A
    if (col->nucleotides[i] == 'A')
    {
      if (col->flags[i] & BAM_FREVERSE)
      {
	positions_strandR[convert_char_index['A']][i_strandR[convert_char_index['A']]] = col->read_length[i] - col->positions_in_read[i];
	i_strandR[convert_char_index['A']]++;
      }
      else
      {
	positions_strandF[convert_char_index['A']][i_strandF[convert_char_index['A']]] = col->positions_in_read[i] + 1;
	i_strandF[convert_char_index['A']]++;
      }
    }
    // Nucleotide T
    if (col->nucleotides[i] == 'T')
    {
      if (col->flags[i] & BAM_FREVERSE)
      {
	positions_strandR[convert_char_index['T']][i_strandR[convert_char_index['T']]] = col->read_length[i] - col->positions_in_read[i];
	i_strandR[convert_char_index['T']]++;
      }
      else
      {
	positions_strandF[convert_char_index['T']][i_strandF[convert_char_index['T']]] = col->positions_in_read[i] + 1;
	i_strandF[convert_char_index['T']]++;
      }
    }
    // Nucleotide G
    if (col->nucleotides[i] == 'G')
    {
      if (col->flags[i] & BAM_FREVERSE)
      {
	positions_strandR[convert_char_index['G']][i_strandR[convert_char_index['G']]] = col->read_length[i] - col->positions_in_read[i];
	i_strandR[convert_char_index['G']]++;
      }
      else
      {
	positions_strandF[convert_char_index['G']][i_strandF[convert_char_index['G']]] = col->positions_in_read[i] + 1;
	i_strandF[convert_char_index['G']]++;
      }
    }
    // Nucleotide C
    if (col->nucleotides[i] == 'C')
    {
      if (col->flags[i] & BAM_FREVERSE)
      {
	positions_strandR[convert_char_index['C']][i_strandR[convert_char_index['C']]] = col->read_length[i] - col->positions_in_read[i];
	i_strandR[convert_char_index['C']]++;
      }
      else
      {
	positions_strandF[convert_char_index['C']][i_strandF[convert_char_index['C']]] = col->positions_in_read[i] + 1;
	i_strandF[convert_char_index['C']]++;
      }
    }
    // Nucleotide N
    if (col->nucleotides[i] == 'N')
    {
      if (col->flags[i] & BAM_FREVERSE)
      {
	positions_strandR[convert_char_index['N']][i_strandR[convert_char_index['N']]] = col->read_length[i] - col->positions_in_read[i];
	i_strandR[convert_char_index['N']]++;
      }
      else
      {
	positions_strandF[convert_char_index['N']][i_strandF[convert_char_index['N']]] = col->positions_in_read[i] + 1;
	i_strandF[convert_char_index['N']]++;
      }
    }
  }
  // Calculate all values related to the positions of the nucleotides in the reads
  for (j = 0; j < ALPHABET_SIZE; j++)
  {
    // Get minimum and maximum positions for each nucleotide on each strand
    col->max_position_strandF[j] = get_maximum(positions_strandF[j], col->depth);
    col->max_position_strandR[j] = get_maximum(positions_strandR[j], col->depth);
    col->min_position_strandF[j] = get_minimum(positions_strandF[j], col->depth);
    col->min_position_strandR[j] = get_minimum(positions_strandR[j], col->depth);
    // Get number of positions in reads and maximum occurrence
    col->nb_positions_strandF[j] = count_unique_elements(positions_strandF[j], col->depth);
    col->nb_positions_strandR[j] = count_unique_elements(positions_strandR[j], col->depth);
    col->max_same_position_strandF[j] = count_maximal_occurrence(positions_strandF[j], col->depth);
    col->max_same_position_strandR[j] = count_maximal_occurrence(positions_strandR[j], col->depth);
  }
  // Free memory
  for (j = 0; j < ALPHABET_SIZE; j++)
  {
    if (positions_strandF[j] != NULL)
      free(positions_strandF[j]);
    if (positions_strandR[j] != NULL)
      free(positions_strandR[j]);
  }
}
