/**
 * snifer.c : simple tools for annotating variants. 
 * CMD : gcc -m64 -I path/to/samtools -L path/to/samtools snifer_9.c misc.c column.c -lbam -lz
 * gcc -m64 -I /Users/stef/pavane/bioports/src/samtools_0_1_12a/  -L/Users/stef/pavane/bioports/src/samtools_0_1_12a/  snifer.c -lbam -lz -lm
 * gcc -Wall -m64 -lbam -lz -lm -Isamtools_dir  -Lsamtools_dir -o snifer7 snifer_7.c misc.c column.c 
 */

#include <stdlib.h> 
#include <stdarg.h>
#include <getopt.h>
#include <time.h>

#include "bam.h"
#include "sam.h"
#include "faidx.h"
#include "column.h"
#include "misc.h"


typedef struct
{
  int beg, end;
  samfile_t *in;
  faidx_t *fai;
  char *ref; 
  int tid; 
  int len;
  int window_size;
  int **qualities;
  int **depths;
  int next_position;
  FILE *mismatch_fd;
  FILE *depth_fd;
} pileup_struct_t;



/* Function to calculate the sum of qualities and the depth at each position */
static int pileup_quality_depth_func(uint32_t tid, uint32_t pos, int n, const bam_pileup1_t *pl, void *data)
{
  int i = 0;
  int qual = 0;
  pileup_struct_t *pileup = (pileup_struct_t*)data;
  char *target = pileup->in->header->target_name[tid];

  // Update pileup->ref if necessary
  if (pileup->fai && (int)tid != pileup->tid)
  {
    if (pileup->ref != NULL)
    {
      free(pileup->ref);
    }
    pileup->ref = faidx_fetch_seq(pileup->fai, target, pileup->beg, pileup->end, &pileup->len);
    pileup->tid = tid;
  }

  // Go through all the reads aligned at the pileup position
  for (i = 0; i < n; ++i)
  {
    // Get structure representing the alignement covering the pileup position
    const bam_pileup1_t *p = pl + i;
    // Get the quality of  all bases aligned at the pileup position
    qual = bam1_qual(p->b)[p->qpos];
    if (qual > 126)
      qual = 126;
    pileup->qualities[tid][pos] += qual;
  }
  pileup->depths[tid][pos] = n;
  return 0;
}




/* Function to look for mismatches and display many information about them */
static int pileup_mismatch_func(uint32_t tid, uint32_t pos, int n, const bam_pileup1_t *pl, void *data)
{
  int i = 0;
  int j = 0;
  int base = 0;
  int qual = 0;
  float average_qual_left = 0;
  float average_qual_right = 0;
  float average_depth_left = 0;
  float average_depth_right = 0;
  pileup_struct_t *pileup = (pileup_struct_t*)data;
  char *target = pileup->in->header->target_name[tid];
  column_t col;
  // Declare pointers and check memory allocation
  char *all_bases = NULL;
  all_bases = (char*)malloc((n + 1)*sizeof(char));
  check_memory_allocation(all_bases);
  char *all_qual = NULL;
  all_qual = (char*)malloc((n + 1)*sizeof(char));
  check_memory_allocation(all_qual);
  unsigned int *all_flag = NULL;
  all_flag = (unsigned int*)malloc(n*sizeof(unsigned int));
  check_memory_allocation(all_flag);
  unsigned int *all_position_in_read = NULL;
  all_position_in_read = (unsigned int*)malloc(n*sizeof(unsigned int));
  check_memory_allocation(all_position_in_read);
  unsigned int *all_read_length = NULL;
  all_read_length = (unsigned int*)malloc(n*sizeof(unsigned int));
  check_memory_allocation(all_read_length);
  
  // Update pileup->ref if necessary
  if (pileup->fai && (int)tid != pileup->tid)
  {
    if (pileup->ref != NULL)
    {
      free(pileup->ref);
    }
    pileup->ref = faidx_fetch_seq(pileup->fai, target, pileup->beg, pileup->end, &pileup->len);
    pileup->tid = tid;
  }
  
  // Go through all the reads aligned at the pileup position
  for (i = 0; i < n; ++i)
  {
    // Get structure representing the alignement covering the pileup position
    const bam_pileup1_t *p = pl + i;
    // Get all bases aligned at the pileup position
    base = bam_nt16_rev_table[bam1_seqi(bam1_seq(p->b), p->qpos)];
    all_bases[i] = base;
    // Get the quality of  all bases aligned at the pileup position
    qual = bam1_qual(p->b)[p->qpos] + CONVERT_PHRED;
    if (qual > 126)
      qual = 126;
    all_qual[i] = qual;
    // Get flag for each read
    all_flag[i] = p->b->core.flag;
    all_position_in_read[i] = p->qpos;
    // Get read length
    all_read_length[i] = p->b->core.l_qseq;
  }
  all_bases[n] = '\0';
  all_qual[n] = '\0';

  // Calculate average quality and depth surrounding the position
  if (pileup->window_size != 0)
  {
    // Left side of the position
    if ((pos - pileup->window_size) > 0)
    {
      for (j = (pos - pileup->window_size); j < pos; j++)
      {
	average_qual_left += pileup->qualities[tid][j];
	average_depth_left += pileup->depths[tid][j];
      }
    }
    else
    {
      for (j = 0; j < pos; j++)
      {
	average_qual_left += pileup->qualities[tid][j];
	average_depth_left += pileup->depths[tid][j];
      }
    }
    if (average_depth_left != 0)
    {
      average_qual_left = average_qual_left / average_depth_left;
    }
    else
    {
      average_qual_left = 0;
    }
    average_depth_left = average_depth_left / pileup->window_size;
    // Right side of the position
    if ((pos + pileup->window_size + 1) < (pileup->in->header->target_len[tid]))
    {
      for (j = (pos + 1); j < (pos + pileup->window_size + 1); j++)
      {
	average_qual_right += pileup->qualities[tid][j];
	average_depth_right += pileup->depths[tid][j];
      }
    }
    else
    {
      for (j = (pos + 1); j < (pileup->in->header->target_len[tid]); j++)
      {
	if (pileup->qualities[tid][j] != 0)
	  average_qual_right += pileup->qualities[tid][j];
	average_depth_right += pileup->depths[tid][j];
      }
    }
    if (average_depth_right != 0)
    {
      average_qual_right = average_qual_right / average_depth_right;
    }
    else
    {
      average_qual_right = 0;
    }
    average_depth_right = average_depth_right / pileup->window_size;
  }
  // Create a structure for each position where reads are aligned
  make_column(&col, pos+1, target, pileup->ref[pos], n, all_bases, all_qual, all_flag, all_position_in_read, all_read_length, average_qual_left, average_qual_right, average_depth_left, average_depth_right);
  count_nucleotides(&col);
  get_dominant_base(&col);
  has_mismatch(&col);
  // Calculate information if there is a mismatch in the column
  if (col.mismatch == 1)
  {
    count_nucleotides_strand(&col);
    calculate_variation(&col);
    calculate_average_qual(&col);
    calculate_average_qual_nucleotides(&col);
    calculate_depth_strand(&col);
    count_position_read_strand(&col);
    print_column(&col, pileup->mismatch_fd);
  }
  
  // Print depth in output file
  if (pos == pileup->next_position)
  {
    fprintf(pileup->depth_fd, "%s\t%d\t%d\n", target, pos + 1, n);
  }
  else
  {
    for (i = pileup->next_position; i < pos; i++)
    {
      fprintf(pileup->depth_fd, "%s\t%d\t0\n", target, i + 1);
    }
    fprintf(pileup->depth_fd, "%s\t%d\t%d\n", target, pos + 1, n);
  }
  pileup->next_position = pos + 1;
  // Free memory
  empty_column(&col);
  return 0;
}




/* Main function */
int main( int argc, char *argv[] )
{
  time_t t0 = time (NULL);
  char *fname_bam = NULL; 
  char *fname_fasta = NULL;
  char *file_name = NULL;
  char *mismatch_file = NULL;
  char *depth_file = NULL;
  pileup_struct_t pileup;
  int option = 0;
  int i = 0;
  int window_size = 0;
  const size_t BSZ = 1024*32;

  // Get options from command line, check their values and set output file name
  while ((option = getopt(argc,argv,"hb:f:w:")) != -1)
  {
    switch (option)
    {
      case 'h':
	print_help();
	return(0);
      case 'b':
	fname_bam = optarg;
	break;
      case 'f':
	fname_fasta = optarg;
	break;
      case 'w':
	window_size = atoi(optarg);
	break;
      case '?':
	print_usage(argv);
	return(-1);
    }
  }

  // Check that input files are provided
  if ((fname_bam == NULL) || (fname_fasta == NULL))
  {
    fprintf(stderr, "error: Files are required as input\n");
    print_usage(argv);
    exit(1);
  }
 
  // Initialisation of pileup structure
  pileup.beg = 0;
  pileup.end = 0x7fffffff;
  pileup.in  = samopen(fname_bam , "rb", 0 );
  if (pileup.in == 0)
  {
    fprintf(stderr, "Fail to open BAM file %s\n", fname_bam);
    return 2;
  }
  if (pileup.in->header == 0)
  {
    fprintf(stderr, "Fail to read the bam header: non-exisiting file or wrong format.\n");
    return 3;
  }
  pileup.fai = fai_load(fname_fasta);
  if (pileup.fai == 0)
  {
    fprintf(stderr, "Fail to open FASTA file %s\n", fname_fasta);
    return 4;
  }
  pileup.ref = NULL; 
  pileup.tid = -1;
  pileup.len = -1;
  pileup.window_size = window_size;
  pileup.qualities = (int**)calloc(pileup.in->header->n_targets, sizeof(int*));
  check_memory_allocation(pileup.qualities);
  pileup.depths = (int**)calloc(pileup.in->header->n_targets, sizeof(int*));
  check_memory_allocation(pileup.depths);
  for (i = 0; i < pileup.in->header->n_targets; i++)
  {
    pileup.qualities[i] = (int*)calloc(pileup.in->header->target_len[i], sizeof(int));
    check_memory_allocation(pileup.qualities[i]);
    pileup.depths[i] = (int*)calloc(pileup.in->header->target_len[i], sizeof(int));
    check_memory_allocation(pileup.depths[i]);
  }
  pileup.next_position = 0;
  // Set output files names and open them
  file_name = (char*)(malloc(strlen(fname_bam) + 1));
  check_memory_allocation(file_name);
  strcpy(file_name, fname_bam);
  split_file_name(fname_bam, file_name);
  mismatch_file = (char*)(malloc(strlen(file_name) + 19));
  check_memory_allocation(mismatch_file);
  sprintf(mismatch_file, "%s_mismatch.txt", file_name);
  pileup.mismatch_fd = fopen(mismatch_file, "w");
  if (!pileup.mismatch_fd)
  {
    fprintf(stderr, "error: Fail to open output file %s\n", mismatch_file);
    return 5;
  }
  fprintf(pileup.mismatch_fd, "#Reference\tPosition\tDepth\tDepth_strandF\tDepth_strandR\tBase\tCalled_base\tDepth_by_base_A:T:G:C:N\tVariation_by_base_A:T:G:C:N\tQuality_by_base_A:T:G:C:N\tAverage_quality\tDepth_by_base_strandF_A:T:G:C:N\tMaximal_position_strandF_A:T:G:C:N\tMinimal_position_strandF_A:T:G:C:N\tNb_position_strandF_A:T:G:C:N\tMaximal_same_position_strandF_A:T:G:C:N\tDepth_by_base_strandR_A:T:G:C:N\tMaximal_position_strandR_A:T:G:C:N\tMinimal_position_strandR_A:T:G:C:N\tNb_position_strandR_A:T:G:C:N\tMaximal_same_position_strandR_A:T:G:C:N\tSurrounds_average_quality_Right:Left\tSurrounds_average_depth_Right:Left\n");
  setvbuf(pileup.mismatch_fd, 0, _IOFBF, BSZ);
  depth_file = (char*)(malloc(strlen(file_name) + 11));
  check_memory_allocation(depth_file);
  sprintf(depth_file, "%s_depth.txt", file_name);
  pileup.depth_fd = fopen(depth_file, "w");
  if (!pileup.depth_fd)
  {
    fprintf(stderr, "error: Fail to open output file %s\n", depth_file);
    return 6;
  }
  
  
  if (window_size != 0)
  {
    // Get sum of all qualities and depth at each position of each reference
    sampileup(pileup.in, -1, pileup_quality_depth_func, &pileup);  

    // Re-initialisation of pileup structure
    pileup.beg = 0;
    pileup.end = 0x7fffffff;
    samclose(pileup.in);
    pileup.in  = samopen( fname_bam , "rb", 0 );
    if (pileup.in == 0)
    {
      fprintf(stderr, "Fail to open BAM file %s\n", fname_bam);
      return 2;
    }
    if (pileup.in->header == 0)
    {
      fprintf(stderr, "Fail to read the bam header: non-exisiting file or wrong format.\n");
      return 3;
    }
    fai_destroy(pileup.fai);
    pileup.fai = fai_load(fname_fasta);
    if (pileup.fai == 0)
    {
      fprintf(stderr, "Fail to open FASTA file %s\n", fname_fasta);
      return 4;
    }
    free(pileup.ref);
    pileup.ref = NULL; 
    pileup.tid = -1; 
    pileup.len = -1;
    pileup.window_size = window_size;
  }
  
  // Look for mismatches
  sampileup(pileup.in, -1, pileup_mismatch_func, &pileup);

  // Free memory
  free(file_name);
  free(mismatch_file);
  free(depth_file);
  for (i = 0; i < pileup.in->header->n_targets; i++)
  {
    if (pileup.qualities[i] != NULL)
      free(pileup.qualities[i]);
    if (pileup.depths[i] != NULL)
      free(pileup.depths[i]);
  }
  if (pileup.qualities != NULL)
    free(pileup.qualities);
  if (pileup.depths != NULL)
    free(pileup.depths);
  samclose(pileup.in);
  fai_destroy(pileup.fai);
  free(pileup.ref);

  time_t t1 = time (NULL);
  printf ("It took %.2lf seconds to compute \n", difftime(t1,t0));
  
  return 0; 
}
