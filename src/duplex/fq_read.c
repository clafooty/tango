/*
 * fq_read.cpp
 *
 *  Created on: 4 juil. 2011
 *      Author: loic
 */

#include "fq_read.h"

inline fq_read * fq_read_create(const char* head,const char *seq,const char *qual){

	fq_read * fqr = (fq_read*) malloc(sizeof(fq_read));
	if (! fqr)
		exit(1);

	fqr->h = strdup(head);
	fqr->s = strdup(seq);
	fqr->q = strdup(qual);
	fqr->copy_number = 0;
	if ( !fqr->h || !fqr->s || !fqr->q)
		exit(1);
	return fqr;
}

inline void fq_read_destroy(fq_read *fqr){
	if (! fqr) return;
	free(fqr->h);free(fqr->s);free(fqr->q);
	free(fqr);
}
