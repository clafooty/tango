/*
 * quadruplex.c
 *
 *  Created on: 12 decembre 2011
 *      Author: loic
 */

#include <zlib.h>
#include <stdio.h>
#include <stdlib.h>
#include "kseq.h"
//#include <time.h>
#include "conf.h"
#include <unistd.h>

#define CONCAT_FQ "temp_concat.fq.gz"
#define HSEP "@"
#define SSEP ";"
#define FQ_ONE "_1.fq.gz"
#define FQ_TWO "_2.fq.gz"

//#define DUPLEX_DIR "toto"

KSEQ_INIT(gzFile, gzread)

void printUsage(char **argv);
inline char *str_sub (const char *s, unsigned int start, unsigned int end);

double error_rate = ERROR_RATE;

int main(int argc, char **argv)
{
  //time_t t0 = time (NULL);
  
  //---------------------------------------------------------------------------
  // Manage options
  //---------------------------------------------------------------------------
  int option = 0;
  char *input_file_1 = NULL;
  char *input_file_2 = NULL;
  int compressLvl = 0;
    
  while ((option = getopt (argc, argv, "h1:2:z:e:")) != -1) {
    switch (option) {
	
    case 'h':
      printUsage(argv);
      break;

    case '1':
      input_file_1 = optarg;
      break;

    case '2':
      input_file_2 = optarg;
      break;
          
    case 'z':
      compressLvl = atoi(optarg);
      if (! ( (compressLvl == -1) || 
	      ((compressLvl >= 0) && (compressLvl <= 9)) )) {
	fprintf (stderr, "ERROR: Option -z %d requires an argument between [-1:9].\n", compressLvl);
	exit(1);
      }
      break;
      
    case 'e':
      error_rate = atof(optarg);
      if (!  ((error_rate > 0) && (error_rate < 1)) ) {
	fprintf (stderr, "ERROR: Option -z %f requires an argument between ]0:1[.\n", error_rate);
      }
      break;
      
    case '?':
      if (optopt == 'i')
	fprintf (stderr, "ERROR: Option -%c requires an argument.\n", optopt);
      else if (isprint (optopt))
	fprintf (stderr, "ERROR: Unknown option `-%c'.\n", optopt);
      else
	fprintf (stderr, "ERROR: Unknown option character `\\x%x'.\n", optopt);
      return 1;
      
    default:
      abort ();
    }
  }
    
  if (input_file_1 == NULL) {
    fprintf(stderr, "ERROR: Files are required as input\n");
    printUsage(argv);
    exit(1);
  }
  if (input_file_2 == NULL) {
    fprintf(stderr, "ERROR: Files are required as input\n");
    printUsage(argv);
    exit(1);
  }
  //---------------------------------------------------------------------------
  // Preprocessing
  //---------------------------------------------------------------------------
  printf("Preprocessing...\n");
  gzFile fp_1 = gzopen(input_file_1, "r");

  if (fp_1 == NULL) {
    fprintf(stderr, "ERROR: Fail to open input file: %s.\n", input_file_1);
    exit(1);
  }

  gzFile fp_2 = gzopen(input_file_2, "r");

  if (fp_2 == NULL) {
    fprintf(stderr, "ERROR: Fail to open input file: %s.\n", input_file_2);
    exit(1);
  }


  kseq_t *seq_1 = kseq_init(fp_1);
  kseq_t *seq_2 = kseq_init(fp_2);

  int l, m;
  int read_count = 0; 

  char mode[4];
  sprintf (mode, "w%d", compressLvl);

  gzFile *concat_fq = gzopen(CONCAT_FQ, mode);
  if (!concat_fq){ perror(CONCAT_FQ); exit(1);}

  while ((l = kseq_read(seq_1)) >= 0) {
    m = kseq_read(seq_2);
    
    if (! (m >=0)){
      fprintf(stderr, "ERROR: Not same number of reads in files %s and %s\n", input_file_1, input_file_2);
      exit(1);
    }

    ++read_count;
    
    //HEADER
    gzputc(concat_fq,'@'); 
    gzputs(concat_fq,seq_1->name.s); 
    gzputs(concat_fq, HSEP);
    gzputs(concat_fq,seq_2->name.s); 
    gzputc(concat_fq,'\n');

    //SEQUENCE
    gzputs(concat_fq,seq_1->seq.s);
    gzputs(concat_fq, SSEP);
    gzputs(concat_fq,seq_2->seq.s);
    gzputc(concat_fq,'\n');

    //QUAL HEADER
    gzputs(concat_fq,"+\n");
    
    //QUAL
    gzputs(concat_fq,seq_1->qual.s); 
    gzputs(concat_fq, SSEP);
    gzputs(concat_fq,seq_2->qual.s);
    gzputc(concat_fq,'\n');

  }
  
  kseq_destroy(seq_1); kseq_destroy(seq_2);
  gzclose(fp_1); gzclose(fp_2);
  gzclose(concat_fq);

  char cmd[1024];

  //---------------------------------------------------------------------------
  // Run duplex
  //---------------------------------------------------------------------------   
   /* char *path=NULL; */
   /* size_t size; */
   /* path=getcwd(path); */
   /* printf("Cwd:%s\n", path); */
#if defined(DUPLEX_DIR)
#else
#define DUPLEX_DIR getenv("DUPLEX_DIR")
#endif //DUPLEX_DIR

  if (DUPLEX_DIR == NULL){
    fprintf(stderr, "ERROR: You need to set the DUPLEX_DIR environment variable \n");
    exit(1);
  }
  printf("%s\n",DUPLEX_DIR);
  sprintf(cmd,"%s/duplex -i %s -z%d -n%d -e%f -f", DUPLEX_DIR, CONCAT_FQ, compressLvl,read_count,error_rate);
  printf("Run: %s...\n",cmd);

  int ret = system(cmd);

  if (ret !=0){
    fprintf(stderr, "ERROR: Duplex failed to execute\n");
    exit(1);
  }

  //---------------------------------------------------------------------------
  // Post-processing
  //---------------------------------------------------------------------------
  printf("Post-processing...\n");
  gzFile uniq_fq = gzopen(EXT_FQ , "r");
  if (uniq_fq == NULL) {
    fprintf(stderr, "ERROR: Fail to open input file: %s.\n", EXT_FQ);
    exit(1);
  }
  kseq_t *seq = kseq_init(uniq_fq);

  gzFile *fq_one = gzopen(FQ_ONE, mode);
  gzFile *fq_two = gzopen(FQ_TWO, mode);

  while ((l = kseq_read(seq)) >= 0)
    {
      //printf("%s\n",seq->name.s);
      //printf("comment:%s\n",seq->comment.s);
      //printf("%s\n",seq->seq.s);
      //printf("%s\n",seq->qual.s);

      //HEADER
      char *ptr_h = strtok(seq->name.s,HSEP);
      
      gzputc(fq_one,'@'); 
      gzputs(fq_one,ptr_h); 
      gzputc(fq_one,'\n');
      
      ptr_h =  strtok(NULL, HSEP);

      gzputc(fq_two,'@'); 
      gzputs(fq_two,ptr_h); 
      gzputc(fq_two,'\n');

      //SEQUENCE
      char *ptr_s = strtok(seq->seq.s,SSEP);
      
      gzputs(fq_one,ptr_s); 
      gzputc(fq_one,'\n');
      int seq_len = strlen(ptr_s);
      ptr_s =  strtok(NULL, SSEP);

      gzputs(fq_two,ptr_s); 
      gzputc(fq_two,'\n');

      //QUAL HEADER
      gzputs(fq_one,"+\n");
      gzputs(fq_two,"+\n");
      
      //QUAL
      char * q = NULL;

      q = str_sub(seq->qual.s,0,seq_len-1);
      gzputs(fq_one,q); 
      gzputc(fq_one,'\n');
      free(q);
      
      q = str_sub(seq->qual.s,seq_len+1,seq->qual.l-1);
      gzputs(fq_two,q); 
      gzputc(fq_two,'\n');
      free(q);
    }
  
  kseq_destroy(seq); 
  gzclose(uniq_fq);
  gzclose(fq_one); gzclose(fq_two);
  /* time_t t1 = time (NULL); */
  /* printf ("It took %.2lf seconds to compute \n", difftime(t1,t0) ); */
  return 0;
}


void printUsage(char **argv){
  printf("\nNAME\n");
  printf("%s -- Remove duplicates reads\n", argv[0]);
  printf("\nSYNOPSIS\n");
  printf("\tUsage: %s -1 <in_1.fastq> -2 <in_2.fastq> [-z <int>] [-e <double>] [-f] [-h]\n", argv[0]);
  printf("\nDESCRIPTION\n");
  printf("\tINPUT:\n");
  printf("\t\tReads pairs must be in the same order, and not have any unpaired reads\n");
  printf("\t\t-1 <in_1.fastq> Paired file 1\n");
  printf("\t\t-2 <in_2.fastq> Paired file 2\n");
  printf("\tOUTPUT:\n");
  printf("\t\t<%s> <%s> two fastq files of unique reads\n", FQ_ONE, FQ_TWO);
  printf("\t\t<output_name>_stats.txt.gz a simple tab delimited file with reads and number of copy (Each paired read separated by '%s').\n",SSEP);
  printf("\t-z Compression level: with -z in [-1:9].\n");
  printf("\t\t -1: Default compression\n");
  printf("\t\t  0: No compression\n");
  printf("\t\t  1: Best speed\n");
  printf("\t\t  9: Best compression\n");
  printf("\t-e False positive rate: with -e in ]0:1[.\n");
  printf("\t-h Display Usage.\n");
  exit(0);
}

/* http://nicolasj.developpez.com/articles/libc/string/ */
inline char *str_sub (const char *s, unsigned int start, unsigned int end)
{
   char *new_s = NULL;

   if (s != NULL && start < end)
   {
      new_s = malloc (sizeof (*new_s) * (end - start + 2));
      if (new_s != NULL)
      {
         unsigned int i;

         for (i = start; i <= end; i++)
         {
            new_s[i-start] = s[i];
         }
         new_s[i-start] = '\0';
      }
      else
      {
         fprintf (stderr, "Memoire insuffisante\n");
         exit (EXIT_FAILURE);
      }
   }
   return new_s;
}
