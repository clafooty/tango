/*
 * fq_read.h
 *
 *  Created on: 4 juil. 2011
 *      Author: loic
 */

#ifndef FQ_READ_H_
#define FQ_READ_H_

#include <stdlib.h> // pour pouvoir utiliser free
#include "string.h"


typedef struct fq_read{
	char *h;
	char *s;
	char *q;
	unsigned int copy_number;
}fq_read;

inline fq_read *fq_read_create(const char* head,const char *seq,const char *qual);
inline void fq_read_destroy(fq_read *fqr);
#endif /* FQ_READ_H_ */
