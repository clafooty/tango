/*
 * main.c
 *
 *  Created on: 23 juin 2011
 *      Author: loic
 */

#include <zlib.h>
#include <stdio.h>
#include "kseq.h"
#include "khash.h"
//#include <time.h>
#include "bloom.h"
#include "fq_read.h"
#include "conf.h"
#include <unistd.h>

KSEQ_INIT(gzFile, gzread)
KHASH_MAP_INIT_STR(str,fq_read *)

void printUsage(char **argv);
unsigned int getSequenceCount(kseq_t *seq);
unsigned int computeDuplicate(kseq_t *seq, const unsigned int sequence_count,khash_t(str) *h);
void computeBestQualityAndWriteSingleReads(kseq_t *seq,gzFile *fq,khash_t(str) *h);
int writeComputedReadAndStats(gzFile *fq,gzFile *stats,khash_t(str) *h);
inline float average(const char * q);


double error_rate = ERROR_RATE;

int main(int argc, char **argv)
{
  //time_t t0 = time (NULL);
  
  //---------------------------------------------------------------------------
  // Manage options
  //---------------------------------------------------------------------------
  int option = 0;
  char *input_file = NULL;
  int compressLvl = 0;
  int sequence_count = -1;
  int force = 0;
    
  while ((option = getopt (argc, argv, "hfi:z:n:e:")) != -1) {
    switch (option) {
	
    case 'h':
      printUsage(argv);
      break;

    case 'i':
      input_file = optarg;
      break;
          
    case 'z':
      compressLvl = atoi(optarg);
      if (! ( (compressLvl == -1) || 
	      ((compressLvl >= 0) && (compressLvl <= 9)) )) {
	fprintf (stderr, "ERROR: Option -z %d requires an argument between [-1:9].\n", compressLvl);
	exit(1);
      }
      break;
      
    case 'n':
      sequence_count = atoi(optarg);
      break;

    case 'e':
      error_rate = atof(optarg);
      if (!  ((error_rate > 0) && (error_rate < 1)) ) {
	fprintf (stderr, "ERROR: Option -z %f requires an argument between ]0:1[.\n", error_rate);
      }
      break;

    case 'f':
      force = 1;
      break;
      
    case '?':
      if (optopt == 'i')
	fprintf (stderr, "ERROR: Option -%c requires an argument.\n", optopt);
      else if (isprint (optopt))
	fprintf (stderr, "ERROR: Unknown option `-%c'.\n", optopt);
      else
	fprintf (stderr, "ERROR: Unknown option character `\\x%x'.\n", optopt);
      return 1;
      
    default:
      abort ();
    }
  }

  if (input_file == NULL) {
    fprintf(stderr, "ERROR: File is required as input\n");
    printUsage(argv);
    exit(1);
  }
  //---------------------------------------------------------------------------
  // Init File
  //---------------------------------------------------------------------------
  if (! force) { 
    if( (access( EXT_FQ, F_OK ) != -1) || 
	(access( EXT_STATS, F_OK ) != -1) ) { 
      fprintf (stderr, "ERROR: Outputs files already exists.\n");
      exit(1);
    }
  }

  gzFile fp;
  kseq_t *seq;
  fp = gzopen(input_file, "r");

  if (fp == NULL) {
    fprintf(stderr, "ERROR: Fail to open input file: %s.\n", input_file);
    exit(1);
  }

  //---------------------------------------------------------------------------
  // Real start
  //---------------------------------------------------------------------------
  seq = kseq_init(fp);
  
  printf("Get the sequence number to proceed: "); fflush(stdout);
  if (sequence_count == -1)
    sequence_count = getSequenceCount(seq);
  printf("%d\n", sequence_count);
  
  // Initiate hash table
  khash_t(str) *h = kh_init(str);
  
  // Reinitialise values to read input file once again from the beginning
  gzseek(fp,0,0);
  kseq_rewind(seq);
  
  printf("Putative Uniq Sequences: "); fflush(stdout);
  int uniq_seq_count = computeDuplicate(seq,sequence_count,h);
  printf("%d\n", uniq_seq_count);

  char mode[4];
  sprintf (mode, "w%d", compressLvl);

  gzFile * uniq_fq;
  uniq_fq = gzopen(EXT_FQ, mode);
  if (!uniq_fq){ perror(EXT_FQ); exit(1);}
  
  gzFile * stats;
  stats = gzopen(EXT_STATS, mode);
  if (!stats){ perror(EXT_STATS); exit(1);}
  
  // Reinitialise values to read input file once again from the beginning
  gzseek(fp,0,0);
  kseq_rewind(seq);

  printf("Compute best quality and write\n");
  computeBestQualityAndWriteSingleReads(seq, uniq_fq, h);
   
  int real_duplicate_number = writeComputedReadAndStats(uniq_fq,stats,h);
  printf("Real Uniq Sequences: %d\n", sequence_count - real_duplicate_number);

  kh_destroy(str, h);
  
  kseq_destroy(seq);
  gzclose(fp);
  
  
  gzclose(uniq_fq);
  gzclose(stats);
  
  /* time_t t1 = time (NULL); */
  /* printf ("It took %.2lf seconds to compute \n", difftime(t1,t0) ); */
  return 0;
}

// Function to read a fasta/fastq file and count the number of reads
unsigned int getSequenceCount(kseq_t *seq){
  
  int l;
  
  unsigned int sequence_count = 0;
  while ((l = kseq_read(seq)) >= 0) ++sequence_count;
  
  return sequence_count;
}

// Function to count the number of unique reads and insert duplicated reads in hash table
unsigned int computeDuplicate(kseq_t *seq,const int unsigned sequence_count,khash_t(str) *h){

  int l;

  unsigned int uniq_seq_count = 0;

  khiter_t k;
  int ret;

  // Initiate bloom filter
  BLOOM *filter = bloom_create(sequence_count,error_rate);

  // Count number of unique reads and store one copy of duplicated
  // reads in hash table
  while ((l = kseq_read(seq)) >= 0) {
    if (bloom_check(filter,seq->seq.s)) {
      char * key = strdup(seq->seq.s);
      if (!key) exit(1);
      k = kh_put(str, h, key, &ret);
      if (ret){
	fq_read * fqr = fq_read_create(seq->name.s,seq->seq.s,seq->qual.s);
	kh_value(h, k) = fqr;
      }
      else{
	free(key);
      }
    }
    else {
      bloom_add(filter,seq->seq.s);
      ++uniq_seq_count;
    }
  }
  bloom_destroy(filter);

  return uniq_seq_count;
}

// Function to count the number of unique reads and insert duplicated
// reads in hash table
void computeBestQualityAndWriteSingleReads(kseq_t *seq,gzFile *fq,khash_t(str) *h){

  int l;
  khiter_t k;

  while ((l = kseq_read(seq)) >= 0) {
    k = kh_get(str, h,seq->seq.s);
    if (k != kh_end(h)){
      kh_value(h, k)->copy_number += 1;
      if ( average(seq->qual.s) > average(kh_value(h, k)->q) ){
	free(kh_value(h, k)->h);
	free(kh_value(h, k)->q);
	kh_value(h, k)->h = strdup(seq->name.s);
	kh_value(h, k)->q = strdup(seq->qual.s);
      }
    }
    else{
      //gzputc(fq,'@'); gzputs(fq,seq->name.s); gzputc(fq,'\n');
      //gzputs(fq, seq->seq.s); gzputc(fq,'\n');
      //gzputs(fq,"+\n");
      //gzputs(fq,seq->qual.s); gzputc(fq,'\n');
      gzprintf(fq, "@%s\n%s\n%s\n%s\n",seq->name.s,seq->seq.s,"+",seq->qual.s);
    }
  }
}

// Function to write one copy of duplicated reads in output fastq file
// and write occurrence of duplicated reads in stats output file
int writeComputedReadAndStats(gzFile *fq,gzFile *stats,khash_t(str) *h){

  int real_duplicates_number = 0;
  khiter_t k;
  for (k = kh_begin(h); k != kh_end(h); ++k){
    if (kh_exist(h, k))  {
      //gzputc(fq,'@'); gzputs(fq,kh_value(h, k)->h); gzputc(fq,'\n');
      //gzputs(fq, kh_value(h, k)->s); gzputc(fq,'\n');
      //gzputs(fq,"+\n");
      //gzputs(fq,kh_value(h, k)->q); gzputc(fq,'\n');
      gzprintf(fq, "@%s\n%s\n%s\n%s\n",kh_value(h, k)->h,kh_value(h, k)->s,"+",kh_value(h, k)->q);

      //if copy_number == 1, it's a false positive
      if (kh_value(h, k)->copy_number != 1) {
	gzprintf(stats, "%s\t%d\n",kh_value(h, k)->s,kh_value(h, k)->copy_number);
	real_duplicates_number += kh_value(h, k)->copy_number - 1;
	}

      fq_read_destroy(kh_value(h, k));
      free((char *)kh_key(h, k));
    }
  }
  return real_duplicates_number;
}


inline float average(const char * q)
{
  unsigned int sum = 0;
  unsigned int idx;
  for(idx=0;idx < strlen(q);++idx) sum += (int) q[idx];

  return (float) sum / (float)idx;;
}

void printUsage(char **argv){
  printf("\nNAME\n");
  printf("%s -- Remove duplicates reads\n", argv[0]);
  printf("\nSYNOPSIS\n");
  printf("\tUsage: %s -i <in.seq> [-z <int>] [-n <int>] [-e <double>] [-f] [-h]\n", argv[0]);
  printf("\nDESCRIPTION\n");
  printf("\tINPUT: -i <in.fastq> a fastq file\n");
  printf("\tOUTPUT:\n");
  printf("\t\t_uniq.fq.gz a fastq file of unique reads\n");
  printf("\t\t_stats.txt.gz a simple tab delimited file with the sequence of a read and number of copy\n");
  printf("\t-z Compression level: with -z in [-1:9].\n");
  printf("\t\t -1: Default compression\n");
  printf("\t\t  0: No compression\n");
  printf("\t\t  1: Best speed\n");
  printf("\t\t  9: Best compression\n");
  printf("\t-n Reads Number\n");
  printf("\t-e False positive rate: with -e in ]0:1[.\n");
  printf("\t-f Force. If output files already exists, remove it and create a new file without prompting for confirmation regardless of its permissions. \n");
  printf("\t-h Display Usage.\n");
  exit(0);
}
