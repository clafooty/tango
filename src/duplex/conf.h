/*
 * conf.h
 *
 *  Created on: 12 dec. 2011
 *      Author: loic
 */

#ifndef CONF_H_
#define CONF_H_

#define ERROR_RATE 0.001
#define EXT_FQ "_uniq.fq.gz"
#define EXT_STATS "_stats.txt.gz"

#endif /* CONF_H_ */
