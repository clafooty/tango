#ifndef MISC_H_
#define MISC_H_

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include <math.h>

// Declare constants, macros and types
#define CONVERT_PHRED 33
#define CONVERT_ILLUMINA 31
#define ALPHABET_SIZE 5
#define PHRED_QUAL(i) ((i)-(CONVERT_PHRED))

#ifndef bool
typedef int bool;
#endif

// Array to convert nucleotides to indices
extern int convert_char_index[];
// Array to convert indices to nucleotides
extern char convert_index_char[];

// General functions
inline void check_memory_allocation(void*);
inline int max(int, int);
inline int min(int, int);
int get_maximum(int*, int);
int get_maximum_char(char*, int);
int get_minimum(int*, int);
int get_minimum_char(char*, int, int*);
int get_maximum_index(int*, int);
int count_unique_elements(int*, int);
int count_maximal_occurrence(int*, int);
float get_average(int*, const int);
float get_average_char(char*, const int);
float get_median(int*, const int);
int comp(const void*, const void*);
int comp_char(const void*, const void*);
void get_quartiles(char*, const int, float*, float*, float*);

#endif /*MISC_H_*/
