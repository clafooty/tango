#ifndef OPTIONS_H_
#define OPTIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Declare prototypes of functions
void print_help(void);
void print_usage(char**);
void test_positive_option(int, char, char**);
void set_new_extension(int, char, char*);
void split_file_name(char*, char*);

#endif /*OPTIONS_H_*/
