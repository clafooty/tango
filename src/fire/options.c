#include "options.h"


// Function to print help
void print_help(void)
{
  printf("\tHELP\n-s\tName of the single-end fastq file (-s single_file.fq)\n-1\tName of the first paired-end fastq file (-1 paired_file_1.fq)\n-2\tName of the second paired-end fastq file (-2 paired_file_2.fq)\n-C\tQuality values from Illumina pipeline v1.2 or higher have to be converted in PHRED quality values\n-d\tDescribe the reads: calculation of base composition by position, calculation of minimum, maximum, average, median, quartiles 1 and 3 and inter quartile range quality by position, calculation of the number of homopolymers\n\tIf the option -P is not used, a read is considered homopolymer if it is exclusively made of one nucleoide\n\tIf the option -P is used, a read is considered homopolymer if it contains REMOVE_POLY or more consecutive times the same nucleotide)\n-D\tDescribe the reads, same description as -d option but without calculation of median and quartiles quality per position\n\tThis option is prefered to the -d option for very large data sets since it is less memory consuming\n-g\tGood quality threshold (bases with a quality lower than this value will be seen as low quality bases)\n\tIf this option is used without the options -l or -n, only the reads for which all nucleotides have a quality bigger than or equal to this value will be kept\n\tIf this option is used with the option -l, the reads for which LENGHT_QPERCENT %% of their length has a quality bigger than or equal to this value will be kept\n\tIf this option is used with the option -n, the reads for which CONSECUTIVE_NUCLEOTIDES or more nucleotides have a quality bigger than or equal to this value will be kept\n-l\tPercentage of read length that has to be of quality bigger than or equal to GOODQ_THRESHOLD for a read to be seen as a good quality read\n\tThis option requires the option -g\n-b\tBad quality threshold (bases with a quality lower than this value will be seen as bad quality bases)\n\tThis option requires the option -m\n-m\tMaximum number of nucleotides with quality lower than or equal to BADQ_THRESHOLD allowed in a read\n\tThis option requires the option -b\n-a\tReads with an average quality bigger than or equal to this value are kept\n-n\tNumber of consecutive nucleotide bases that have to be of quality bigger than or equal to GOODQ_THRESHOLD for a read to be seen as a good quality read\n\tThis option requires the option -g\n-c\tReads occurring more than COPY_NUMBER times are displayed\n-P\tRemove reads containing the same nucleotide (A, C, G or T) REMOVE_POLY times or morein a raw\n-N\tRemove reads containing Ns REMOVE_NS times or more\n-t\tQuality trimming threshold for the first bases of reads (All nucleotides of the begin of the reads with a quality lower than this threshold are trimmed)\n-T\tQuality trimming threshold for the last bases of reads (All nucleotides of the end of the reads with a quality lower than this threshold are trimmed)\n-L\tReads shorter than MINIMUM_READ_LENGTH are discarded\n");
}

// Function to print usage
void print_usage(char **argv)
{
  printf("usage for help: %s -h\n", argv[0]);
  printf("usage for Single-End files: %s -s file.fq [-C -d -D -g <int> -l <int> -b <int> -m <int> -a <int> -n <int> -c <int> -P <int> -N <int> -t <int> -T <int> -L <int>]\n", argv[0]);
  printf("usage for Paired-End files: %s -1 file_1.fq -2 file_2.fq [-C -d -D -g <int> -l <int> -b <int> -m <int> -a <int> -n <int> -c <int> -P <int> -N <int> -t <int> -T <int> -L <int>]\n", argv[0]);
}


// Function to test whether an option has a positive value
void test_positive_option(int option_value, char option, char **argv)
{
  if (option_value <= 0)
  {
    fprintf(stderr, "error: option -%c must have a positive integer as argument\n", option);
    print_usage(argv);
    exit(1);
  }
}


// Function to set output file name with option and option value
void set_new_extension(int option_value, char option, char *new_extension)
{
  char extension[20] = "";
  sprintf(extension, "_%c%d", option, option_value);
  strcat(new_extension, extension);
}


// Function to remove extensions .txt, .fastq and .fq from file name
void split_file_name(char *file_name, char *output_name)
{
  int i = strlen(file_name) - 1;
  if ( (file_name[i] == 't') && (file_name[i-1] == 'x') && (file_name[i-2] == 't') && (file_name[i-3] == '.') )
  {
    output_name[i-3] = '\0';
  }
  else if ( (file_name[i] == 'q') && (file_name[i-1] == 'f') && (file_name[i-2] == '.') )
  {
    output_name[i-2] = '\0';
  }
  else if ( (file_name[i] == 'q') && (file_name[i-1] == 't') && (file_name[i-2] == 's') && (file_name[i-3] == 'a') && (file_name[i-4] == 'f') && (file_name[i-5] == '.') )
  {
    output_name[i-5] = '\0';
  }
  else
  {
    fprintf(stderr, "error: input file name %s has not an appropriate extension (.txt or .fastq or .fq)\n", file_name);
    exit(8);
  }
}
