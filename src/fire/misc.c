#include "misc.h"


// Array to convert nucleotides to indices (A->0, T->1, G->2, C->3, N->4)
int convert_char_index[256] = {
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, 0, -1, 3, -1, -1,
  -1, 2, -1, -1, -1, -1, -1, -1, 4, -1,
  -1, -1, -1, -1, 1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1
};


// Array to convert indices to nucleotides (0->A, 1->T, 2->G, 3->C, 4->N)
char convert_index_char[10] = {
  'A', 'T', 'G', 'C', 'N', ' ', ' ', ' ', ' ', ' '
};


// Function to check memoty allocation after malloc, calloc or strdup
inline void check_memory_allocation(void *pointer)
{
  if (pointer == NULL)
  {
    fprintf(stderr, "error: impossible to allocate memory\n");
    exit(-1);
  }
}


// Function to calculate the maximum number of two numbers
inline int max(int a, int b)
{
  if (a > b)
    return a;
  else
    return b;
}


// Function to calculate the minimum number of two numbers
inline int min(int a, int b)
{
  if (a < b)
    return a;
  else
    return b;
}


// Function to calculate the maximum number of an array of int and return the maximum value
int get_maximum(int *array, int nb_elts)
{
  int i = 0;
  int maximum = 0;
  for (i = 0; i < nb_elts; i++)
  {
    maximum = max(array[i], maximum);
  }
  return (maximum);
}


// Function to calculate the maximum number of an array of char and return the maximum value
int get_maximum_char(char *array, int nb_elts)
{
  int i = 0;
  int maximum = 0;
  for (i = 0; i < nb_elts; i++)
  {
    maximum = max((int)array[i], maximum);
  }
  return (maximum);
}


// Function to calculate the minimum positive number of an array of int and return the minimum value
int get_minimum(int *array, int nb_elts)
{
  int i = 0;
  int minimum = get_maximum(array, nb_elts);
  for (i = 0; i < nb_elts; i++)
  {
    if ( (array[i] < minimum) && (array[i] != 0) )
      minimum = array[i];
  }
  return (minimum);
}

// Function to calculate the minimum positive number of an array of char and return the minimum value
int get_minimum_char(char *array, int nb_elts, int *minimum)
{
  int i = 0;
  for (i = 0; i < nb_elts; i++)
  {
    if ( ((int)array[i] < *minimum) && (array[i] != 0) )
      *minimum = array[i];
  }
  return (*minimum);
}


// Function to calculate the maximum number of an array of int and return the index at which the maximum value is obtained
int get_maximum_index(int *array, int nb_elts)
{
  int i = 0;
  int index = -1;
  int maximum = -1;
  for (i = 0; i < nb_elts; i++)
  {
    if (array[i] > maximum)
    {
      maximum = array[i];
      index = i;
    }
  }
  return (index);
}


// Function to count the number of unique elements of an array //and the occurrence of the element appearing the most
int count_unique_elements(int *array, int nb_elts)
{
  int i = 0;
  int j = 0;
  bool unique = 0;
  int unique_elts[nb_elts];
  memset(unique_elts, 0, sizeof(unique_elts));
  int nb_unique_elts = 0;
  for (i = 0; i < nb_elts; i++)
  {
    // Consider only non null elements
    if (array[i] != 0)
    {
      unique = 1;
      // Check if element already encountered
      for (j = 0; j < nb_unique_elts; j++)
      {
	if (array[i] == unique_elts[j])
	{
	  unique = 0;
	}
      }
      // Insert unique element in array of unique elements
      if (unique == 1)
      {
	unique_elts[nb_unique_elts] = array[i];
	nb_unique_elts++;
      }
    }
  }
  return (nb_unique_elts);
}


// Function to count the occurrence of the element appearing the most
int count_maximal_occurrence(int *array, int nb_elts)
{
  int i = 0;
  int j = 0;
  bool unique = 0;
  int unique_elts[nb_elts];
  memset(unique_elts, 0, sizeof(unique_elts));
  int occurrence_elts[nb_elts];
  memset(occurrence_elts, 0, sizeof(occurrence_elts));
  int nb_unique_elts = 0;
  int maximal_occurrence = 0;
  for (i = 0; i < nb_elts; i++)
  {
    // Consider only non null elements
    if (array[i] != 0)
    {
      unique = 1;
      // Check if element already encountered
      for (j = 0; j < nb_unique_elts; j++)
      {
	if (array[i] == unique_elts[j])
	{
	  unique = 0;
	  occurrence_elts[j]++;
	  
	}
      }
      // Insert unique element in array of unique elements
      if (unique == 1)
      {
	unique_elts[nb_unique_elts] = array[i];
	occurrence_elts[j] = 1;
	nb_unique_elts++;
      }
    }
  }
  // Calculate maximum occurrence
  maximal_occurrence = get_maximum(occurrence_elts, nb_unique_elts);
  return (maximal_occurrence);
}


// Function to calculate the average value of an array of int
float get_average(int *array, const int nb_elts)
{
  int i = 0;
  unsigned long sum = 0;
  float average = 0;
  for (i = 0; i < nb_elts; i++)
  {
    sum += array[i];
  }
  average = (float)sum / (float)nb_elts;
  return(average);
}


// Function to calculate the average value of an array of char
float get_average_char(char *array, const int nb_elts)
{
  int i = 0;
  unsigned long sum = 0;
  float average = 0;
  for (i = 0; i < nb_elts; i++)
  {
    sum += (int)array[i];
  }
  average = (float)sum / (float)nb_elts;
  return(average);
}


// Function to calculate the median value of an array
float get_median(int *sorted_array, const int nb_elts)
{
  float median = 0;
  if (nb_elts % 2 == 0)  
  {
    median = ((float)sorted_array[nb_elts / 2] + (float)sorted_array[(nb_elts / 2) - 1]) / 2;   
  }  
  else
  {  
    median = (float)sorted_array[(nb_elts - 1) / 2];
  }
  //printf("median: %.2f\n", median);
  return(median);
}


int comp(const void *a, const void *b)
{
  return ( *(int*)a - *(int*)b );
}


int comp_char(const void *a, const void *b)
{
  return ( *(char*)a - *(char*)b );
}

void get_quartiles(char *sorted_array, const int nb_elts, float *median, float *q1, float *q3)
{
  int mid = 0;
  if (nb_elts % 2 == 0)  
  {
    mid = nb_elts / 2;
    *median = ((float)sorted_array[mid] + (float)sorted_array[mid - 1]) / 2;
    if (mid % 2 == 0)
    {
      *q1 = ((float)sorted_array[mid / 2] + (float)sorted_array[(mid / 2) - 1]) / 2;
      *q3 = ((float)sorted_array[mid + (mid / 2)] + (float)sorted_array[mid + (mid / 2) - 1]) / 2;
    }
    else
    {
      *q1 = (float)sorted_array[(mid - 1) / 2];
      *q3 = (float)sorted_array[mid + ((mid - 1) / 2)];
    }
  }  
  else
  {
    mid = (nb_elts - 1) / 2;
    *median = (float)sorted_array[mid];
    if (mid % 2 == 0)
    {
      *q1 = ((float)sorted_array[mid / 2] + (float)sorted_array[(mid / 2) - 1]) / 2;
      *q3 = ((float)sorted_array[mid + (mid / 2)] + (float)sorted_array[mid + (mid / 2) + 1]) / 2;
    }
    else
    {
      *q1 = (float)sorted_array[(mid - 1) / 2];
      *q3 = (float)sorted_array[mid + ((mid + 1) / 2)];
    }
  }
}
