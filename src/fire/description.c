#include "description.h"


// Function to count the number of reads in a fastq file and get the maximum length of the reads
void count_read_length(kseq_t *read, int *nb_reads, int *min_read_length, int *max_read_length, float *average_read_length)
{
  int l;
  int read_length = 0;
  unsigned long sum = 0;
  while ( (l = kseq_read(read)) >= 0 )
  {
    ++*nb_reads;
    read_length = strlen(read->seq.s);
    sum += read_length;
    *max_read_length = max(*max_read_length, read_length);
    *min_read_length = min(*min_read_length, read_length);
  }
  *average_read_length = (float)sum / *nb_reads;
}


// Function to calculate the minimum, maximum and avearge quality of reads per position
void quality_stats(char *read_qual, int *min_quality, int *max_quality, unsigned long *sum_quality, const int read_length)
{
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    sum_quality[i] += (int)read_qual[i];
    if (min_quality[i] == 0)
      min_quality[i] = (int)read_qual[i];
    min_quality[i] = min(min_quality[i], (int)read_qual[i]);
    max_quality[i] = max(max_quality[i], (int)read_qual[i]);
  }
}


// Function to fill in the base composition array
void check_base_composition(char *read_seq, int **base_composition, const int read_length)
{
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    if (read_seq[i] == 'A')
    {
      base_composition[convert_char_index['A']][i]++;
    }
    if (read_seq[i] == 'T')
    {
      base_composition[convert_char_index['T']][i]++;
    }
    if (read_seq[i] == 'G')
    {
      base_composition[convert_char_index['G']][i]++;
    }
    if (read_seq[i] == 'C')
    {
      base_composition[convert_char_index['C']][i]++;
    }
    if (read_seq[i] == 'N')
    {
      base_composition[convert_char_index['N']][i]++;
    }
  }
}


// Function to calculate the proportion of bases per position
void get_proportions(int **base_composition, float **base_proportion, const int nb_elts)
{
  int i = 0;
  int j = 0;
  int *nb_bases;
  nb_bases = calloc(nb_elts, sizeof(int));
  check_memory_allocation(nb_bases);
  // Calculate the number of bases per position (necessary if all reads do not have the same length)
  for (i = 0; i < nb_elts; i++)
  {
    for (j = 0; j < ALPHABET_SIZE; j++)
    {
      nb_bases[i] += base_composition[j][i];
    }
  }
  // Calculate proportion of bases by position
  for (i = 0; i < nb_elts; i++)
  {
    for (j = 0; j < ALPHABET_SIZE; j++)
    {
      base_proportion[j][i] = (float)base_composition[j][i] / (float)nb_bases[i] *100;
    }
  }
  free(nb_bases);
}


// Function to store the quality of all bases in an array
void check_quality(char *read_qual, char **quality_position, const int read_length, const int read_nb)
{
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    quality_position[i][read_nb-1] = read_qual[i];
  }
}


// Function to count the number of homopolymers
void count_poly(char *read_seq, int *count_poly_reads, const int remove_poly, const int read_length)
{
  if (remove_poly != 0)
  {
    if (is_poly(read_seq, remove_poly, read_length) != ' ')
    {
      count_poly_reads[convert_char_index[(int)is_poly(read_seq, remove_poly, read_length)]]++;
    }
  }
  else
  {
    if (is_poly(read_seq, read_length, read_length) != ' ')
    {
      count_poly_reads[convert_char_index[(int)is_poly(read_seq, read_length, read_length)]]++;
    }
  }  
}


// Function to print the base compososition
void print_base_composition(int **base_composition, float **base_proportion, const int max_read_length, FILE *fd)
{
  int i = 0;
  fprintf(fd, "\nBase composition\nPosition\tA\tT\tG\tC\tN\t%%A\t%%T\t%%G\t%%C\t%%N\n");
  for (i = 0; i < max_read_length; i++)
  {
    fprintf(fd, "%d\t%d\t%d\t%d\t%d\t%d\t", i+1, base_composition[convert_char_index['A']][i], base_composition[convert_char_index['T']][i], base_composition[convert_char_index['G']][i], base_composition[convert_char_index['C']][i], base_composition[convert_char_index['N']][i]);
    fprintf(fd, "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", base_proportion[convert_char_index['A']][i], base_proportion[convert_char_index['T']][i], base_proportion[convert_char_index['G']][i], base_proportion[convert_char_index['C']][i], base_proportion[convert_char_index['N']][i]);
  }
}


// Function to print the quality statistics
void print_quality(char **quality_position, int *min_quality, int* max_quality, unsigned long *sum_quality, const int max_read_length, const int nb_reads, const int description, FILE *fd)
{
  int i = 0;
  float median = 0;
  float q1 = 0;
  float q3 = 0;
  fprintf(fd, "\nQuality by position\nPosition\tAverage quality\tMinimum Quality\tMaximum Quality");
  if (description == 1)
  {
    fprintf(fd, "\tMedian Quality\tQ1\tQ3\tIQR");
  }
  fprintf(fd, "\n");
  for (i = 0; i < max_read_length; i++)
  {
    fprintf(fd, "%d\t%.2f\t%d\t%d", i+1, PHRED_QUAL((float)sum_quality[i]/nb_reads), PHRED_QUAL(min_quality[i]), PHRED_QUAL(max_quality[i]));
    if (description == 1)
    {
      median = q1 = q3 = 0;
      qsort(quality_position[i], nb_reads, sizeof(char), comp_char);
      get_quartiles(quality_position[i], nb_reads, &median, &q1, &q3);
      fprintf(fd, "\t%.2f\t%.2f\t%.2f\t%.2f", PHRED_QUAL(median), PHRED_QUAL(q1), PHRED_QUAL(q3), (PHRED_QUAL(q3)-PHRED_QUAL(q1)));
    }
    fprintf(fd, "\n");
  }
}


// Function to print the homopolymer statistics
void print_poly(int *count_poly_reads, FILE *fd)
{
  int i = 0;
  fprintf(fd, "\nHomopolymers\nNucleotide\tNumber of homopolymers\n");
  for (i = 0; i < ALPHABET_SIZE; i++)
  {
    fprintf(fd, "%c\t%d\n", convert_index_char[i], count_poly_reads[i]);
  }
}
  
