#ifndef READS_H_
#define READS_H_

#include <stdio.h>
#include <stdlib.h>

#include "misc.h"


// Declare prototypes of functions
inline void print_read(char*, char*, char*);
inline void print_read_file(FILE*, char*, char*, char*);
inline void illumina_ascii2phred_ascii(char*, const int);
inline float count_goodQ_nucleotides(char*, const int, const int);
inline int count_badQ_nucleotides(char*, const int, const int);
inline float calculate_averageQ(char*, const int);
inline int count_goodQ_consecutive(char*, const int, const int, const int);
inline int position_trim_begin(char*, const int, const int);
inline void trim_begin(char*, const int, const int);
inline int position_trim_end(char*, const int, const int);
inline void trim_end(char*, const int, const int);
inline int count_Ns(char*, const int);
inline int count_consecutive_nucleotides(char*, const char, const int);
inline char is_poly(char*, const int, const int);

#endif /*READS_H_*/
