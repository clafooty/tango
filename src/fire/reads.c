#include "reads.h"

char alphabet [ALPHABET_SIZE]="ATGCN";


// Function to print a read in fastq format
inline void print_read(char *read_name, char *read_seq, char *read_qual)
{
  printf("@%s\n%s\n+\n%s\n", read_name, read_seq, read_qual);
}


// Function to print a read in fastq format in a file
inline void print_read_file(FILE *fd, char *read_name, char *read_seq, char *read_qual)
{
  fprintf(fd, "@%s\n%s\n+\n%s\n", read_name, read_seq, read_qual);
}


// Function to convert Illumina ascii quality score to PHRED quality score in ascii
inline void illumina_ascii2phred_ascii(char* read_qual, const int read_length)
{
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    read_qual[i] = read_qual[i]-CONVERT_ILLUMINA;
  }
}


// Function to count the number of good quality nucleotides contained in a read
inline float count_goodQ_nucleotides(char *read_qual, const int goodQ_threshold, const int read_length)
{
  float goodQ_nucleotides = 0.0;
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    if ((PHRED_QUAL(read_qual[i])) >= goodQ_threshold)
    {
      goodQ_nucleotides++;
    }
  }
  return (goodQ_nucleotides);
}


// Function to count the number of bad quality nucleotides contained in a read
inline int count_badQ_nucleotides(char *read_qual, const int badQ_threshold, const int read_length)
{
  int nb_badQ = 0;
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    if ((PHRED_QUAL(read_qual[i])) <= badQ_threshold)
    {
      nb_badQ ++;
    }
  }
  return (nb_badQ);
}


// Function to calculate the average quality of a read
inline float calculate_averageQ(char *read_qual, const int read_length)
{
  float averageQ = 0.0;
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    averageQ += PHRED_QUAL(read_qual[i]);
  }
  averageQ = averageQ / read_length;
  return (averageQ);
}


// Function to count the number of consecutive good quality nucleotides contained in a read
inline int count_goodQ_consecutive(char *read_qual, const int goodQ_threshold, const int consecutive_nucleotides, const int read_length)
{
  int i = 0;
  int nb_goodQ_consecutive = 0;
  for (i = 0; i < read_length; i++)
  {
    if ((PHRED_QUAL(read_qual[i])) >= goodQ_threshold)
    {
      nb_goodQ_consecutive ++;
      if (nb_goodQ_consecutive >= consecutive_nucleotides)
      {
	return (1);
      }
    }
    else
    {
      nb_goodQ_consecutive = 0;
      if (i > (read_length - consecutive_nucleotides + 1))
      {
	return 0;
      }
    }
  }
  return 0;
}


// Function to determine the position at which the read should start after quality trimming of the beginning of the read
inline int position_trim_begin(char *read_qual, const int trim_begin_quality, const int read_length)
{
  int i = 0;
  int trim_begin_position = 0;
  if ((PHRED_QUAL(read_qual[i])) < trim_begin_quality)
  {
    while ( (i < read_length) && ((PHRED_QUAL(read_qual[i])) < trim_begin_quality) )
    {
      trim_begin_position++;
      i++;
    }
  }
  return (trim_begin_position);
}


// Function to trim the begin of a read (sequence or quality)
inline void trim_begin(char *read, const int trim_begin_position, const int read_length)
{
  int i = 0;
  if (trim_begin_position != 0)
  {
    for (i = 0; i < read_length; i++)
    {
      if (i <= (read_length - trim_begin_position))
      {
	read[i] = read[i+trim_begin_position];
      }
    }
  }
}


// Function to determine the position at which the read should stop after quality trimming of the end of the read
inline int position_trim_end(char *read_qual, const int trim_end_quality, const int read_length)
{
  int i = read_length;
  int trim_end_position = read_length;
  if ((PHRED_QUAL(read_qual[read_length])) < trim_end_quality)
  {
    while ( (i > 0) && ((PHRED_QUAL(read_qual[i])) < trim_end_quality) )
    {
      trim_end_position--;
      i--;
    }
  }
  return (trim_end_position);
}


// Function to trim the end of a read (sequence or quality)
inline void trim_end(char *read, const int trim_end_position, const int read_length)
{
  if (trim_end_position != 0)
  {
    read[trim_end_position + 1] = '\0';
  }
}


// Function to count the number of Ns in a sequence
inline int count_Ns(char *read_seq, const int read_length)
{
  int nb_Ns = 0;
  int i = 0;
  for (i = 0; i < read_length; i++)
  {
    if (read_seq[i] == 'N')
    {
      nb_Ns++;
    }
  }
  return (nb_Ns);
}


// Function to count the number of consecutive same nucleotides in a read
inline int count_consecutive_nucleotides(char *read_seq, const char nucleotide, const int read_length)
{
  int nb_consecutive_bases = 0;
  int max_nb_consecutive_bases = 0;
  int i = 0;
  while (i < read_length)
  {  
    if (read_seq[i] == nucleotide)
    {
      nb_consecutive_bases ++;
      if (nb_consecutive_bases > max_nb_consecutive_bases)
      {
	max_nb_consecutive_bases = nb_consecutive_bases;
      }
    }
    else
    {
      nb_consecutive_bases = 0;
    }
    i++;
  }
  return (max_nb_consecutive_bases);
}


// Function to check whether a read is a poly A/C/G/T/N (contains more than remove_poly times the same nucleotide in a raw)
inline char is_poly(char *read_seq, const int remove_poly, const int read_length)
{
  int i = 0;
  char poly_nucleotide = ' ';
  for (i = 0; i < ALPHABET_SIZE; i++)
  {
    if (count_consecutive_nucleotides(read_seq, alphabet[i], read_length) >= remove_poly)
    {
      poly_nucleotide = alphabet[i];
    }
  }
  return(poly_nucleotide);
}



