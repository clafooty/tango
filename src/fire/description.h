#ifndef DESCRIPTION_H_
#define DESCRIPTION_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#include "kseq.h"
#include "misc.h"
#include "reads.h"

// Declare type of file handler and  read() function
KSEQ_INIT(gzFile, gzread)

void count_read_length(kseq_t*, int*, int*, int*, float*);
void quality_stats(char*, int*, int*, unsigned long*, const int);
void check_base_composition(char*, int**, const int);
void get_proportions(int**, float**, const int);
void check_quality(char*, char**, const int, const int);
void count_poly(char*, int*, const int, const int);

void print_quality(char**, int*, int*, unsigned long*, const int, const int, const int, FILE*);
void print_base_composition(int**, float**, const int, FILE*);
void print_poly(int*, FILE*);

#endif /*DESCRIPTION_H_*/
