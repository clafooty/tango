/*
 * main.c
 * pre-processing pipeline
 * description but without array of lengths and with qualities stored in char in order to use less memory...
 * compilation: gcc -lz -Wall -o pipeline preprocessing_11.c options.c reads.c description.c misc.c
 *
 *  Created on: 8 july 2011
 *      Author: Erika
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <zlib.h>
#include <string.h>
#include <getopt.h>

#include "kseq.h"
#include "options.h"
#include "reads.h"
#include "description.h"



int main(int argc, char *argv[])
{
  // Declare variables
  time_t t0 = time (NULL);
  int option = 0;
  char *single_file = NULL;
  char *paired_file_1 = NULL;
  char *paired_file_2 = NULL;
  char *description_file = NULL;
  char *description_file_1 = NULL;
  char *description_file_2 = NULL;
  gzFile single_fd;
  gzFile paired_1_fd;
  gzFile paired_2_fd;
  char *file_name = NULL;
  char *file_name_1 = NULL;
  char *file_name_2 = NULL;
  char new_extension[200] = "";
  char *output_name = NULL;
  char *output_name_1 = NULL;
  char *output_name_2 = NULL;
  FILE *out_single_fd;
  FILE *out_paired_1_fd;
  FILE *out_paired_2_fd;
  FILE *description_fd;
  FILE *description_1_fd;
  FILE *description_2_fd;
  kseq_t *read;
  kseq_t *read_1;
  kseq_t *read_2;
  int l, m;  
  bool filtering = 0;
  bool description = 0;
  bool short_description = 0;
  int goodQ_threshold = 0;
  int length_Qpercent = 0;
  int badQ_threshold = 0;
  int max_badQ_nucleotides = 0;
  bool define_badQ = 0;
  int averageQ = 0;
  int consecutive_nucleotides = 0;
  int remove_poly = 0;
  bool define_rm_poly = 0;
  int remove_Ns = 0;
  bool define_rm_Ns = 0;
  int trim_begin_quality = 0;
  int trim_end_quality = 0;
  int minimum_read_length = 0;
  bool convert_quality = 0;
  bool single_end = 0;
  int copy_number = 0;
  char *read_name = NULL;
  char *read_name_1 = NULL;
  char *read_name_2 = NULL;
  char *read_seq = NULL;
  char *read_seq_1 = NULL;
  char *read_seq_2 = NULL;
  char *read_qual = NULL;
  char *read_qual_1 = NULL;
  char *read_qual_2 = NULL;
  int passed_read = 1;
  int read_length = 0;
  int read_length_1 = 0;
  int read_length_2 = 0;
  int trim_begin_position = 0;
  int trim_begin_position_1 = 0;
  int trim_begin_position_2 = 0;
  int trim_end_position = 0;
  int trim_end_position_1 = 0;
  int trim_end_position_2 = 0;
  int count_reads = 0;
  int nb_reads = 0;
  int nb_reads_1 = 0;
  int nb_reads_2 = 0;
  int count_good_reads = 0;
  int min_read_length = 1000;
  int min_read_length_1 = 1000;
  int min_read_length_2 = 1000;
  int max_read_length = 0;
  int max_read_length_1 = 0;
  int max_read_length_2 = 0;
  float average_read_length = 0;
  float average_read_length_1 = 0;
  float average_read_length_2 = 0;
  int **base_composition = NULL;
  int **base_composition_1 = NULL;
  int **base_composition_2 = NULL;
  float **base_proportion = NULL;
  float **base_proportion_1 = NULL;
  float **base_proportion_2 = NULL;
  int *min_quality = NULL;
  int *min_quality_1 = NULL;
  int *min_quality_2 = NULL;
  int *max_quality = NULL;
  int *max_quality_1 = NULL;
  int *max_quality_2 = NULL;
  unsigned long *sum_quality = NULL;
  unsigned long *sum_quality_1 = NULL;
  unsigned long *sum_quality_2 = NULL;
  char **quality_position = NULL;
  char **quality_position_1 = NULL;
  char **quality_position_2 = NULL;
  int *count_poly_reads = NULL;
  int *count_poly_reads_1 = NULL;
  int *count_poly_reads_2 = NULL;
  int **base_composition_filter = NULL;
  int **base_composition_filter_1 = NULL;
  int **base_composition_filter_2 = NULL;
  float **base_proportion_filter = NULL;
  float **base_proportion_filter_1 = NULL;
  float **base_proportion_filter_2 = NULL;
  int *min_quality_filter = NULL;
  int *min_quality_filter_1 = NULL;
  int *min_quality_filter_2 = NULL;
  int *max_quality_filter = NULL;
  int *max_quality_filter_1 = NULL;
  int *max_quality_filter_2 = NULL;
  unsigned long *sum_quality_filter = NULL;
  unsigned long *sum_quality_filter_1 = NULL;
  unsigned long *sum_quality_filter_2 = NULL;
  int min_read_length_filter = 0;
  int min_read_length_filter_1 = 0;
  int min_read_length_filter_2 = 0;
  int max_read_length_filter = 0;
  int max_read_length_filter_1 = 0;
  int max_read_length_filter_2 = 0;
  unsigned long sum_read_length_filter = 0;
  unsigned long sum_read_length_filter_1 = 0;
  unsigned long sum_read_length_filter_2 = 0;
  char **quality_position_filter = NULL;
  char **quality_position_filter_1 = NULL;
  char **quality_position_filter_2 = NULL;
  int *count_poly_reads_filter = NULL;
  int *count_poly_reads_filter_1 = NULL;
  int *count_poly_reads_filter_2 = NULL;
  int i = 0;
  
  // Get options from command line, check their values and set output file name
  while ((option = getopt(argc,argv,"hs:1:2:dDg:l:b:m:a:n:c:CP:N:t:T:L:")) != -1)
  {
    switch (option)
    {
      case 'h':
	print_help();
	return(0);
      case 's':
	single_file = optarg;
	file_name = (char*)(malloc(strlen(single_file) + 1));
	check_memory_allocation(file_name);
	strcpy(file_name, single_file);
	split_file_name(single_file, file_name);
	single_end = 1;
        break;
      case '1':
	paired_file_1 = optarg;
	file_name_1 = (char*)(malloc(strlen(paired_file_1) + 1));
	check_memory_allocation(file_name_1);
	strcpy(file_name_1, paired_file_1);
	split_file_name(paired_file_1, file_name_1);
	single_end = 0;
        break;
      case '2':
	paired_file_2 = optarg;
	file_name_2 = (char*)(malloc(strlen(paired_file_2) + 1));
	check_memory_allocation(file_name_2);
	strcpy(file_name_2, paired_file_2);
	split_file_name(paired_file_2, file_name_2);
	single_end = 0;
        break;
      case 'd':
	description = 1;
	break;
      case 'D':
	short_description = 1;
        break;	
      case 'g':
	goodQ_threshold = atoi(optarg);
	test_positive_option(goodQ_threshold, 'g', argv);
	set_new_extension(goodQ_threshold, 'g', new_extension);
	filtering = 1;
        break;
      case 'l':
	length_Qpercent = atoi(optarg);
	test_positive_option(length_Qpercent, 'l', argv);
	set_new_extension(length_Qpercent, 'l', new_extension);
	filtering = 1;
        break;
      case 'b':
	badQ_threshold = atoi(optarg);
	test_positive_option(badQ_threshold, 'b', argv);
	set_new_extension(badQ_threshold, 'b', new_extension);
	filtering = 1;
        break;
      case 'm':
	max_badQ_nucleotides = atoi(optarg);
	test_positive_option(max_badQ_nucleotides, 'm', argv);
	set_new_extension(max_badQ_nucleotides, 'm', new_extension);
	define_badQ = 1;
	filtering = 1;
        break;
      case 'a':
	averageQ = atoi(optarg);
	test_positive_option(averageQ, 'a', argv);
	set_new_extension(averageQ, 'a', new_extension);
	filtering = 1;
        break;
      case 'n':
	consecutive_nucleotides = atoi(optarg);
	test_positive_option(consecutive_nucleotides, 'n', argv);
	set_new_extension(consecutive_nucleotides, 'n', new_extension);
	filtering = 1;
        break;
      case 'c':
	copy_number = atoi(optarg);
	test_positive_option(copy_number, 'c', argv);
	set_new_extension(copy_number, 'c', new_extension);
	filtering = 1;
        break;
      case 'C':
	convert_quality = 1;
	strcat(new_extension, "_C");
	filtering = 1;
        break;
      case 'P':
	remove_poly = atoi(optarg);
	test_positive_option(remove_poly, 'P', argv);
	set_new_extension(remove_poly, 'P', new_extension);
	define_rm_poly = 1;
	filtering = 1;
        break;
      case 'N':
	remove_Ns = atoi(optarg);
	test_positive_option(remove_Ns, 'N', argv);
	set_new_extension(remove_Ns, 'N', new_extension);
	define_rm_Ns = 1;
	filtering = 1;
        break;
      case 't':
	trim_begin_quality = atoi(optarg);
	test_positive_option(trim_begin_quality, 't', argv);
	set_new_extension(trim_begin_quality, 't', new_extension);
	filtering = 1;
        break;
      case 'T':
	trim_end_quality = atoi(optarg);
	test_positive_option(trim_end_quality, 'T', argv);
	set_new_extension(trim_end_quality, 'T', new_extension);
	filtering = 1;
        break;
      case 'L':
	minimum_read_length = atoi(optarg);
	test_positive_option(minimum_read_length, 'L', argv);
	set_new_extension(minimum_read_length, 'L', new_extension);
	filtering = 1;
        break;
      case '?':
	print_usage(argv);
	return(-1);
	break;
    }
  }
  // Check that input files are provided
  if ((single_file == NULL) && ((paired_file_1 == NULL) || (paired_file_2 == NULL)))
  {
    fprintf(stderr, "error: Files are required as input\n");
    print_usage(argv);
    exit(2);
  }
  // Check that appropriate arguments are provided for quality filtering
  if (length_Qpercent != 0)
  {
    if (goodQ_threshold == 0)
    {
      fprintf(stderr, "error: option -g: goodQ_threshold required for quality filtering\n");
      exit(3);
    }
  }
  if (badQ_threshold != 0)
  {
    if (max_badQ_nucleotides == 0)
    {
      fprintf(stderr, "error: option -m: max_badQ_nucleotides required for quality filtering\n");
      exit(4);
    }
  }
  if (max_badQ_nucleotides != 0)
  {
    if (badQ_threshold == 0)
    {
      fprintf(stderr, "error: option -b: badQ_threshold required for quality filtering\n");
      exit(5);
    }
  }
  if (consecutive_nucleotides != 0)
  {
    if (goodQ_threshold == 0)
    {
      fprintf(stderr, "error: option -g: goodQ_threshold required for quality filtering\n");
      exit(6);
    }
  }

  
    
  /* Process single end file */
  if (single_end == 1)
  {
    // Open input file
    single_fd = gzopen(single_file, "r");
    if (!single_fd)
    {
      fprintf(stderr, "error: Unable to open file %s\n", single_file);
      exit(7);
    }
    // Set output file name with new extension and open it
    if (filtering == 1)
    {
      strcat(new_extension, ".fq");
      output_name = (char*)(malloc(strlen(file_name) + strlen(new_extension) + 1));
      check_memory_allocation(output_name);
      sprintf(output_name, "%s%s", file_name, new_extension);
      out_single_fd = fopen(output_name, "w");
      if (!out_single_fd)
      {
	fprintf(stderr, "error: Unable to open file %s\n", output_name);
	exit(8);
      }
      printf("Results available in the file: %s\n", output_name);
    }

    // Initialise read
    read = kseq_init(single_fd);
    
    // Parse input file to calculate the number of reads and maximum read length if description is required.
    if ( (description == 1) || (short_description == 1) )
    {
      // Set name of description file and open it
      description_file = (char*)(malloc(strlen(file_name) + 17));
      check_memory_allocation(description_file);
      sprintf(description_file, "%s_description.txt", file_name);
      description_fd = fopen(description_file, "w");
      if (!description_fd)
      {
	fprintf(stderr, "error: Unable to open file %s\n", description_file);
	exit(8);
      }
      printf("Description of the reads available in the file: %s\n", description_file);
      // Calculate number of reads, minimum, maximum and average read length
      count_read_length(read, &nb_reads, &min_read_length, &max_read_length, &average_read_length);
      // Reinitialise values to read input file once again from the beginning
      gzseek(single_fd, 0, 0);
      kseq_rewind(read);
      // Allocate memory for variables describing reads before filtering
      if (description == 1)
      {
	quality_position = calloc(max_read_length, sizeof(char*));
	check_memory_allocation(quality_position);
	for (i = 0; i < max_read_length; i++)
	{
	  quality_position[i] = calloc(nb_reads + 1, sizeof(char));
	  check_memory_allocation(quality_position[i]);
	}
      }
      min_quality = calloc(max_read_length, sizeof(int*));
      check_memory_allocation(min_quality);
      max_quality = calloc(max_read_length, sizeof(int*));
      check_memory_allocation(max_quality);
      sum_quality = calloc(max_read_length, sizeof(unsigned long*));
      check_memory_allocation(sum_quality);
      base_composition = calloc(ALPHABET_SIZE, sizeof(int*));
      check_memory_allocation(base_composition);
      base_proportion = calloc(ALPHABET_SIZE, sizeof(int*));
      check_memory_allocation(base_proportion);
      for (i = 0; i < ALPHABET_SIZE; i++)
      {
	base_composition[i] = calloc(max_read_length, sizeof(int));
	check_memory_allocation(base_composition[i]);
	base_proportion[i] = calloc(max_read_length, sizeof(int));
	check_memory_allocation(base_proportion[i]);
      }
      count_poly_reads = calloc(ALPHABET_SIZE, sizeof(int));
      check_memory_allocation(count_poly_reads);
      // Allocate memory for variables describing reads after filtering
      if (filtering == 1)
      {
	min_read_length_filter = max_read_length;
	if (description == 1)
	{
	  quality_position_filter = calloc(max_read_length, sizeof(char*));
	  check_memory_allocation(quality_position_filter);
	  for (i = 0; i < max_read_length; i++)
	  {
	    quality_position_filter[i] = calloc(nb_reads + 1, sizeof(char));
	    check_memory_allocation(quality_position_filter[i]);
	  }
	}
	min_quality_filter = calloc(max_read_length, sizeof(int*));
	check_memory_allocation(min_quality_filter);
	max_quality_filter = calloc(max_read_length, sizeof(int*));
	check_memory_allocation(max_quality_filter);
	sum_quality_filter = calloc(max_read_length, sizeof(unsigned long*));
	check_memory_allocation(sum_quality_filter);
	base_composition_filter = calloc(ALPHABET_SIZE, sizeof(int*));
	check_memory_allocation(base_composition_filter);
	base_proportion_filter = calloc(ALPHABET_SIZE, sizeof(int*));
	check_memory_allocation(base_proportion_filter);
	for (i = 0; i < ALPHABET_SIZE; i++)
	{
	  base_composition_filter[i] = calloc(max_read_length, sizeof(int));
	  check_memory_allocation(base_composition_filter[i]);
	  base_proportion_filter[i] = calloc(max_read_length, sizeof(int));
	  check_memory_allocation(base_proportion_filter[i]);
	}
	count_poly_reads_filter = calloc(ALPHABET_SIZE, sizeof(int));
	check_memory_allocation(count_poly_reads_filter);
      }
    }
    
    // Get each read and pre-process it
    while ((l = kseq_read(read)) >= 0)
    {
      read_name = strdup(read->name.s);
      check_memory_allocation(read_name);
      read_seq = strdup(read->seq.s);
      check_memory_allocation(read_seq);
      read_qual = strdup(read->qual.s);
      check_memory_allocation(read_qual);
      read_length = strlen(read_seq);
      count_reads++;
      passed_read = 1;
      // Convert quality if necessary
      if (convert_quality == 1)
      {
	illumina_ascii2phred_ascii(read_qual, read_length);
      }
      // Desription of reads before filtering if necessary
      if ( (description == 1) || (short_description == 1) )
      {
	check_base_composition(read_seq, base_composition, read_length);
	count_poly(read_seq, count_poly_reads, remove_poly, read_length);
	quality_stats(read_qual, min_quality, max_quality, sum_quality, read_length);
	if (description == 1)
	{
	  check_quality(read_qual, quality_position, read_length, count_reads);
	}
      }

      if (filtering == 1)
      {  
	// Trim begin of read if necessary
	if (trim_begin_quality != 0)
	{
	  // Check position at which the trimmed read should start
	  trim_begin_position = position_trim_begin(read_qual, trim_begin_quality, read_length);
	  // Trim read sequence
	  trim_begin(read_seq, trim_begin_position, read_length);
	  // Trim read quality
	  trim_begin(read_qual, trim_begin_position, read_length);
	  // Modify read length
	  read_length = strlen(read_seq);
	}
	// Trim end of read if necessary
	if (trim_end_quality != 0)
	{
	  // Check position at which the trimmed read should stop
	  trim_end_position = position_trim_end(read_qual, trim_end_quality, read_length);
	  // Trim read sequence
	  trim_end(read_seq, trim_end_position, read_length);
	  // Trim read quality
	  trim_end(read_qual, trim_end_position, read_length);
	  // Modify read length
	  read_length = strlen(read_seq);
	}
	// Assign appropriate default values for filtering reads and check that filtering values are shorter than the length of the read
	if (define_badQ == 0)
	{
	  max_badQ_nucleotides = read_length;
	}
	if (define_rm_Ns != 0)
	{
	  if (remove_Ns <= read_length)
	  {
	    if (count_Ns(read_seq, read_length) >= remove_Ns)
	    {
	      passed_read = 0;
	    }
	  }
	  else
	  {
	    passed_read = 0;
	    fprintf (stderr, "Inappropriate value of parameter -N: remove_Ns for read %s", read_name);
	  }
	}
	if (define_rm_poly != 0)
	{
	  if (remove_poly <= read_length)
	  {
	    if (is_poly(read_seq, remove_poly, read_length) != ' ')
	    {
	      passed_read = 0;
	    }
	  }
	  else
	  {
	    passed_read = 0;
	    fprintf (stderr, "Inappropriate value of parameter -P: remove_poly for read %s", read_name);
	  }
	}
	if (consecutive_nucleotides > read_length)
	{
	  fprintf(stderr, "Inappropriate value of parameter -n: consecutive_nucleotides for read %s\n", read_name);
	}
	else if (max_badQ_nucleotides > read_length)
	{
	  fprintf(stderr, "Inappropriate value of parameter -m: max_badQ_nucleotides for read %s\n", read_name);
	}
	// Print reads that passed filters to output file
	else if ( (read_length >= minimum_read_length) && (read_length > 0) )
	{
	  if (passed_read == 1)
	  {
	    if ( ((count_goodQ_nucleotides(read_qual, goodQ_threshold, read_length)/read_length*100) >= length_Qpercent)
		 && (count_badQ_nucleotides(read_qual, badQ_threshold, read_length) < max_badQ_nucleotides)
		 && (calculate_averageQ(read_qual, read_length) >= averageQ)
		 && (count_goodQ_consecutive(read_qual, goodQ_threshold, consecutive_nucleotides, read_length) == 1) )
	    {
	      print_read_file(out_single_fd, read_name, read_seq, read_qual);
	      count_good_reads++;
	      // Desription of reads after filtering if necessary
	      if ( (description == 1) || (short_description == 1) )
	      {
		min_read_length_filter = min(min_read_length_filter, read_length);
		max_read_length_filter = max(max_read_length_filter, read_length);
		sum_read_length_filter += read_length;
		check_base_composition(read_seq, base_composition_filter, read_length);
		quality_stats(read_qual, min_quality_filter, max_quality_filter, sum_quality_filter, read_length);		
		count_poly(read_seq, count_poly_reads_filter, remove_poly, read_length);
		if (description == 1)
		{
		  check_quality(read_qual, quality_position_filter, read_length, count_good_reads);
		}
	      }
	    }
	  }
	}
      }
      
      // Free read information
      free(read_name);
      read_name = NULL;
      free(read_seq);
      read_seq = NULL;
      free(read_qual);
      read_qual = NULL;
    }      

    // Print description before and after filtering
    if ( (description == 1) || (short_description == 1) )
    {
      fprintf(description_fd, "\t\tDescription of the reads before filtering\n\n");
      fprintf(description_fd, "File processed: %s\n", single_file);
      fprintf(description_fd, "Reads processed: %d\n", count_reads);
      fprintf(description_fd, "\nRead length\nAverage Length\tMinimum Length\tMaximum Length\n");
      fprintf(description_fd, "%.2f\t%d\t%d\n", average_read_length, min_read_length, max_read_length);
      get_proportions(base_composition, base_proportion, max_read_length);
      print_base_composition(base_composition, base_proportion, max_read_length, description_fd);
      if (description == 1)
      {
	for (i = 0; i < max_read_length; i++)
	  quality_position[i][nb_reads] = '\0';
      }
      print_quality(quality_position, min_quality, max_quality, sum_quality, max_read_length, nb_reads, description, description_fd);
      print_poly(count_poly_reads, description_fd);
      if (filtering == 1)
      {
	fprintf(description_fd, "\n\n\t\tDescription of the reads after filtering\n\n");
	fprintf(description_fd, "File processed: %s\n", output_name);
	fprintf(description_fd, "Reads that passed filters: %d\n", count_good_reads);
	fprintf(description_fd, "\nRead length\nAverage Length\tMinimum Length\tMaximum Length\n");
	fprintf(description_fd, "%.2f\t%d\t%d\n", ((float)sum_read_length_filter / count_good_reads), min_read_length_filter, max_read_length_filter);
	get_proportions(base_composition_filter, base_proportion_filter, max_read_length);
	print_base_composition(base_composition_filter, base_proportion_filter, max_read_length, description_fd);
	if (description == 1)
	{
	  for (i = 0; i < max_read_length; i++)
	    quality_position_filter[i][count_good_reads] = '\0';
	}
	print_quality(quality_position_filter, min_quality_filter, max_quality_filter, sum_quality_filter, max_read_length, count_good_reads, description, description_fd);
	print_poly(count_poly_reads_filter, description_fd);
      }
    }
    // Print number of processed and passed reads
    printf("\n%d reads processed\n", count_reads);
    if (filtering == 1)
      printf("%d reads kept\n\n", count_good_reads);
    
    // Free memory
    free(file_name);
    if ( (description == 1) || (short_description == 1) )
    {
      if (description == 1)
      {
	for (i = 0; i < max_read_length; i++)
	{
	  free(quality_position[i]);
	}
	free(quality_position);
      }
      free(min_quality);
      free(max_quality);
      free(sum_quality);
      for (i = 0; i < ALPHABET_SIZE; i++)
      {
	free(base_composition[i]);
	free(base_proportion[i]);
      }
      free(base_composition);
      free(base_proportion);
      free(count_poly_reads);
      free(description_file);
      fclose(description_fd);
      if (filtering == 1)
      {
	if (description == 1)
	{
	  for (i = 0; i < max_read_length; i++)
	  {
	    free(quality_position_filter[i]);
	  }
	  free(quality_position_filter);
	}
	free(min_quality_filter);
	free(max_quality_filter);
	free(sum_quality_filter);
	for (i = 0; i < ALPHABET_SIZE; i++)
	{
	  free(base_composition_filter[i]);
	  free(base_proportion_filter[i]);
	}
	free(base_composition_filter);
	free(base_proportion_filter);
	free(count_poly_reads_filter);
      }
    }
    // Destroy read
    kseq_destroy(read);
    // Close input and output files
    gzclose(single_fd);
    if (filtering == 1)
    {
      free(output_name);
      fclose(out_single_fd);
    }

    time_t t1 = time (NULL);
    printf ("It took %.2lf seconds to compute \n", difftime(t1,t0));
  }


  /* Process paired end files */
  else
  {
    // Open input files
    paired_1_fd = gzopen(paired_file_1, "r");
    if (!paired_1_fd)
    {
      fprintf(stderr, "error: Unable to open file %s\n", paired_file_1);
      exit(7);
    }
    paired_2_fd = gzopen(paired_file_2, "r");
    if (!paired_2_fd)
    {
      fprintf(stderr, "error: Unable to open file %s\n", paired_file_2);
      exit(7);
    }
    // Set output file name with new extension and open it
    if (filtering == 1)
    {
      strcat(new_extension, ".fq");
      output_name_1 = (char*)(malloc(strlen(file_name_1) + strlen(new_extension) + 1));
      check_memory_allocation(output_name_1);
      output_name_2 = (char*)(malloc(strlen(file_name_2) + strlen(new_extension) + 1));
      check_memory_allocation(output_name_2);
      sprintf(output_name_1, "%s%s", file_name_1, new_extension);
      sprintf(output_name_2, "%s%s", file_name_2, new_extension);
      out_paired_1_fd = fopen(output_name_1, "w");
      if (!out_paired_1_fd)
      {
	fprintf(stderr, "error: Unable to open file %s\n", output_name_1);
	exit(8);
      }
      out_paired_2_fd = fopen(output_name_2, "w");
      if (!out_paired_2_fd)
      {
	fprintf(stderr, "error: Unable to open file %s\n", output_name_2);
	exit(8);
      }
      printf("Results available in the files: %s and %s\n", output_name_1, output_name_2);
    }

    // Initialise reads
    read_1 = kseq_init(paired_1_fd);
    read_2 = kseq_init(paired_2_fd);
    
    // Parse input files to calculate the number of reads and maximum read length if description is required
    if ( (description == 1) || (short_description == 1) )
    {
      // Set names of description files and open them
      description_file_1 = (char*)(malloc(strlen(file_name_1) + 17));
      check_memory_allocation(description_file_1);
      description_file_2 = (char*)(malloc(strlen(file_name_2) + 17));
      check_memory_allocation(description_file_2);
      sprintf(description_file_1, "%s_description.txt", file_name_1);
      sprintf(description_file_2, "%s_description.txt", file_name_2);
      description_1_fd = fopen(description_file_1, "w");
      description_2_fd = fopen(description_file_2, "w");
      if (!description_1_fd)
      {
	fprintf(stderr, "error: Unable to open file %s\n", description_file_1);
	exit(8);
      }
      if (!description_2_fd)
      {
	fprintf(stderr, "error: Unable to open file %s\n", description_file_2);
	exit(8);
      }
      printf("Description of the reads available in the files: %s and %s\n", description_file_1, description_file_2);
      // Calculate number of reads, minimum, maximum and average read length
      count_read_length(read_1, &nb_reads_1, &min_read_length_1, &max_read_length_1, &average_read_length_1);
      count_read_length(read_2, &nb_reads_2, &min_read_length_2, &max_read_length_2, &average_read_length_2);
      // Print error message if the number of reads is not the same in the paire end files
      if (nb_reads_1 != nb_reads_2)
      {
	fprintf(stderr, "error: Not same number of reads in files %s and %s\n", paired_file_1, paired_file_2);
      }
      // Reinitialise values to read input file once again from the beginning
      gzseek(paired_1_fd, 0, 0);
      gzseek(paired_2_fd, 0, 0);
      kseq_rewind(read_1);
      kseq_rewind(read_2);
      // Allocate memory for variables describing reads before filtering
      if (description == 1)
      {
	quality_position_1 = calloc(max_read_length_1, sizeof(int*));
	check_memory_allocation(quality_position_1);
	quality_position_2 = calloc(max_read_length_2, sizeof(int*));
	check_memory_allocation(quality_position_2);
	for (i = 0; i < max_read_length_1; i++)
	{
	  quality_position_1[i] = calloc(nb_reads_1 + 1, sizeof(int));
	  check_memory_allocation(quality_position_1[i]);
	}
	for (i = 0; i < max_read_length_2; i++)
	{
	  quality_position_2[i] = calloc(nb_reads_2 + 1, sizeof(int));
	  check_memory_allocation(quality_position_2[i]);
	}
      }
      min_quality_1 = calloc(max_read_length_1, sizeof(int*));
      check_memory_allocation(min_quality_1);
      min_quality_2 = calloc(max_read_length_2, sizeof(int*));
      check_memory_allocation(min_quality_2);
      max_quality_1 = calloc(max_read_length_1, sizeof(int*));
      check_memory_allocation(max_quality_1);
      max_quality_2 = calloc(max_read_length_2, sizeof(int*));
      check_memory_allocation(max_quality_2);
      sum_quality_1 = calloc(max_read_length_1, sizeof(unsigned long*));
      check_memory_allocation(sum_quality_1);
      sum_quality_2 = calloc(max_read_length_2, sizeof(unsigned long*));
      check_memory_allocation(sum_quality_2);
      base_composition_1 = calloc(ALPHABET_SIZE, sizeof(int*));
      check_memory_allocation(base_composition_1);
      base_composition_2 = calloc(ALPHABET_SIZE, sizeof(int*));
      check_memory_allocation(base_composition_2);
      base_proportion_1 = calloc(ALPHABET_SIZE, sizeof(int*));
      check_memory_allocation(base_proportion_1);
      base_proportion_2 = calloc(ALPHABET_SIZE, sizeof(int*));
      check_memory_allocation(base_proportion_2);
      for (i = 0; i < ALPHABET_SIZE; i++)
      {
	base_composition_1[i] = calloc(max_read_length_1, sizeof(int));
	check_memory_allocation(base_composition_1[i]);
	base_composition_2[i] = calloc(max_read_length_2, sizeof(int));
	check_memory_allocation(base_composition_2[i]);
	base_proportion_1[i] = calloc(max_read_length_1, sizeof(int));
	check_memory_allocation(base_proportion_1[i]);
	base_proportion_2[i] = calloc(max_read_length_2, sizeof(int));
	check_memory_allocation(base_proportion_2[i]);
      }
      count_poly_reads_1 = calloc(ALPHABET_SIZE, sizeof(int));
      check_memory_allocation(count_poly_reads_1);
      count_poly_reads_2 = calloc(ALPHABET_SIZE, sizeof(int));
      check_memory_allocation(count_poly_reads_2);
      // Allocate memory for variables describing reads after filtering
      if (filtering == 1)
      {
	min_read_length_filter_1 = max_read_length_1;
	min_read_length_filter_2 = max_read_length_2;
	if (description == 1)
	{
	  quality_position_filter_1 = calloc(max_read_length_1, sizeof(int*));
	  check_memory_allocation(quality_position_filter_1);
	  quality_position_filter_2 = calloc(max_read_length_2, sizeof(int*));
	  check_memory_allocation(quality_position_filter_2);
	  for (i = 0; i < max_read_length_1; i++)
	  {
	    quality_position_filter_1[i] = calloc(nb_reads_1, sizeof(int));
	    check_memory_allocation(quality_position_filter_1[i]);
	  }
	  for (i = 0; i < max_read_length_2; i++)
	  {
	    quality_position_filter_2[i] = calloc(nb_reads_2, sizeof(int));
	    check_memory_allocation(quality_position_filter_2[i]);
	  }
	}
	min_quality_filter_1 = calloc(max_read_length_1, sizeof(int*));
	check_memory_allocation(min_quality_filter_1);
	min_quality_filter_2 = calloc(max_read_length_2, sizeof(int*));
	check_memory_allocation(min_quality_filter_2);
	max_quality_filter_1 = calloc(max_read_length_1, sizeof(int*));
	check_memory_allocation(max_quality_filter_1);
	max_quality_filter_2 = calloc(max_read_length_2, sizeof(int*));
	check_memory_allocation(max_quality_filter_2);
	sum_quality_filter_1 = calloc(max_read_length_1, sizeof(unsigned long*));
	check_memory_allocation(sum_quality_filter_1);
	sum_quality_filter_2 = calloc(max_read_length_2, sizeof(unsigned long*));
	check_memory_allocation(sum_quality_filter_2);
	base_composition_filter_1 = calloc(ALPHABET_SIZE, sizeof(int*));
	check_memory_allocation(base_composition_filter_1);
	base_composition_filter_2 = calloc(ALPHABET_SIZE, sizeof(int*));
	check_memory_allocation(base_composition_filter_2);
	base_proportion_filter_1 = calloc(ALPHABET_SIZE, sizeof(int*));
	check_memory_allocation(base_proportion_filter_1);
	base_proportion_filter_2 = calloc(ALPHABET_SIZE, sizeof(int*));
	check_memory_allocation(base_proportion_filter_2);
	for (i = 0; i < ALPHABET_SIZE; i++)
	{
	  base_composition_filter_1[i] = calloc(max_read_length_1, sizeof(int));
	  check_memory_allocation(base_composition_filter_1[i]);
	  base_composition_filter_2[i] = calloc(max_read_length_2, sizeof(int));
	  check_memory_allocation(base_composition_filter_2[i]);
	  base_proportion_filter_1[i] = calloc(max_read_length_1, sizeof(int));
	  check_memory_allocation(base_proportion_filter_1[i]);
	  base_proportion_filter_2[i] = calloc(max_read_length_2, sizeof(int));
	  check_memory_allocation(base_proportion_filter_2[i]);
	}
	count_poly_reads_filter_1 = calloc(ALPHABET_SIZE, sizeof(int));
	check_memory_allocation(count_poly_reads_filter_1);
	count_poly_reads_filter_2 = calloc(ALPHABET_SIZE, sizeof(int));
	check_memory_allocation(count_poly_reads_filter_2);
      }
    }
    
    // Get each pair of reads and pre-process it
    while ( ((l = kseq_read(read_1)) >= 0) && ((m = kseq_read(read_2)) >= 0) )
    {
      read_name_1 = strdup(read_1->name.s);
      check_memory_allocation(read_name_1);
      read_name_2 = strdup(read_2->name.s);
      check_memory_allocation(read_name_2);
      read_seq_1 = strdup(read_1->seq.s);
      check_memory_allocation(read_seq_1);
      read_seq_2 = strdup(read_2->seq.s);
      check_memory_allocation(read_seq_2);
      read_qual_1 = strdup(read_1->qual.s);
      check_memory_allocation(read_qual_1);
      read_qual_2 = strdup(read_2->qual.s);
      check_memory_allocation(read_qual_2);
      read_length_1 = strlen(read_seq_1);
      read_length_2 = strlen(read_seq_2);
      count_reads++;
      passed_read = 1;
      // Convert quality if necessary
      if (convert_quality == 1)
      {
	illumina_ascii2phred_ascii(read_qual_1, read_length_1);
	illumina_ascii2phred_ascii(read_qual_2, read_length_2);
      }
      // Desription of reads before filtering if necessary
      if ( (description == 1) || (short_description == 1) )
      {
	check_base_composition(read_seq_1, base_composition_1, read_length_1);
	check_base_composition(read_seq_2, base_composition_2, read_length_2);
	count_poly(read_seq_1, count_poly_reads_1, remove_poly, read_length_1);
	count_poly(read_seq_2, count_poly_reads_2, remove_poly, read_length_2);
	quality_stats(read_qual_1, min_quality_1, max_quality_1, sum_quality_1, read_length_1);
	quality_stats(read_qual_2, min_quality_2, max_quality_2, sum_quality_2, read_length_2);
	if (description == 1)
	{
	  check_quality(read_qual_1, quality_position_1, read_length_1, count_reads);
	  check_quality(read_qual_2, quality_position_2, read_length_2, count_reads);
	}
      }
      if (filtering == 1)
      {  
	// Trim begin of read if necessary
	if (trim_begin_quality != 0)
	{
	  // Check position at which the trimmed read should start
	  trim_begin_position_1 = position_trim_begin(read_qual_1, trim_begin_quality, read_length_1);
	  trim_begin_position_2 = position_trim_begin(read_qual_2, trim_begin_quality, read_length_2);
	  // Trim read sequence
	  trim_begin(read_seq_1, trim_begin_position_1, read_length_1);
	  trim_begin(read_seq_2, trim_begin_position_2, read_length_2);
	  // Trim read quality
	  trim_begin(read_qual_1, trim_begin_position_1, read_length_1);
	  trim_begin(read_qual_2, trim_begin_position_2, read_length_2);
	  // Modify read length
	  read_length_1 = strlen(read_seq_1);
	  read_length_2 = strlen(read_seq_2);
	}
	// Trim end of read if necessary
	if (trim_end_quality != 0)
	{
	  // Check position at which the trimmed read should stop
	  trim_end_position_1 = position_trim_end(read_qual_1, trim_end_quality, read_length_1);
	  trim_end_position_2 = position_trim_end(read_qual_2, trim_end_quality, read_length_2);
	  // Trim read sequence
	  trim_end(read_seq_1, trim_end_position_1, read_length_1);
	  trim_end(read_seq_2, trim_end_position_2, read_length_2);
	  // Trim read quality
	  trim_end(read_qual_1, trim_end_position_1, read_length_1);
	  trim_end(read_qual_2, trim_end_position_2, read_length_2);
	  // Modify read length
	  read_length_1 = strlen(read_seq_1);
	  read_length_2 = strlen(read_seq_2);
	}
	// Assign appropriate default values for filtering reads and check that filtering values are shorter than the length of the read
	if (define_badQ == 0)
	{
	  max_badQ_nucleotides = max(read_length_1, read_length_2);
	}
	if (define_rm_Ns != 0)
	{
	  if (remove_Ns <= max(read_length_1, read_length_2))
	  {
	    if ( (count_Ns(read_seq_1, read_length_1) >= remove_Ns) && (count_Ns(read_seq_2, read_length_2) >= remove_Ns) )
	    {
	      passed_read = 0;
	    }
	  }
	  else
	  {
	    passed_read = 0;
	    fprintf (stderr, "Inappropriate value of parameter -N: remove_Ns for reads %s and %s\n", read_name_1, read_name_2);
	  }
	}
	if (define_rm_poly != 0)
	{
	  if (remove_poly <= max(read_length_1, read_length_2))
	  {
	    if ( (is_poly(read_seq_1, remove_poly, read_length_1) != ' ') || (is_poly(read_seq_2, remove_poly, read_length_2) != ' ') )
	    {
	      passed_read = 0;
	    }
	  }
	  else
	  {
	    passed_read = 0;
	    fprintf (stderr, "Inappropriate value of parameter -P: remove_poly for read %s and %s\n", read_name_1, read_name_2);
	  }
	}
	if (consecutive_nucleotides > max(read_length_1, read_length_2))
	{
	  fprintf(stderr, "Inappropriate value of parameter -n: consecutive_nucleotides for read %s and %s\n", read_name_1, read_name_2);
	}
	else if (max_badQ_nucleotides > max(read_length_1, read_length_2))
	{
	  fprintf(stderr, "Inappropriate value of parameter -m: max_badQ_nucleotides for read %s and %s\n", read_name_1, read_name_2);
	}
	// Print reads that passed filters to output file
	else if ( (read_length_1 >= minimum_read_length) && (read_length_2 >= minimum_read_length) )
	{
	  if (passed_read == 1)
	  {
	    if ( ((count_goodQ_nucleotides(read_qual_1, goodQ_threshold, read_length_1)/read_length_1*100) >= length_Qpercent)
		 && ((count_goodQ_nucleotides(read_qual_2, goodQ_threshold, read_length_2)/read_length_2*100) >= length_Qpercent)
		 && (count_badQ_nucleotides(read_qual_1, badQ_threshold, read_length_1) < max_badQ_nucleotides)
		 && (count_badQ_nucleotides(read_qual_2, badQ_threshold, read_length_2) < max_badQ_nucleotides)
		 && (calculate_averageQ(read_qual_1, read_length_1) >= averageQ)
		 && (calculate_averageQ(read_qual_2, read_length_2) >= averageQ)
		 && (count_goodQ_consecutive(read_qual_1, goodQ_threshold, consecutive_nucleotides, read_length_1) == 1)
		 && (count_goodQ_consecutive(read_qual_2, goodQ_threshold, consecutive_nucleotides, read_length_2) == 1) )
	    {
	      print_read_file(out_paired_1_fd, read_name_1, read_seq_1, read_qual_1);
	      print_read_file(out_paired_2_fd, read_name_2, read_seq_2, read_qual_2);
	      count_good_reads++;
	      // Desription of reads after filtering if necessary
	      if ( (description == 1) || (short_description == 1) )
	      {
		min_read_length_filter_1 = min(min_read_length_filter_1, read_length_1);
		min_read_length_filter_2 = min(min_read_length_filter_2, read_length_2);
		max_read_length_filter_1 = max(max_read_length_filter_1, read_length_1);
		max_read_length_filter_2 = max(max_read_length_filter_2, read_length_2);
		sum_read_length_filter_1 += read_length_1;
		sum_read_length_filter_2 += read_length_2;
		check_base_composition(read_seq_1, base_composition_filter_1, read_length_1);
		check_base_composition(read_seq_2, base_composition_filter_2, read_length_2);
		count_poly(read_seq_1, count_poly_reads_filter_1, remove_poly, read_length_1);
		count_poly(read_seq_2, count_poly_reads_filter_2, remove_poly, read_length_2);
		quality_stats(read_qual_1, min_quality_filter_1, max_quality_filter_1, sum_quality_filter_1, read_length_1);
		quality_stats(read_qual_2, min_quality_filter_2, max_quality_filter_2, sum_quality_filter_2, read_length_2);
		if (description == 1)
		{
		  check_quality(read_qual_1, quality_position_filter_1, read_length_1, count_good_reads);
		  check_quality(read_qual_2, quality_position_filter_2, read_length_2, count_good_reads);
		}
	      }
	    }
	  }
	}
      }
      
      // Free reads information
      free(read_name_1);
      free(read_name_2);
      read_name_1 = NULL;
      read_name_2 = NULL;
      free(read_seq_1);
      free(read_seq_2);
      read_seq_1 = NULL;
      read_seq_2 = NULL;
      free(read_qual_1);
      free(read_qual_2);
      read_qual_1 = NULL;
      read_qual_2 = NULL;
    }

    // Print description before and after filtering
    if ( (description == 1) || (short_description == 1) ) 
    {
      fprintf(description_1_fd, "\t\tDescription of the reads before filtering\n\n");
      fprintf(description_1_fd, "File processed: %s\n", paired_file_1);
      fprintf(description_1_fd, "Reads processed: %d\n", nb_reads_1);
      fprintf(description_1_fd, "\nRead length\nAverage Length\tMinimum Length\tMaximum Length\n");
      fprintf(description_1_fd, "%.2f\t%d\t%d\n", average_read_length_1, min_read_length_1, max_read_length_1);
      get_proportions(base_composition_1, base_proportion_1, max_read_length_1);
      print_base_composition(base_composition_1, base_proportion_1, max_read_length_1, description_1_fd);
      if (description == 1)
      {
	for (i = 0; i < max_read_length_1; i++)
	  quality_position_1[i][nb_reads_1] = '\0';
      }
      print_quality(quality_position_1, min_quality_1, max_quality_1, sum_quality_1, max_read_length_1, nb_reads_1, description, description_1_fd);
      print_poly(count_poly_reads_1, description_1_fd);
      fprintf(description_2_fd, "\t\tDescription of the reads before filtering\n\n");
      fprintf(description_2_fd, "File processed: %s\n", paired_file_2);
      fprintf(description_2_fd, "Reads processed: %d\n", nb_reads_2);
      fprintf(description_2_fd, "\nRead length\nAverage Length\tMinimum Length\tMaximum Length\n");
      fprintf(description_2_fd, "%.2f\t%d\t%d\n", average_read_length_2, min_read_length_2, max_read_length_2);
      get_proportions(base_composition_2, base_proportion_2, max_read_length_2);
      print_base_composition(base_composition_2, base_proportion_2, max_read_length_2, description_2_fd);
      if (description == 1)
      {
	for (i = 0; i < max_read_length_2; i++)
	  quality_position_2[i][nb_reads_2] = '\0';
      }
      print_quality(quality_position_2, min_quality_2, max_quality_2, sum_quality_2, max_read_length_2, nb_reads_2, description, description_2_fd);
      //print_quality(quality_position_2, max_read_length_2, nb_reads_2, description_2_fd);
      print_poly(count_poly_reads_2, description_2_fd);
      if (filtering == 1)
      {
	fprintf(description_1_fd, "\n\n\t\tDescription of the reads after filtering\n\n");
	fprintf(description_1_fd, "File processed: %s\n", output_name_1);
	fprintf(description_1_fd, "Reads that passed filters: %d\n", count_good_reads);
	fprintf(description_1_fd, "\nRead length\nAverage Length\tMinimum Length\tMaximum Length\n");
	fprintf(description_1_fd, "%.2f\t%d\t%d\n", ((float)sum_read_length_filter_1 / count_good_reads), min_read_length_filter_1, max_read_length_filter_1);
	get_proportions(base_composition_filter_1, base_proportion_filter_1, max_read_length_1);
	print_base_composition(base_composition_filter_1, base_proportion_filter_1, max_read_length_1, description_1_fd);
	if (description == 1)
	{
	  for (i = 0; i < max_read_length_filter_1; i++)
	    quality_position_filter_1[i][count_good_reads] = '\0';
	}
	print_quality(quality_position_filter_1, min_quality_filter_1, max_quality_filter_1, sum_quality_filter_1, max_read_length_1, count_good_reads, description, description_1_fd);
	//print_quality(quality_position_filter_1, max_read_length_1, count_good_reads, description_1_fd);
	print_poly(count_poly_reads_filter_1, description_1_fd);
	fprintf(description_2_fd, "\n\n\t\tDescription of the reads after filtering\n\n");
	fprintf(description_2_fd, "File processed: %s\n", output_name_1);
	fprintf(description_2_fd, "Reads that passed filters: %d\n", count_good_reads);
	fprintf(description_2_fd, "\nRead length\nAverage Length\tMinimum Length\tMaximum Length\n");
	fprintf(description_2_fd, "%.2f\t%d\t%d\n", ((float)sum_read_length_filter_2 / count_good_reads), min_read_length_filter_2, max_read_length_filter_2);
	get_proportions(base_composition_filter_1, base_proportion_filter_1, max_read_length_1);
	get_proportions(base_composition_filter_2, base_proportion_filter_2, max_read_length_2);
	print_base_composition(base_composition_filter_2, base_proportion_filter_2, max_read_length_2, description_2_fd);
	if (description == 1)
	{
	  for (i = 0; i < max_read_length_filter_2; i++)
	    quality_position_filter_2[i][count_good_reads] = '\0';
	}
	print_quality(quality_position_filter_2, min_quality_filter_2, max_quality_filter_2, sum_quality_filter_2, max_read_length_2, count_good_reads, description, description_2_fd);
	//print_quality(quality_position_filter_2, max_read_length_2, count_good_reads, description_2_fd);
	print_poly(count_poly_reads_filter_2, description_2_fd);
      }
    }
    // Print number of processed and passed reads
    printf("\n%d reads processed\n", count_reads);
    if (filtering == 1)
      printf("%d reads kept\n\n", count_good_reads);
    
    // Free memory
    free(file_name_1);
    free(file_name_2);
    if ( (description == 1) || (short_description == 1) )
    {
      if (description == 1)
      {
	for (i = 0; i < max_read_length_1; i++)
	{
	  free(quality_position_1[i]);
	}
	for (i = 0; i < max_read_length_2; i++)
	{
	  free(quality_position_2[i]);
	}
	free(quality_position_1);
	free(quality_position_2);
      }
      free(min_quality_1);
      free(min_quality_2);
      free(max_quality_1);
      free(max_quality_2);
      free(sum_quality_1);
      free(sum_quality_2);
      for (i = 0; i < ALPHABET_SIZE; i++)
      {
	free(base_composition_1[i]);
	free(base_composition_2[i]);
	free(base_proportion_1[i]);
	free(base_proportion_2[i]);
      }
      free(base_composition_1);
      free(base_composition_2);
      free(base_proportion_1);
      free(base_proportion_2);
      free(count_poly_reads_1);
      free(count_poly_reads_2);
      free(description_file_1);
      free(description_file_2);
      fclose(description_1_fd);
      fclose(description_2_fd);
      if (filtering == 1)
      {
	if (description == 1)
	{
	  for (i = 0; i < max_read_length_1; i++)
	  {
	    free(quality_position_filter_1[i]);
	  }
	  for (i = 0; i < max_read_length_2; i++)
	  {
	    free(quality_position_filter_2[i]);
	  }
	  free(quality_position_filter_1);
	  free(quality_position_filter_2);
	}
	free(min_quality_filter_1);
	free(min_quality_filter_2);
	free(max_quality_filter_1);
	free(max_quality_filter_2);
	free(sum_quality_filter_1);
	free(sum_quality_filter_2);
	for (i = 0; i < ALPHABET_SIZE; i++)
	{
	  free(base_composition_filter_1[i]);
	  free(base_composition_filter_2[i]);
	  free(base_proportion_filter_1[i]);
	  free(base_proportion_filter_2[i]);
	}
	free(base_composition_filter_1);
	free(base_composition_filter_2);
	free(base_proportion_filter_1);
	free(base_proportion_filter_2);
	free(count_poly_reads_filter_1);
	free(count_poly_reads_filter_2);
      }
    }
    // Destroy read
    kseq_destroy(read_1);
    kseq_destroy(read_2);
    // Close input and output files
    gzclose(paired_1_fd);
    gzclose(paired_2_fd);
    if (filtering == 1)
    {
      free(output_name_1);
      free(output_name_2);
      fclose(out_paired_1_fd);
      fclose(out_paired_1_fd);
    }

    time_t t1 = time (NULL);
    printf ("It took %.2lf seconds to compute \n", difftime(t1,t0));
    
  }


  //
  return 0;
}
