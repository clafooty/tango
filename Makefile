# Makefile for Tango

SHELL=/bin/sh
MAKE = make
INSTALL = install -s -m 755

SUBDIRS = samtools-0.1.18 src/duplex  src/fire  src/snifer
EXECS = duplex dduplex fire snifer
BINDIR = bin
FIND = gfind  # find under linux gfind for macosx 

.PHONY: clean install 

all: rec-all install 

rec-all:
	@echo "Make all in sub"
	@for i in $(SUBDIRS); do echo "make all in $$i...";  (cd $$i; $(MAKE) ); done

install:
	@for i in $(EXECS); do echo "Installing $$i..."; $(FIND) -name $$i -a -type f -exec $(INSTALL) {} $(BINDIR) \; ; done

clean:
	@for i in $(EXECS); do \rm -f $(BINDIR)/$$i; done 
	@for i in $(SUBDIRS); do echo "Clearing in $$i...";  (cd $$i; $(MAKE) clean); done
