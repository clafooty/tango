#!/usr/bin/env python

'''
Functions for the pre-processing of FASTQ files
'''
__author__ = "Erika L. Souche"
__date__ = "Wednesday 11th of August 2010"

import os, sys, operator, math, re




# ---------------------------------------------------------------------------
# CLASSES
# ---------------------------------------------------------------------------

class IlluminaRead:
    def __init__(self, name, seq, qual_ascii, goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality):
        self.name = name
        self.seq = seq
        self.list_nucleotides = [x for x in self.seq]
        self.qual_ascii = qual_ascii
        self.list_qual_ascii = [x for x in self.qual_ascii]
        self.list_qual_number = [phred_ascii2phred(x) for x in self.list_qual_ascii]
        if remove_poly == 0:
            remove_poly = len(self.seq)
        self.poly = is_poly(self.list_nucleotides, remove_poly)
        (self.list_trim_nucleotides, self.list_trim_qual_number) = trim(self.list_nucleotides, self.list_qual_number, trim_begin_quality, trim_end_quality)
        self.trim_seq = "".join(self.list_trim_nucleotides)
        self.list_trim_qual_ascii = [phred2phred_ascii(x) for x in self.list_trim_qual_number]
        self.trim_qual_ascii = "".join(self.list_trim_qual_ascii)
        if len(self.list_trim_qual_number) > 0:
            self.average_qual = sum(self.list_trim_qual_number)/float(len(self.list_trim_qual_number))
            self.min_qual = min(self.list_trim_qual_number)
        else:
            self.average_qual = 0
            self.min_qual = 0
        if (remove_poly == 0) or (remove_poly > len(self.trim_seq)):
            remove_poly = len(self.trim_seq)
        self.trim_poly = is_poly(self.list_trim_nucleotides, remove_poly)
        self.nb_Ns = self.list_trim_nucleotides.count('N')
        self.nb_good_qual = count_goodQ_nucleotides(self.list_trim_qual_number, goodQ_threshold)
        self.nb_bad_qual = count_badQ_nucleotides(self.list_trim_qual_number, badQ_threshold)
        self.nb_good_qual_consecutive = count_goodQ_consecutive(self.list_trim_qual_number, goodQ_threshold)
        

    def __str__(self):
        return '%s\n%s\n+\n%s\n' % (self.name, self.trim_seq, self.trim_qual_ascii)


class IlluminaPair:
    def __init__(self, name_1, name_2, seq_1, seq_2, trim_seq_1, trim_seq_2, remove_poly):
        # PE1
        self.name_1 = name_1
        self.seq_1 = seq_1
        self.list_nucleotides_1 = [x for x in self.seq_1]
        if remove_poly == 0:
            remove_poly = len(self.seq_1)
        self.poly_1 = is_poly(self.list_nucleotides_1, remove_poly)
        self.trim_seq_1 = trim_seq_1
        self.list_trim_nucleotides_1 = [x for x in self.trim_seq_1]
        if (remove_poly == 0) or (remove_poly > len(self.trim_seq_1)):
            remove_poly = len(self.trim_seq_1)
        self.trim_poly_1 = is_poly(self.list_trim_nucleotides_1, remove_poly)
        # PE2
        self.name_2 = name_2
        self.seq_2 = seq_2
        self.list_nucleotides_2 = [x for x in self.seq_2]
        if remove_poly == 0:
            remove_poly = len(self.seq_2)
        self.poly_2 = is_poly(self.list_nucleotides_2, remove_poly)
        self.trim_seq_2 = trim_seq_2
        self.list_trim_nucleotides_2 = [x for x in self.trim_seq_2]
        if (remove_poly == 0) or (remove_poly > len(self.trim_seq_2)):
            remove_poly = len(self.trim_seq_2)
        self.trim_poly_2 = is_poly(self.list_trim_nucleotides_2, remove_poly)
        # Pair
        self.seq = self.seq_1+'\t'+self.seq_2
        self.trim_seq = self.trim_seq_1+'\t'+self.trim_seq_2
        self.poly = self.poly_1+self.poly_2
        self.trim_poly = self.trim_poly_1+self.trim_poly_2




# ---------------------------------------------------------------------------
# FUNCTIONS FOR INPUT FILES
# ---------------------------------------------------------------------------

def read_fastq(fd):
    line = fd.readline().strip()
    while line != '':
        if re.search('^@', line):
            name = line
            line = fd.readline().strip()
            if re.search('^(\w+)', line):
                seq = line
            line = fd.readline().strip()
            if re.search('^\+', line):
                qual_ascii = fd.readline().strip()
        yield (name, seq, qual_ascii)


# ---------------------------------------------------------------------------
# FUNCTIONS
# ---------------------------------------------------------------------------

# Function to convert Illumina ascii quality score to PHRED quality score in ascii
def illumina_ascii2phred_ascii(qual_ascii):
    '''
    return quality from ascii
    '''
    list_qual_ascii = [x for x in qual_ascii]
    list_qual_converted = [ chr(ord(x)-31) for x in list_qual_ascii]
    #return chr( ord(ascii_char)-31 )
    return "".join(list_qual_converted)


# Function to convert PHRED quality score in ascii to PHRED quality score
def phred_ascii2phred(ascii_char):
    '''
    return quality from ascii
    '''
    return ord(ascii_char)-33


# Function to convert PHRED quality score to PHRED quality score in ascii
def phred2phred_ascii(number):
    '''
    return ascii from quality
    '''
    return chr(number+33)


# Function to trim reads
def trim(list_nucleotides, list_qual_number, trim_begin_quality, trim_end_quality):
    start, end = 0, 0
    list_trim_nucleotides = []
    list_trim_qual_number = []
    # Trim begin of read
    position = 0
    if list_qual_number[position] < trim_begin_quality:
        while (position < len(list_nucleotides)) and (list_qual_number[position] < trim_begin_quality):
            start += 1
            position += 1
    # Trim end of read
    position = len(list_nucleotides) - 1
    end = len(list_nucleotides) - 1
    if list_qual_number[position] < trim_end_quality:
        while (position > 0) and (list_qual_number[position] < trim_end_quality):
            end -= 1
            position -= 1
    # Make read
    for x in range (start, end+1):
        list_trim_nucleotides.append(list_nucleotides[x])
        list_trim_qual_number.append(list_qual_number[x])
    return (list_trim_nucleotides, list_trim_qual_number)
 
        

# Function to count the number of same nucleotides in a read
def count_consecutive_nucleotides(list_nucleotides, base):
    nb_consecutive_bases, max_nb_consecutive_bases, position, previous_same_nucleotide = 0, 0, 0, 0
    while position < len(list_nucleotides):
        if list_nucleotides[position] == base:
            if previous_same_nucleotide == 0:
                nb_consecutive_bases = 1
                previous_same_nucleotide = 1
            else:
                nb_consecutive_bases += 1
                previous_same_nucleotide = 1
                if nb_consecutive_bases > max_nb_consecutive_bases:
                    max_nb_consecutive_bases = nb_consecutive_bases
        else:
            nb_consecutive_bases = 0
            previous_same_nucleotide = 0
        position += 1
    return max_nb_consecutive_bases


# Function to check whether a read is a poly A/C/G/T/N
def is_poly(list_nucleotides, remove_poly):
    poly = ''
    if count_consecutive_nucleotides(list_nucleotides, 'A') >= remove_poly:
        poly = 'A'
    elif count_consecutive_nucleotides(list_nucleotides, 'C') >= remove_poly:
        poly = 'C'
    elif count_consecutive_nucleotides(list_nucleotides, 'G') >= remove_poly:
        poly = 'G'
    elif count_consecutive_nucleotides(list_nucleotides, 'T') >= remove_poly:
        poly = 'T'
    elif count_consecutive_nucleotides(list_nucleotides, 'N') >= remove_poly:
        poly = 'N'
    return poly


# Function to count the number of good quality nucleotides contained in a read
def count_goodQ_nucleotides(list_qual_number, goodQ_threshold):
    nb_goodQ = 0
    for qual in list_qual_number:
        if qual >= goodQ_threshold:
            nb_goodQ += 1
    return nb_goodQ

# Function to count the number of bad quality nucleotides contained in a read
def count_badQ_nucleotides(list_qual_number, badQ_threshold):
    nb_badQ = 0
    for qual in list_qual_number:
        if qual <= badQ_threshold:
            nb_badQ += 1
    return nb_badQ


# Function to count the maximum number of good quality consecutive nucleotides contained in a read
def count_goodQ_consecutive(list_qual_number, goodQ_threshold):
    nb_goodQ_consecutive, max_nb_goodQ_consecutive, position, previous_good_position = 0, 0, 0, 0
    while position < len(list_qual_number):
        if list_qual_number[position] >= goodQ_threshold:
            if previous_good_position == 0:
                nb_goodQ_consecutive = 1
                previous_good_position = 1
            elif previous_good_position == position:
                nb_goodQ_consecutive += 1
                previous_good_position = position + 1
                if nb_goodQ_consecutive > max_nb_goodQ_consecutive:
                    max_nb_goodQ_consecutive = nb_goodQ_consecutive
        else:
            nb_goodQ_consecutive = 0
            previous_good_position = 0
        position += 1
    return max_nb_goodQ_consecutive



# Function to calculate the number and type of poly reads
def count_poly_read(poly, count_poly):
    if poly != '':
        if count_poly.has_key(poly):
            count_poly[poly] += 1
        else:
            count_poly[poly] = 1
    return count_poly


# Function to store quality by position
def quality_position_read(list_qual_number, quality_by_position):
    for position in range (0, len(list_qual_number)):
        # Create dictionary containing all the qualities by position
        if quality_by_position.has_key(position):
            quality_by_position[position].append(list_qual_number[position])
        else:
            quality_by_position[position] = [ list_qual_number[position] ]
    return quality_by_position


# Function to count nucleotides by position
def nucleotide_position_read(list_nucleotides, nucleotide_by_position):
    for position in range (0, len(list_nucleotides)):
        # Create dictionary of dictionary containing for each position the number of A, C, G, T and N sequenced
        if nucleotide_by_position.has_key(position):
            if nucleotide_by_position[position].has_key(list_nucleotides[position]):
                nucleotide_by_position[position][list_nucleotides[position]] += 1
            else:
                nucleotide_by_position[position][list_nucleotides[position]] = 1
        else:
            nucleotide_by_position[position] = {}
            nucleotide_by_position[position][list_nucleotides[position]] = 1
    return nucleotide_by_position



# Function to store unique reads in dictionary and check which reads are unique
def is_unique_read(seq, unique_reads):
    is_unique = 0
    if unique_reads.has_key(seq):
        unique_reads[seq] += 1
        is_unique = 0
    else:
        unique_reads[seq] = 1
        is_unique = 1
    return (unique_reads, is_unique)



# ---------------------------------------------------------------------------
# FUNCTIONS TO PRINT RESULTS
# ---------------------------------------------------------------------------


# Function to print number of poly reads
def print_poly(count_poly, output_description_fd):
    output_description_fd.write("\nNumber of poly A/C/G/T/N\nPoly\tNumber of poly\n")
    for nucleotide in sorted (count_poly.keys()):
        output_description_fd.write('%s\t%d\n' % (nucleotide, count_poly[nucleotide]))


# Function to calculate and print average quality by position
def print_average_quality(quality_by_position,output_description_fd):
    output_description_fd.write("\nAverage quality by position\nPosition\tAverage Quality\tMinimum Quality\tMaximum Quality\n")
    for position in sorted (quality_by_position.keys()):
        #print quality_by_position[position]
        output_description_fd.write('%s\t%f\t%d\t%d\n' % (position+1, sum(quality_by_position[position])/float(len(quality_by_position[position])), min(quality_by_position[position]), max(quality_by_position[position])))


# Function to print base composition by position
def print_base_composition(nucleotide_by_position, output_description_fd):
    output_description_fd.write("\nBase composition of the reads by position\nPosition\tA\tC\tG\tT\tN\t% A\t% C\t% G\t% T\t% N\n")
    for position in sorted (nucleotide_by_position.keys()):
        print_nucleotide_by_position = {}
        nucleotides = ['A', 'C', 'G', 'T', 'N']
        total_nucleotides = 0
        for nucleotide in nucleotides:
            if nucleotide_by_position[position].has_key(nucleotide):
                print_nucleotide_by_position[nucleotide] = nucleotide_by_position[position][nucleotide]
            else:
                print_nucleotide_by_position[nucleotide] = 0
        total_nucleotides = print_nucleotide_by_position['A'] + print_nucleotide_by_position['C'] + print_nucleotide_by_position['G'] + print_nucleotide_by_position['T'] + print_nucleotide_by_position['N']
        output_description_fd.write('%d\t%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f\n' % (position+1, print_nucleotide_by_position['A'], print_nucleotide_by_position['C'], print_nucleotide_by_position['G'], print_nucleotide_by_position['T'], print_nucleotide_by_position['N'], float(print_nucleotide_by_position['A'])/total_nucleotides*100, float(print_nucleotide_by_position['C'])/total_nucleotides*100, float(print_nucleotide_by_position['G'])/total_nucleotides*100, float(print_nucleotide_by_position['T'])/total_nucleotides*100, float(print_nucleotide_by_position['N'])/total_nucleotides*100))


# Function to count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
def print_copy_number_reads(unique_reads, copy_number, output_description_fd):
    copy_number_reads = {}
    output_description_fd.write('\nReads occurring %d times or more\nCopy number\tReads\n' % copy_number)
    for read in unique_reads.keys():
        if copy_number_reads.has_key(unique_reads[read]):
            copy_number_reads[unique_reads[read]] += 1
        else:
            copy_number_reads[unique_reads[read]] = 1
        if unique_reads[read] >= copy_number:
            output_description_fd.write('%d\t%s\n' % (unique_reads[read], read))
    output_description_fd.write("\nOccurrence\tNumber of reads\n")
    for copy_key in sorted (copy_number_reads.keys()):
        output_description_fd.write('%d\t%d\n' % (copy_key, copy_number_reads[copy_key]))
