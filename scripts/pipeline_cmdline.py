#!/usr/bin/env python

'''
Pipeline for the description and pre-processing of Illumina reads

Description of reads before and after pre-processing
- Count number of poly reads
- Calculate average quality by position
- Calculate base composition by position
- Calculate the number of times reads are duplicated
Pre-processing
- Filter reads according to several quality filters
- Trim begin and/or end of reads
- Filter duplicated reads
- Filter reads with too many Ns
- Filter poly reads
'''

__author__ = "Erika L. Souche"
__date__ = "Wednesday 11th of August 2010"

import sys, traceback, re, os, resource
from optparse import OptionParser
from preprocessing_functions_8 import read_fastq, illumina_ascii2phred_ascii, phred_ascii2phred
from preprocessing_functions_8 import IlluminaRead, IlluminaPair
from preprocessing_functions_8 import count_poly_read, quality_position_read, nucleotide_position_read, is_unique_read
from preprocessing_functions_8 import print_poly, print_average_quality, print_base_composition, print_copy_number_reads



# ---------------------------------------------------------------------------
# MAIN
# ---------------------------------------------------------------------------

def main(argv=None):

    # Define and initialise variables
    single_file, paired_file_1, paired_file_2 = 'undef', 'undef', 'undef'
    paired_files = ()
    convert_quality, remove_duplicates, filterQ = 'False', 'False', 'False'
    goodQ_threshold, length_Qpercent, badQ_threshold, max_badQ_nucleotides, max_badQ_nucleotides_value, averageQ, consecutive_nucleotides, remove_poly, remove_Ns, trim_begin_quality, trim_end_quality, minimum_read_length = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    output_description_file, output_description_after_file, output_single_file, output_paired_file_1, output_paired_file_2 = 'undef', 'undef', 'undef', 'undef', 'undef'
    single_end = 1
    nb_reads, nb_reads_kept = 0, 0
    output_name = ''

    ######
    print os.system('date')

    # Object to parse the command line
    usage = "usage: %prog -s/p single_file.fq / paired_file_1.fq paired_file_2.fq"
    parser = OptionParser(usage=usage)
    parser.add_option("-s", "--single-file", dest="single_file", default="undef", help="Name of the single-end fastq file (-s single_file.fq")
    parser.add_option("-p", "--paired-files", dest="paired_files", nargs=2, default=(), help="Names of the two paired-end fastq files (-p paired_file_1.fq paired_file_2.fq)")
    parser.add_option("-d", "--describe_reads", dest="describe_reads", action="store_true", help="Describe the reads: calculation of base composition by position, calculation of minimum, maximum and average quality by position...")
    parser.add_option("-g", "--goodQ-threshold", dest="goodQ_threshold", default=0, type="int", help="Good quality threshold (bases with a quality lower than this value will be seen as low quality bases). If this option is used without the options -l or -n, only the reads for which all nucleotides have a quality bigger than or equal to this value will be kept. If this option is used with the option -l, the reads for which LENGHT_QPERCENT % of their length has a quality bigger than or equal to this value will be kept. If this option is used with the option -n, the reads for which CONSECUTIVE_NUCLEOTIDES or more nucleotides have a quality bigger than or equal to this value will be kept")
    parser.add_option("-l", "--length-Qpercent", dest="length_Qpercent", default=0, type="int", help="Percentage of read length that has to be of quality bigger than or equal to GOODQ_THRESHOLD for a read to be seen as a good quality read. This option requires the option -g")
    parser.add_option("-b", "--badQ-threshold", dest="badQ_threshold", default=0, type="int", help="Bad quality threshold (bases with a quality lower than this value will be seen as bad quality bases). This option requires the option -m")
    parser.add_option("-m", "--max-badQ-nucleotides", dest="max_badQ_nucleotides", default=0, type="int", help="Maximum number of nucleotides with quality lower than or equal to BADQ_THRESHOLD allowed in a read. This option requires the option -b")
    parser.add_option("-a", "--averageQ", dest="averageQ", default=0, type="int", help="Reads with an average quality bigger than or equal to this value are kept")
    parser.add_option("-n", "--consecutive-nucleotides", dest="consecutive_nucleotides", default=0, type="int", help="Number of consecutive nucleotide bases that have to be of quality bigger than or equal to GOODQ_THRESHOLD for a read to be seen as a good quality read. This option requires the option -g")
    parser.add_option("-c", "--copy-number", dest="copy_number", default=1000, type="int", help="Reads occurring more than COPY_NUMBER times are displayed")
    parser.add_option("-C", "--convert-quality", dest="convert_quality", action="store_true", help="Quality values from Illumina pipeline v1.2 or higher have to be converted in PHRED quality values")
    parser.add_option("-D", "--remove-duplicates", dest="remove_duplicates", action="store_true", help="Remove duplicated reads (only keeps one instance of duplicated reads)")
    parser.add_option("-P", "--remove-poly", dest="remove_poly", default=0, type="int", help="Remove reads containing the same nucleotide (A, C, G or T) REMOVE_POLY times or more")
    parser.add_option("-N", "--remove-Ns", dest="remove_Ns", default=0, type="int", help="Remove reads containing Ns REMOVE_NS times or more")
    parser.add_option("-t", "--trim-begin-quality", dest="trim_begin_quality", default=0, type="int", help="Quality trimming threshold for the first bases of reads (All nucleotides of the begin of the reads with a quality lower than this threshold are trimmed)")
    parser.add_option("-T", "--trim-end-quality", dest="trim_end_quality", default=0, type="int", help="Quality trimming threshold for the last bases of reads (All nucleotides of the end of the reads with a quality lower than this threshold are trimmed)")
    parser.add_option("-L", "--minimum-read-length", dest="minimum_read_length", default=0, type="int", help="Reads shorter than MINIMUM_READ_LENGTH are discarded")

    # Read and parse the command line
    (options, args) = parser.parse_args()
    single_file, paired_files, describe_reads, convert_quality, goodQ_threshold, length_Qpercent, badQ_threshold, max_badQ_nucleotides, averageQ, consecutive_nucleotides, copy_number, remove_duplicates, remove_poly, remove_Ns, trim_begin_quality, trim_end_quality, minimum_read_length = (options.single_file, options.paired_files, options.describe_reads, options.convert_quality, options.goodQ_threshold, options.length_Qpercent, options.badQ_threshold, options.max_badQ_nucleotides, options.averageQ, options.consecutive_nucleotides, options.copy_number, options.remove_duplicates, options.remove_poly, options.remove_Ns, options.trim_begin_quality, options.trim_end_quality, options.minimum_read_length)

    # Check that arguments are defined and correct
    if (single_file is 'undef') and (paired_files is ()):
        parser.error("option -s/-p: input FASTQ file(s) required")
    if not paired_files is ():
        paired_file_1 = paired_files[0]
        paired_file_2 = paired_files[1]
        single_end = 0

    # Check that appropriate arguments are provided for quality filtering and set output file name
    if goodQ_threshold != 0:
        output_name = '%s_g%d' % (output_name, goodQ_threshold)
    if length_Qpercent != 0:
        output_name = '%s_l%d' % (output_name, length_Qpercent)
        if goodQ_threshold == 0:
            parser.error("option -g: goodQ_threshold required for quality filtering")
    if badQ_threshold != 0:
        output_name = '%s_b%d' % (output_name, badQ_threshold)
        if max_badQ_nucleotides == 0:
            parser.error("option -m: max_badQ_nucleotides required for quality filtering")
    if max_badQ_nucleotides != 0:
        output_name = '%s_m%d' % (output_name, max_badQ_nucleotides)
        if badQ_threshold == 0:
            parser.error("option -b: badQ_threshold required for quality filtering")
    if averageQ != 0:
        output_name = '%s_a%d' % (output_name, averageQ)
    if consecutive_nucleotides != 0:
        output_name = '%s_n%d' % (output_name, consecutive_nucleotides)
        if goodQ_threshold == 0:
            parser.error("option -g: goodQ_threshold required for quality filtering")
    if trim_begin_quality != 0:
        output_name = '%s_t%d' % (output_name, trim_begin_quality)
    if trim_end_quality != 0:
        output_name = '%s_T%d' % (output_name, trim_end_quality)
    if minimum_read_length != 0:
        output_name = '%s_L%d' % (output_name, minimum_read_length)
    # Check which reads have to be removed
    if convert_quality:
        output_name = '%s_C' % output_name
    if remove_duplicates:
        output_name = '%s_D' % output_name
    if remove_poly != 0:
        output_name = '%s_P%d' % (output_name, remove_poly)
    if remove_Ns != 0:
        output_name = '%s_N%d' % (output_name, remove_Ns)
    # Check that integer arguments have not any negative values
    if goodQ_threshold < 0:
        parser.error("option -g: invalid value: goodQ_threshold must be bigger than 0")
    if length_Qpercent < 0:
        parser.error("option -l: invalid value: length_Qpercent must be bigger than 0")
    if badQ_threshold < 0:
        parser.error("option -b: invalid value: badQ_threshold must be bigger than 0")
    if max_badQ_nucleotides < 0:
        parser.error("option -m: invalid value: max_badQ_nucleotides must be bigger than 0")
    if averageQ < 0:
        parser.error("option -a: invalid value: averageQ must be bigger than 0")
    if consecutive_nucleotides < 0:
        parser.error("option -n: invalid value: consecutive_nucleotides must be bigger than 0")
    if copy_number < 0:
        parser.error("option -c: invalid value: copy_number must be bigger than 0")
    if remove_poly < 0:
        parser.error("option -P: invalid value: remove_poly must be bigger than 0")
    if remove_Ns < 0:
        parser.error("option -N: invalid value: remove_Ns must be bigger than 0")
    if trim_begin_quality < 0:
        parser.error("option -t: invalid value: trim_begin_quality must be bigger than 0")
    if trim_end_quality < 0:
        parser.error("option -T: invalid value: trim_end_quality must be bigger than 0")
    if minimum_read_length < 0:
        parser.error("option -L: invalid value: minimum_read_length must be bigger than 0")

    try:
        # Process single file
        if (single_end == 1):
            # Declare and initialise variables
            quality_by_position, quality_by_position_after = {}, {}
            nucleotide_by_position, nucleotide_by_position_after = {}, {}
            unique_reads, unique_reads_after = {}, {}
            count_poly, count_poly_after = {}, {}
            no_filtering = 'False'
            is_unique, is_unique_after, print_read = 0, 0, 1
            # Set output files name and open them
            if re.search('\.', single_file):
                file_name = re.match(r"(.+)\.(.+)", single_file)
                output_single_file_name = '%s%s' % (file_name.group(1), output_name)
                output_single_file = '%s.fq' % (output_single_file_name)
                # File for reads in fastq
                if str(output_single_file_name) != file_name.group(1):
                    try:
                        output_single_fd = open (output_single_file, 'w')
                        print "\nReads available in the file: %s\n" % output_single_file
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_single_file
                        sys.exit(1)
                else:
                    no_filtering = 'True'
                    print "\nNo filtering performed"
                # File for reads description before pre-processing, if reads have to be described
                if describe_reads:
                    output_description_file = '%s_description.txt' % file_name.group(1)
                    try:
                        output_description_fd = open (output_description_file, 'w')
                        print "\nDescription of reads before filtering in the file: %s\n" % output_description_file
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_description_file
                        sys.exit(2)
                # File for reads description after pre-processing, if reads have to be described
                if (no_filtering == 'False') and describe_reads:
                    output_description_after_file = '%s_description%s.txt' % (file_name.group(1), output_name)
                    try:
                        output_description_after_fd = open (output_description_after_file, 'w')
                        print "Description of reads after filtering in the file: %s\n" % output_description_after_file
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_description_after_file
                        sys.exit(3)
            # Open the single_end file
            try:
                single_fd = open (single_file, 'r')
            except IOError:
                print 'error: Impossible to open the file %s' % single_file
                sys.exit(4)
            else:
                print "process...."
                # Read sequences and quality values
                while True:
                    try:
                        name, seq, qual_ascii = read_fastq(single_fd).next()
                        nb_reads += 1
                        print_read = 1
                        # Convert quality if necessary
                        if convert_quality:
                            single_read = IlluminaRead(name, seq, illumina_ascii2phred_ascii(qual_ascii), goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality)
                        else:
                            single_read = IlluminaRead(name, seq, qual_ascii, goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality)
                        # Describe reads if necessary
                        if describe_reads:
                            # Count number of poly reads
                            count_poly = count_poly_read(single_read.poly, count_poly)
                            # Store quality by position
                            quality_by_position = quality_position_read(single_read.list_qual_number, quality_by_position)                        
                            # Count nucleotides by position
                            nucleotide_by_position = nucleotide_position_read(single_read.list_nucleotides, nucleotide_by_position)
                            # Store unique reads in dictionary
                            (unique_reads, is_unique) = is_unique_read(single_read.seq, unique_reads)

                        # Print non-filtered reads to output file
                        if (no_filtering == 'False'):
                            # Assign appropriate default values for filtering reads
                            if max_badQ_nucleotides == 0:
                                max_badQ_nucleotides_value = len(single_read.list_trim_nucleotides)
                            else:
                                max_badQ_nucleotides_value = max_badQ_nucleotides
                            # Check that filtering values are shorter than the length of the read
                            if consecutive_nucleotides > len(single_read.list_trim_nucleotides):
                                print_read = 0
                                print 'Inappropriate value of parameter -n: consecutive_nucleotides for read %s' % single_read.name
                            if max_badQ_nucleotides > len(single_read.list_trim_nucleotides):
                                print_read = 0
                                print 'Inappropriate value of parameter -m: max_badQ_nucleotides for read %s' % single_read.name
                            # Check which reads to print
                            if remove_poly != 0:
                                if remove_poly <= len(single_read.list_trim_nucleotides):
                                    if single_read.trim_poly != '':
                                        print_read = 0
                                else:
                                    print_read = 0
                                    print 'Inappropriate value of parameter -P: remove_poly for read %s' % single_read.name
                            if remove_Ns != 0:
                                if remove_Ns <= len(single_read.list_trim_nucleotides):
                                    if single_read.nb_Ns >= remove_Ns:
                                        print_read = 0
                                else:
                                    print_read = 0
                                    print 'Inappropriate value of parameter -N: remove_Ns for read %s' % single_read.name
                            
                            # Print non-filtered reads to output file
                            if (len(single_read.list_trim_nucleotides) >= minimum_read_length) and ((float(single_read.nb_good_qual)/len(single_read.list_trim_nucleotides)*100) >= length_Qpercent) and (single_read.nb_bad_qual < max_badQ_nucleotides_value) and (single_read.average_qual >= averageQ) and (single_read.nb_good_qual_consecutive >= consecutive_nucleotides):
                                # Count unique reads if duplicated should be filtered or reads should be described
                                if describe_reads or remove_duplicates:
                                    # Store unique reads in dictionary
                                    (unique_reads_after, is_unique_after) = is_unique_read(single_read.trim_seq, unique_reads_after)
                                    if remove_duplicates and (is_unique_after == 0):
                                        print_read = 0
                                if print_read == 1:
                                    output_single_fd.write(single_read.__str__())
                                    nb_reads_kept += 1
                                    # Describe reads after pre-processing if necessary
                                    if describe_reads:
                                        # Count number of poly reads
                                        count_poly_after = count_poly_read(single_read.trim_poly, count_poly_after)
                                        # Store quality by position
                                        quality_by_position_after = quality_position_read(single_read.list_trim_qual_number, quality_by_position_after)
                                        # Count nucleotides by position
                                        nucleotide_by_position_after = nucleotide_position_read(single_read.list_trim_nucleotides, nucleotide_by_position_after)
                            # Delete read object
                            del(single_read)

                        # Print every 100000 reads the number of reads processed.
                        #if (nb_reads%1 == 0):
                        if (nb_reads%100000 == 0):
                            print '%s reads processed...' % nb_reads
                            #print os.system('ps ux')
                            #print resource.getrusage(resource.RUSAGE_SELF)
                        
                    except StopIteration:
                        break

                # Close fastq files
                single_fd.close()
                if no_filtering == 'False':
                    output_single_fd.close()
                
                # Print results
                print '\n%s single reads processed' % nb_reads
                if no_filtering == 'False':
                    print '%s single reads kept' % nb_reads_kept
                # Print description of the reads if required
                if describe_reads:
                    output_description_fd.write('File processed: %s\n' % single_file)
                    output_description_fd.write('\n%s single reads processed\n' % nb_reads)
                    # Print number of poly reads
                    print_poly(count_poly, output_description_fd)
                    # Calculate and print average quality by position
                    print_average_quality(quality_by_position, output_description_fd)
                    # Print base composition by position
                    print_base_composition(nucleotide_by_position, output_description_fd)
                    # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                    print_copy_number_reads(unique_reads, copy_number, output_description_fd)
                    # Close description file
                    output_description_fd.close()
                    if no_filtering == 'False':
                        output_description_after_fd.write('File processed: %s\n' % output_single_file)
                        output_description_after_fd.write('\n%s single reads processed\n' % nb_reads_kept)
                        # Print number of poly reads
                        print_poly(count_poly_after, output_description_after_fd)
                        # Calculate and print average quality by position
                        print_average_quality(quality_by_position_after, output_description_after_fd)
                        # Print base composition by position
                        print_base_composition(nucleotide_by_position_after, output_description_after_fd)
                        # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                        print_copy_number_reads(unique_reads_after, copy_number, output_description_after_fd)
                        # Close description file
                        output_description_after_fd.close()
                ######
                print os.system('date')

                
        # Process paired end files
        elif (single_end == 0):
            # Declare and initialise variables
            quality_by_position_1, quality_by_position_2, quality_by_position_1_after, quality_by_position_2_after = {}, {}, {}, {}
            nucleotide_by_position_1, nucleotide_by_position_2, nucleotide_by_position_1_after, nucleotide_by_position_2_after = {}, {}, {}, {}
            unique_reads_1, unique_reads_2, unique_reads_pair, unique_reads_1_after, unique_reads_2_after, unique_reads_pair_after = {}, {}, {}, {}, {}, {}
            is_unique_1, is_unique_2, is_unique_pair, is_unique_1_after, is_unique_2_after, is_unique_pair_after = 0, 0, 0, 0, 0, 0
            count_poly_1, count_poly_2, count_poly_pair, count_poly_1_after, count_poly_2_after, count_poly_pair_after = {}, {}, {}, {}, {}, {}
            print_pair = 1
            no_filtering = 'False'
            # Set output files name and open them
            if re.search('\.', paired_file_1):
                file_name = re.match(r"(.+)\.(.+)", paired_file_1)
                # File for first paired end reads in fastq
                output_paired_file_name = '%s%s' % (file_name.group(1), output_name)
                output_paired_file_1 = '%s.fq' % (output_paired_file_name)
                if str(output_paired_file_name) != file_name.group(1):
                    try:
                        output_paired_1_fd = open (output_paired_file_1, 'w')
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_paired_file_1
                        sys.exit(5)
                else:
                    no_filtering = 'True'
                    print "\nNo filtering performed"
                # File for reads description before pre-processing, if reads have to be described
                if describe_reads:
                    output_description_file = '%s_paired_description.txt' % file_name.group(1)
                    try:
                        output_description_fd = open (output_description_file, 'w')
                        print "\nDescription of reads and pairs of reads before filtering in the file: %s\n" % output_description_file
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_description_file
                        sys.exit(6)
                # File for reads description after pre-processing, if reads have to be described
                if (no_filtering == 'False') and describe_reads:
                    output_description_after_file = '%s_paired_description%s.txt' % (file_name.group(1), output_name)
                    try:
                        output_description_after_fd = open (output_description_after_file, 'w')
                        
                        print "Description of reads and pairs of reads after filtering in the file: %s\n" % output_description_after_file
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_description_after_file
                        sys.exit(7)
            if re.search('\.', paired_file_2):
                file_name = re.match(r"(.+)\.(.+)", paired_file_2)
                # File for second paired end reads in fastq
                output_paired_file_name = '%s%s' % (file_name.group(1), output_name)
                output_paired_file_2 = '%s.fq' % (output_paired_file_name)
                if str(output_paired_file_name) != file_name.group(1):
                    try:
                        output_paired_2_fd = open (output_paired_file_2, 'w')
                        print "\nReads available in the files: %s and %s\n" % (output_paired_file_1, output_paired_file_2)
                    except IOError:
                        print 'error: Impossible to open the file %s' % output_paired_file_2
                        sys.exit(8)
            # Open the paired_end files
            try:
                paired_1_fd = open (paired_file_1, 'r')
            except IOError:
                print 'error: Impossible to open the file %s' % paired_file_1
                sys.exit(9)
            try:
                paired_2_fd = open (paired_file_2, 'r')
            except IOError:
                print 'error: Impossible to open the file %s' % paired_file_2
                sys.exit(10)
            else:
                print "process...."
                # Read sequences and quality values
                while True:
                    try:
                        name_1, seq_1, qual_ascii_1 = read_fastq(paired_1_fd).next()
                        name_2, seq_2, qual_ascii_2 = read_fastq(paired_2_fd).next()
                        nb_reads += 1
                        print_pair = 1
                        # Check that read names are the same for both paired end reads
                        read_name_1 = (re.match(r"(.+)/1", name_1)).group(1)
                        read_name_2 = (re.match(r"(.+)/2", name_2)).group(1)
                        if read_name_1 != read_name_2:
                            print 'error: Reads must be in the same order in both files'
                            sys.exit(11)
                        else:
                            # Convert quality if necessary
                            if convert_quality:
                                paired_read_1 = IlluminaRead(name_1, seq_1, illumina_ascii2phred_ascii(qual_ascii_1), goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality)
                                paired_read_2 = IlluminaRead(name_2, seq_2, illumina_ascii2phred_ascii(qual_ascii_2), goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality)
                            else:
                                paired_read_1 = IlluminaRead(name_1, seq_1, qual_ascii_1, goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality)
                                paired_read_2 = IlluminaRead(name_2, seq_2, qual_ascii_2, goodQ_threshold, badQ_threshold, remove_poly, trim_begin_quality, trim_end_quality)
                            pair = IlluminaPair(name_1, name_2, seq_1, seq_2, paired_read_1.trim_seq, paired_read_2.trim_seq, remove_poly)
                            
                            # Describe reads if necessary
                            if describe_reads:
                                # Count number of poly reads
                                count_poly_1 = count_poly_read(paired_read_1.poly, count_poly_1)
                                count_poly_2 = count_poly_read(paired_read_2.poly, count_poly_2)
                                count_poly_pair = count_poly_read(pair.poly, count_poly_pair)
                                # Store quality by position (dictionary containing all the qualities by position)
                                quality_by_position_1 = quality_position_read(paired_read_1.list_qual_number, quality_by_position_1)
                                quality_by_position_2 = quality_position_read(paired_read_2.list_qual_number, quality_by_position_2)
                                # Count nucleotides by position (dictionary of dictionary containing for each position the number of A, C, G, T and N sequenced)
                                nucleotide_by_position_1 = nucleotide_position_read(paired_read_1.list_nucleotides, nucleotide_by_position_1)
                                nucleotide_by_position_2 = nucleotide_position_read(paired_read_2.list_nucleotides, nucleotide_by_position_2)
                                # Store unique reads and pairs of reads in dictionary
                                (unique_reads_1, is_unique_1) = is_unique_read(paired_read_1.seq, unique_reads_1)
                                (unique_reads_2, is_unique_2) = is_unique_read(paired_read_2.seq, unique_reads_2)
                                (unique_reads_pair, is_unique_pair) = is_unique_read(pair.seq, unique_reads_pair)

                        # Print filtered reads to output file
                        if (no_filtering == 'False'):
                            # Assign appropriate default values for filtering reads
                            if max_badQ_nucleotides == 0:
                                max_badQ_nucleotides_value = max(len(paired_read_1.list_trim_nucleotides), len(paired_read_2.list_trim_nucleotides))
                            else:
                                max_badQ_nucleotides_value = max_badQ_nucleotides
                            # Check that filtering values are shorter than the length of the read
                            if consecutive_nucleotides > max(len(paired_read_1.list_trim_nucleotides), len(paired_read_2.list_trim_nucleotides)):
                                print_pair = 0
                                print 'Inappropriate value of parameter -n: consecutive_nucleotides for pair %s %s' % (paired_read_1.name, paired_read_2.name)
                            if max_badQ_nucleotides > max(len(paired_read_1.list_trim_nucleotides), len(paired_read_2.list_trim_nucleotides)):
                                print_pair = 0
                                print 'Inappropriate value of parameter -m: max_badQ_nucleotides for pair %s %s' % (paired_read_1.name, paired_read_2.name)
                            # Check which pairs of reads to print
                            if remove_poly != 0:
                                if remove_poly <= max(len(paired_read_1.list_trim_nucleotides), len(paired_read_2.list_trim_nucleotides)):
                                    if paired_read_1.trim_poly != '' or paired_read_2.trim_poly != '':
                                        print_pair = 0
                                else:
                                    print_pair = 0
                                    print 'Inappropriate value of parameter -P: remove_poly for pair %s %s' % (paired_read_1.name, paired_read_2.name)
                            if remove_Ns != 0:
                                if remove_Ns <= max(len(paired_read_1.list_trim_nucleotides), len(paired_read_2.list_trim_nucleotides)):
                                    if paired_read_1.nb_Ns >= remove_Ns or paired_read_1.nb_Ns >= remove_Ns:
                                        print_pair = 0
                                else:
                                    print_pair = 0
                                    print 'Inappropriate value of parameter -N: remove_Ns for pair %s %s' % (paired_read_1.name, paired_read_2.name)

                            # Print non-filtered reads to output file
                            if (len(paired_read_1.list_trim_nucleotides) >= minimum_read_length) and (len(paired_read_2.list_trim_nucleotides) >= minimum_read_length) and ((float(paired_read_1.nb_good_qual)/len(paired_read_1.list_trim_nucleotides)*100) >= length_Qpercent) and ((float(paired_read_2.nb_good_qual)/len(paired_read_2.list_trim_nucleotides)*100) >= length_Qpercent) and (paired_read_1.nb_bad_qual < max_badQ_nucleotides_value) and (paired_read_2.nb_bad_qual < max_badQ_nucleotides_value) and (paired_read_1.average_qual >= averageQ) and (paired_read_2.average_qual >= averageQ) and (paired_read_1.nb_good_qual_consecutive >= consecutive_nucleotides ) and (paired_read_2.nb_good_qual_consecutive >= consecutive_nucleotides):
                                if describe_reads or remove_duplicates:
                                    (unique_reads_pair_after, is_unique_pair_after) = is_unique_read(pair.trim_seq, unique_reads_pair_after)
                                    if remove_duplicates and (is_unique_pair_after == 0):
                                        print_pair = 0
                                if print_pair == 1:
                                    output_paired_1_fd.write(paired_read_1.__str__())
                                    output_paired_2_fd.write(paired_read_2.__str__())
                                    nb_reads_kept += 1
                                    # Describe reads after pre-processing if necessary
                                    if describe_reads:
                                        # Count number of poly reads
                                        count_poly_1_after = count_poly_read(paired_read_1.trim_poly, count_poly_1_after)
                                        count_poly_2_after = count_poly_read(paired_read_2.trim_poly, count_poly_2_after)
                                        count_poly_pair_after = count_poly_read(pair.trim_poly, count_poly_pair_after)
                                        # Store quality by position (dictionary containing all the qualities by position)
                                        quality_by_position_1_after = quality_position_read(paired_read_1.list_trim_qual_number, quality_by_position_1_after)
                                        quality_by_position_2_after = quality_position_read(paired_read_2.list_trim_qual_number, quality_by_position_2_after)
                                        # Count nucleotides by position (dictionary of dictionary containing for each position the number of A, C, G, T and N sequenced)
                                        nucleotide_by_position_1_after = nucleotide_position_read(paired_read_1.list_trim_nucleotides, nucleotide_by_position_1_after)
                                        nucleotide_by_position_2_after = nucleotide_position_read(paired_read_2.list_trim_nucleotides, nucleotide_by_position_2_after)
                                        # Store unique reads and pairs of reads in dictionary
                                        (unique_reads_1_after, is_unique_1_after) = is_unique_read(paired_read_1.trim_seq, unique_reads_1_after)
                                        (unique_reads_2_after, is_unique_2_after) = is_unique_read(paired_read_2.trim_seq, unique_reads_2_after)
                            # Delete read objects
                            del(paired_read_1)
                            del(paired_read_2)
                            del(pair)

                        # Print every 100000 reads the number of reads processed.
                        if (nb_reads%100000 == 0):
                            print '%s pairs of reads processed...' % nb_reads
                            
                    except StopIteration:
                        break

                # Close fastq files
                paired_1_fd.close()
                paired_2_fd.close()
                if no_filtering == 'False':
                    output_paired_1_fd.close()
                    output_paired_2_fd.close()
                
                # Print results
                print '\n%s pairs of reads processed' % nb_reads
                if no_filtering == 'False':
                    print '%s pairs of reads kept' % nb_reads_kept

                # Print description of the reads if required
                if describe_reads:
                    # General results
                    output_description_fd.write('Files processed: %s, %s\n' % (paired_file_1, paired_file_2))
                    output_description_fd.write('\n%s pairs of reads processed\n' % nb_reads)                    
                    # Results for PE1 reads
                    output_description_fd.write("\n\n\tResults of paired-end 1 reads\n")
                    # Print number of poly reads
                    print_poly(count_poly_1, output_description_fd)
                    # Calculate and print average quality by position
                    print_average_quality(quality_by_position_1, output_description_fd)                    
                    # Print base composition by position
                    print_base_composition(nucleotide_by_position_1, output_description_fd)
                    # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                    print_copy_number_reads(unique_reads_1, copy_number, output_description_fd)
                    # Results for PE2 reads
                    output_description_fd.write("\n\n\tResults of paired-end 2 reads\n")
                    # Print number of poly reads
                    print_poly(count_poly_2, output_description_fd)
                    # Calculate and print average quality by position
                    print_average_quality(quality_by_position_2, output_description_fd)                    
                    # Print base composition by position
                    print_base_composition(nucleotide_by_position_2, output_description_fd)
                    # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                    print_copy_number_reads(unique_reads_2, copy_number, output_description_fd)
                    # Results for pairs of reads
                    output_description_fd.write("\n\n\tResults of pairs of reads\n")
                    # Print number of poly reads
                    print_poly(count_poly_pair, output_description_fd)
                    # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                    print_copy_number_reads(unique_reads_pair, copy_number, output_description_fd)
                    # Close description file
                    output_description_fd.close()

                    if no_filtering == 'False':
                        output_description_after_fd.write('Files processed: %s and %s\n' % (output_paired_file_1, output_paired_file_2))
                        output_description_after_fd.write('\n%s single reads processed\n' % nb_reads_kept)
                        # Results for PE1 reads
                        output_description_after_fd.write("\n\n\tResults of paired-end 1 reads\n")
                        # Print number of poly reads
                        print_poly(count_poly_1_after, output_description_after_fd)
                        # Calculate and print average quality by position
                        print_average_quality(quality_by_position_1_after, output_description_after_fd)                    
                        # Print base composition by position
                        print_base_composition(nucleotide_by_position_1_after, output_description_after_fd)
                        # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                        print_copy_number_reads(unique_reads_1_after, copy_number, output_description_after_fd)
                        # Results for PE2 reads
                        output_description_after_fd.write("\n\n\tResults of paired-end 2 reads\n")
                        # Print number of poly reads
                        print_poly(count_poly_2_after, output_description_after_fd)
                        # Calculate and print average quality by position
                        print_average_quality(quality_by_position_2_after, output_description_after_fd)                    
                        # Print base composition by position
                        print_base_composition(nucleotide_by_position_2_after, output_description_after_fd)
                        # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                        print_copy_number_reads(unique_reads_2_after, copy_number, output_description_after_fd)
                        # Results for pairs of reads
                        output_description_after_fd.write("\n\n\tResults of pairs of reads\n")
                        # Print number of poly reads
                        print_poly(count_poly_pair_after, output_description_after_fd)
                        # Count number of copies of all reads (nb of reads occuring once, twice, etc) and print reads occurring more than copy_number times.
                        print_copy_number_reads(unique_reads_pair_after, copy_number, output_description_after_fd)
                        # Close description file
                        output_description_after_fd.close()

                ######
                print os.system('date')

    except:
        traceback.print_exc()


## entry point
if __name__ == '__main__':
    main()
