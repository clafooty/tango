"""Data model used in order to handle biological sequences"""

from libbio import get_by_pos, comp_seq, reverse_seq

## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.


__author__  = "ES&SDD"
__date__    = "Mar  8 dec 2009 15:33:31 CET"
__version__ = "1"
__email__   = "sdeclere@gmail.com"


# ---------------------------------------------------------------------------
# DATA MODEL 
# ---------------------------------------------------------------------------

class Segment:
    '''Segment is a portion of a sequence. 
    '''

    def __init__(self, start, end):
        self._start = start
        self._end   = end
        self.slen   = (self._end-self._start)+1
   
    def __str__(self):
        return '[%d,%d]' % ( self.get_start(), self.get_end() )

    def __cmp__(self, other):
        return cmp(self.get_start(), other.get_start())

    def __len__(self):
        return self.slen # speed up

    def get_start(self):
        return self._start

    def set_start(self, start):
        self._start = start 

    def set_end(self, end):
        self._end = end 
        
    def get_end(self):
        return self._end
    
    def str_loc (self):
        return '[%s:%s]' % (self.get_start(), self.get_end())
## Allen's relations  
    def before(self, other):
        #print '%d->%d' % (self.get_end() ,other.get_start())
        return (self.get_end() < other.get_start()) 

    def meets(self, other):
        return (self.get_end() == other.get_start())
    
    def overlaps(self, other):
        start1, end1 = (self.get_start(), self.get_end())
        start2, end2 = (other.get_start(), other.get_end())
        if (start2 < end1) and (start2 > start1) and (end1 < end2):
            return True
        if (start1 < end2) and (start1 > start2) and (end1 > end2):
            return True
        return False
  
    def starts_with(self, other):
        return ( (self.get_start() == other.get_start()) and 
                 (len(self) != len(other)))

    def finishes_with(self, other):
        return ((self.get_end() == other.get_end()) and
                (len(self) != len(other)))
  
    # boundaries equals not segment equals 
    def equals(self, other):
        return ((self.get_start() == other.get_start()) and
                (self.get_end() == other.get_end())) 
    
    def include(self, other):
        return ((self.get_start() < other.get_start()) and
                (self.get_end() < other.get_end())) 
  
    def intersection(self, other):
        start = max (self._start, other.get_start())
        end   = min (self._end, other.get_end())
        return (start, end)

        
class BioSequence(Segment):
    """ Base class for all biological sequences.  
    """
    def __init__(self, organism, identifier, seq):
        Segment.__init__(self, 1, len(seq) )
        self._seq = seq
        self._organism = organism
        self._id = identifier 
        self._feats = []
        self._mother = None
    
    def __str__(self):
        return str(self._seq)
    
    def get_organism(self):
        return self._organism
        
    def get_id(self):
        return self._id
          
    def get_features(self):
        return self._feats
    
    def set_features(self, feats):
        self._feats = feats
    
    def add_feature(self, feat):
        self._feats.append(feat)
    
    def get_sequence(self):
        return self._seq

    def get_mother(self):
        return self._mother
    
    def set_mother(self, mother):
        self._mother = mother
        
##     # unused code        
##     def cut_seq(self, from_, to_):
##         raise NotImplementedError  

    def cut_string(self, from_, to_, strand):
        raise NotImplementedError  

class DNASequence(BioSequence):
    """ Base class for all double strand DNA sequences.
    """
    
    def __init__(self, organism, identifier, seq, 
                 seq_type='unknown', topo='unknown'):
        BioSequence.__init__(self, organism, identifier, seq)
        self._seq_type = seq_type
        self._topo = topo

    def get_sequence_type(self):
        """Return the type of sequence : {chromosom, plasmid, contig, ...}. 
        """
        return self._seq_type  

    def get_topology(self):
        """Return the topology of sequence : {circular, linear, or unknown}.
        """
        return self._topo

    def get_compinv(self):
        """Return string representation of the complementary strand.
        """
        import libbio
        return libbio.reverse_seq(libbio.comp_seq(self._seq))
    
##     # unused code        
##     def cut_seq(self, from_, to_):
##         """Return a sub DNA-sequence as an object of type DNASequence.
##         """
##         cseq = self.get_sequence()[from_ - 1: to_]
##         ret = DNASequence(self.get_organism(), self.get_id(), cseq,
##                           self.get_sequence_type(), self.get_topology())
##         ret.set_start(from_)
##         ret.set_end(to_)
##         ret.set_mother(self)
##         return ret
   
    def cut_string(self, from_, to_, strand):
        """Return a string representation of sub DNA-sequence as an object
        of type DNASequence.
        """
        if (strand == '+'):
            return self.get_sequence()[from_:to_]
        else:
            return reverse_seq(comp_seq(self.get_sequence()[from_:to_]))

##     # unused code
##     def transcribe(self, from_, to_, strand):
##         raise NotImplementedError
   
    def transalte(self, from_, to_, strand):
        """Translate a portion of the DNA sequence.
        """
        import libbio
        sub_seq = get_by_pos(from_, to_, strand, self.get_sequence())
        aa_seq = libbio.translate(sub_seq)
        return Protein( self.get_organism(), 'IN_SILICO_TRANS_'
                        % self.get_id(), aa_seq)
    
        
class Protein(BioSequence):
    """ Base class for all proteins sequences.
    """    
    
    def __init__(self, organism, identifier, seq):
        BioSequence.__init__(self, organism, identifier, seq)

##     # TODO: implement cut_string    
##     def cut_string(self, from_, to_, strand):
##         pass

## # unused code    
## class RNASequence(BioSequence):
##     """ Base class for all rna sequences.
##     """
##     pass
##     # TODO: implement cut_string    
##     def cut_string(self, from_, to_, strand):
##         pass

            
# ---------------------------------------------------------------------------
# MISC FUNCS 
# ---------------------------------------------------------------------------
def get_feature_by_seq (pos, seq):
    """Return for the features over a the position pos. 
    """
    return get_feature_by_pos(pos, seq.get_features())

def get_feature_by_pos(pos, feats):
    """Return for the features over a the position pos. 
    """
    for feat in feats:
        #print pos, feat.get_start() , feat.get_end()
        if ((feat.get_start() < pos) and (pos <= feat.get_end()) ):
            return feat 
    return None
# ---------------------------------------------------------------------------
