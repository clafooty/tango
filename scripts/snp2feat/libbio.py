## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

import sys 

"""Misc tools able to work with DNA sequence. """

__author__  = "ES&SDD"
__date__    = "Lun 31 aout 2009 15:07"
__version__ = "0.1"
__email__   = "sdeclere@gmail.com"

def gc3_percent(s):
    """ Return GC3 composition of a fragmen.
    """
    seq = s.upper()
    lst_res = []

    # get last nuc of the codon 
    for p in range(2, len(seq), 3):
        lst_res.append(seq[p])
        
    gc = lst_res.count('G') + lst_res.count('C')
    p = float(gc) / float(len(lst_res))
    return p

def comp_seq(seq):
    """ Return the complementary sequence """
    from string import maketrans
    trans = maketrans('AGCTagct', 'TCGAtcga')

    res = seq.translate(trans)
    return res

def reverse_seq(seq):
    """ Reverse a sequence """
    l = list(seq)
    l.reverse()

    res = "".join(l)
    return res

def get_by_pos(start, stop, strand, seq):
    if (strand == '+'):
        return seq[start - 1:stop]
    else:
        return reverse_seq(comp_seq(seq[start - 1:stop]))
    return None

def codons(self): 
    """Return list of codons for the dna string.""" 
    s = self.seq 
    end = len(s) - (len(s) % 3) - 1 
    codons = [s[i:i+3] for i in range(0, end, 3)] 
    return codons  
#
# Retourne le polypeptide correspondant a la sequence 
#
def translate(seq, code_name='std'):
    protein = []        
    gcode = gencode[code_name]
    iupac = ['B', 'D', 'H', 'K', 'M', 'N', 'S', 'R', 'W', 'V', 'Y', 'X']    
    for j in range(0, len(seq), 3):
        codon = seq[j:j + 3]
        if (len (codon) != 3):
            sys.stderr.write ('[Translate] codon incomplet :%s a la position %d /%d\n' % (codon, j, len(seq)) )
            sys.stderr.write ('%s\n' % seq )
            sys.stderr.write ('%s\n' % ''.join(protein) )
            sys.exit(1)
            t = '?'
        elif (codon[0] in iupac or codon[1] in iupac or codon[2] in iupac):
            protein.append('X')
        else: 
            t = gcode[codon.upper()]
            protein.append(t)
    return protein 

def random_DNA_seq(seq_len):
    import random
    return ''.join([ random.choice('ATGC') for x in range(seq_len)] ) 

AA3 = {'A': 'ALA', 'C': 'CYS', 'E': 'GLU', 'D': 'ASP', 'G': 'GLY', 'F': 'PHE', 
       'I': 'ILE', 'H': 'HIS', 'K': 'LYS', 'M': 'MET', 'L': 'LEU', 'N': 'ASN', 
       'Q': 'GLN', 'P': 'PRO', 'S': 'SER', 'R': 'ARG', 'T': 'THR', 'W': 'TRP', 
       'V': 'VAL', 'Y': 'TYR', 'X': 'MIS', '*': 'STOP'}

def iupac2bases(letter):
    """Return the list of possible bases according to IUPAC code.
    """
    iupac_table = {"A":["A"],
                   "T":["T"],
                   "C":["C"],
                   "G":["G"],
                   'M':['A','C'],  
                   'R':['A','G'],  
                   'W':['A','T'],  
                   'S':['C','G'],  
                   'Y':['C','T'],  
                   'K':['G','T'],  
                   'V':['A','C','G'],  
                   'H':['A','C','T'],  
                   'D':['A','G','T'],  
                   'B':['C','G','T'],  
                   'X':['A','C','G','T'],  
                   'N':['A','C','G','T']
                 }
    return iupac_table[letter]

# -- Resources 
code_std = {
    'ATA':'I',    #Isoleucine
    'ATC':'I',    #Isoleucine
    'ATT':'I',    # Isoleucine
    'ATG':'M',    # Methionine
    'ACA':'T',    # Threonine
    'ACC':'T',    # Threonine
    'ACG':'T',    # Threonine
    'ACT':'T',    # Threonine
    'AAC':'N',    # Asparagine
    'AAT':'N',    # Asparagine
    'AAA':'K',    # Lysine
    'AAG':'K',    # Lysine
    'AGC':'S',    # Serine
    'AGT':'S',    # Serine
    'AGA':'R',    # Arginine
    'AGG':'R',    # Arginine
    'CTA':'L',    # Leucine
    'CTC':'L',    # Leucine
    'CTG':'L',    # Leucine
    'CTT':'L',    # Leucine
    'CCA':'P',    # Proline
    'CCC':'P',    # Proline
    'CCG':'P',    # Proline
    'CCT':'P',    # Proline
    'CAC':'H',    # Histidine
    'CAT':'H',    # Histidine
    'CAA':'Q',    # Glutamine
    'CAG':'Q',    # Glutamine
    'CGA':'R',    # Arginine
    'CGC':'R',    # Arginine
    'CGG':'R',    # Arginine
    'CGT':'R',    # Arginine
    'GTA':'V',    # Valine
    'GTC':'V',    # Valine
    'GTG':'V',    # Valine
    'GTT':'V',    # Valine
    'GCA':'A',    # Alanine
    'GCC':'A',    # Alanine
    'GCG':'A',    # Alanine
    'GCT':'A',    # Alanine
    'GAC':'D',    # Aspartic Acid
    'GAT':'D',    # Aspartic Acid
    'GAA':'E',    # Glutamic Acid
    'GAG':'E',    # Glutamic Acid
    'GGA':'G',    # Glycine
    'GGC':'G',    # Glycine
    'GGG':'G',    # Glycine
    'GGT':'G',    # Glycine
    'TCA':'S',    # Serine
    'TCC':'S',    # Serine
    'TCG':'S',    # Serine
    'TCT':'S',    # Serine
    'TTC':'F',    # Phenylalanine
    'TTT':'F',    # Phenylalanine
    'TTA':'L',    # Leucine
    'TTG':'L',    # Leucine
    'TAC':'Y',    # Tyrosine
    'TAT':'Y',    # Tyrosine
    'TAA':'*',    # Stop
    'TAG':'*',    # Stop
    'TGC':'C',    # Cysteine
    'TGT':'C',    # Cysteine
    'TGA':'*',    # Stop
    'TGG':'W',    # Tryptophan
}

gencode = {
           'std':code_std
}
