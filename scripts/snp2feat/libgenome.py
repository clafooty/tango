## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

"""Genomics toolkit. """

__author__  = "ES&SDD"
__date__    = "Jeu  1 avr 2010"
__version__ = "0.1"
__email__   = "sdeclere@gmail.com"

from feat_classes import CDSChunk, TRNAChunk, IntergenicRegion
from genbank import process_genbank_in_directory, GenbankFactory
import sys
import os

def make_intergenic_features(seq):
    """
    From a sequence fully annotated, allocate intergenic regions
    between CDS in order fill all the gaps.
    """
    ret  = []
    cdss = [o for o in seq.get_features() if not isinstance(o, CDSChunk) and not isinstance(o, TRNAChunk)]
    inter_cds = make_list_of_comp_intervals(cdss, len(seq))
    
    for pos in inter_cds: 
        ret.append(IntergenicRegion(pos[0], pos[1], '+', seq, source='genbank') ) 
        
    return ret

def make_list_of_comp_intervals(lst_cds, last_x=0):
    comp_ints  = []
    sorted_cds = sorted(lst_cds, key=lambda interval: interval.get_start(),
                        reverse=True)
    cur = None
    try: 
        cur = sorted_cds.pop()
        # append 0-to first 
        if cur.get_start()>0:
            comp_ints.append( (0, cur.get_start()) )
        while(True):
            next = sorted_cds.pop()
            #print cur.before(next)
            if cur.before(next):
                start = cur.get_end()
                end   = next.get_start()
                comp_ints.append( (start, end) )
            cur = next
    except IndexError:
        if cur is None:
            return []
        if cur.get_end()<last_x:
            comp_ints.append( (cur.get_end(), last_x) )
        return comp_ints
    
    return []

# ---------------------------------------------------------------------------
# TEST
# ---------------------------------------------------------------------------
if __name__ == "__main__":
    if os.path.isdir(sys.argv[1]):
        seqs = process_genbank_in_directory(sys.argv[1], ext='gbf')
    else:
        factory =  GenbankFactory(sys.argv[1])
        seqs = [factory.get_sequence()]
        
    for seq in seqs:
        make_intergenic_features(seq)

