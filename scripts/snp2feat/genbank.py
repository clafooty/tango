""" Functions & classes related to genbank files. """

from Bio import GenBank
## from feat_classes import Gene, Exon, Intron
from feat_classes import LongTerminalRepeat, NonCodingRNA, \
     ReplicationOrigin, TransferRNA, RibosomalRNA, PseudoGene, \
     RepeatRegion, CDS, CDSChunk, TRNAChunk, \
     RibosomalBindingSite, Terminator
from seq_classes import DNASequence
import glob
import os
import sys
import time 

## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## $Id: genbank.py,v 1.6 2010/06/14 09:24:01 screno Exp $
 

__author__  = "ES&SDD"
__date__    = "Mer  9 dec 2009 17:10:32 CET"
__version__ = "1"
__email__   = "sdeclere@gmail.com"


# ---------------------------------------------------------------------------
# time a function using time.time() and the a @ function decorator
# ---------------------------------------------------------------------------

def print_timing(func):
    """ Print the time spent by a funtion given as a parameter """
    def wrapper(*arg):
        t1 = time.clock()
        res = func(*arg)
        t2 = time.clock()
        sys.stderr.write ( '[%s] took %.1f sec\n' % (func.func_name, t2-t1) )
        sys.stdout.flush()
        return res
    return wrapper


def select_id_from_qualifier(qualifiers):
    """ Id retrived from gene or locus_tag if necessary """
    if qualifiers.has_key('gene'):
        return '_'.join(qualifiers['gene'])
    elif  qualifiers.has_key('locus_tag'):
        return '_'.join (qualifiers['locus_tag'])
    else:
        return 'WTF'

# ---------------------------------------------------------------------------
# FUNCTIONS  
# ---------------------------------------------------------------------------
def process_genbank_in_directory(dir_name, ext='gbk', err_fd=sys.stderr):
    """ Retrieve the content of all genbank files in a directory """
    seqs = {}
    for file_name in glob.glob('%s/*.%s' % (dir_name, ext)):
        print >> err_fd, 'Processing : %s \n' % file_name
        factory = GenbankFactory(file_name)
        seq = factory.get_sequence()
        seqs[ seq.get_id() ] = seq
    return seqs

    
# ---------------------------------------------------------------------------
# DATA MODEL 
# ---------------------------------------------------------------------------

class UnhandledFeatureException(Exception):
    pass
    
class IgnoredFeatureException(Exception):
    pass

class GenbankFactory:
    """Factory able to make sequence objects from genbank file
    """
    feature_dict = {'CDS': CDS,  'pseudo': PseudoGene, 
                    'ncRNA': NonCodingRNA, 'tRNA': TransferRNA,
                    'rRNA': RibosomalRNA,  'RBS': RibosomalBindingSite,
                    'rep_origin': ReplicationOrigin, 'terminator': Terminator,
                    'repeat_region': RepeatRegion,   'LTR': LongTerminalRepeat
                    }
    
    def __init__(self, file_):
        if isinstance(file_, file):
            self._filename  = file_.name
            self._gb_handle = file_
        else:
            self._filename  = file_
            self._gb_handle = open (self._filename, 'r')
        self._cache     = None

    def get_sequence(self):
        if self._cache is None:
            self.do_parse()
        return self._cache
    
    def do_parse(self, err_fd=sys.stderr):
        """ Parse a genbank file and create a DNASequence
        with these features 
        """
        parser = GenBank.FeatureParser() 
        record = parser.parse(self._gb_handle)   
        
        sequence = DNASequence(record.annotations['organism'],
                               str(record.id),
                               str(record.seq)) 
        feats    = []
    
        for feature in record.features:
            try:
                entry = self.build(feature, sequence)
                feats.append(entry)
            except IgnoredFeatureException:
                pass
            except UnhandledFeatureException:
                print >> err_fd, 'Unexpected feature type : %s \n' % (feature.type),
                
        # sort & bind features
        feats.sort()

        # bind features to sequence
        sequence.set_features(feats)
        self._cache = sequence
        self._gb_handle.close()
        print >> err_fd, 'Loaded %d feature(s) from : %s \n' \
            % ( len(feats), self._filename )
    
    def build(self, feature, sequence):

        # !! biopython give coordinates from 0 not 1
        loc    = feature.location
        start  = loc.start.position
        end    = loc.end.position
        strand = {-1:'-', 1: '+'}[feature.strand]

        entry  = None
        if feature.type == "misc_feature" or  feature.type == "source":
            raise(IgnoredFeatureException)
        
        if feature.type == "gene":
            
            if not feature.qualifiers.has_key('pseudo'):
                # entry=Gene(start, end, strand, sequence, source='genbank')
                # pass
                # for now genes are not allocated, because they have not
                # biological meanings (just a copycat of CDS!!)
                # This choice'll be changed if I can find some real example
                # where the definition of a gene in an genbank file gives
                # more information than the enumeration of it's compound.
                raise(IgnoredFeatureException)
            
            # current gene is a pseudo gene 
            entry = PseudoGene(start, end, strand, sequence, source='genbank')
                                
        if feature.type == "CDS":
            entry = CDS( start, end, strand, sequence, source='genbank')
            entry.set_id(select_id_from_qualifier(feature.qualifiers))
            if len(feature.sub_features) > 0: 
                for sub_feature in feature.sub_features:
                    loc    = sub_feature.location
                    start  = loc.start.position
                    end    = loc.end.position
                    strand = {-1:'-', 1: '+'}[sub_feature.strand]
                    sub_entry = CDSChunk( start, end, strand, sequence,
                                          source='genbank')
                    sub_entry.update_annotations(feature.qualifiers)
                    sub_entry.set_mother_feature(entry)
                    entry.add_feature(sub_entry)
            try:
                entry.set_product(','.join(
                    entry.get_annotations()['product']))
            except KeyError:
                entry.set_product('NA')
                #entry.set_product(entry.get_annotations()['note'])

        if feature.type == "tRNA":
            
            entry = TransferRNA(start, end, strand, sequence,
                                source='genbank')
            for sub_feature in feature.sub_features:
                loc   = sub_feature.location
                start = loc.start.position
                end   = loc.end.position
                strand = {-1:'-', 1: '+'}[sub_feature.strand]
                sub_entry = TRNAChunk( start, end, strand, sequence,
                                       source='genbank')
                sub_entry.update_annotations(feature.qualifiers)
                sub_entry.set_mother_feature(entry)
                entry.add_feature(sub_entry)
            try:
                entry.set_product(','.join(
                    entry.get_annotations()['product']))
            except KeyError:
                entry.set_product('NA')
                #entry.set_product(entry.get_annotations()['note'])

        if entry is None:
            # it was not one of the previous types
            try:
                entry = GenbankFactory.feature_dict[feature.type](start, end, strand,
                                                                  sequence,
                                                                  source='genbank')
            except KeyError:
                raise(UnhandledFeatureException)
            
        entry.update_annotations(feature.qualifiers)
        return entry

# ---------------------------------------------------------------------------
# TEST
# ---------------------------------------------------------------------------
if __name__ == "__main__":
    seqs = {}
    if os.path.isdir(sys.argv[1]):
        seqs = process_genbank_in_directory(sys.argv[1], ext='gb*')
    else:
        factory = GenbankFactory(sys.argv[1])
        seq     = factory.get_sequence()
        seqs[ seq.get_id() ] = seq
        
