#!/usr/bin/env python

""" Take a list of SNP as input, output informations about variants. """

## $Id: snp2gene.py,v 1.7 2010/06/14 09:24:01 screno Exp $

from feat_classes import CDS, Producer
from genbank import process_genbank_in_directory, GenbankFactory
from libgenome import make_intergenic_features
from libbio import translate, AA3, iupac2bases
from seq_classes import get_feature_by_pos, get_feature_by_seq
from string import maketrans

import getopt
import os
import sys
import traceback

__author__  = "Erika Souche & Stephane Descorps-Declere"
__date__    = "Tue Jun  7 15:42:05 CEST 2010"
__version__ = "0.5"
__email__   = "sdeclere@gmail.com"
__license__ = "Public Domain"

# note : travailler avec des objets serialises

# ---------------------------------------------------------------------------
# GLOBALS 
# ---------------------------------------------------------------------------

INDEL_SIGNS = ('*', '-')

# ---------------------------------------------------------------------------
# CORE FUNCS 
# ---------------------------------------------------------------------------

def gene_variant (gene, pos, char, strand):
    #print  (gene, pos, char, strand)
    """Build a new dna (string) sequence substitution of char
       at the position pos. 
    """
    gene_list = list(gene)
    if (strand == '+'):
        gene_list[pos] = char
    else:
        trans          = maketrans('AGCTagct', 'TCGAtcga')
        _char          = char.translate(trans)
        gene_list[pos] = _char
    return ''.join(gene_list)

def get_mut_pos (snp_pos, start_pos, end_pos, strand):
    """Compute the position of the SNP in a gene,
       given a position in the genome.
       snp_pos are counted from 1 not 0
       (biologist way not computer science). 
    """
    if (strand == '+'):
        return ( (snp_pos-1) - start_pos )
    else:
        # end is excluded for easy use in tables
        return ( (end_pos-1) - (snp_pos-1) )

def get_mut_pos_in_composite_cds(snp_pos, feat):
    """ Compute the position of the SNP in a gene
       containing introns and exons
       given a position in the genome.
       snp_pos are counted from 1 not 0
       (biologist way not computer science). 
    """
    sub_feats   = feat.get_features()
    feat_ex     = None
    feat_ex     = get_feature_by_pos(snp_pos, sub_feats)

    if feat_ex is not None:  # exon
        pos_in_exon = get_mut_pos (snp_pos, feat_ex.get_start(),
                                   feat_ex.get_end(), feat_ex.get_strand())
        
        idx_of_feat = sub_feats.index(feat_ex)
        
        if feat_ex.get_strand() == '+':
            start_range = 0
            step = 1
            feat_type = 'exon-%d' % (idx_of_feat + 1)
        else:
            start_range = len(sub_feats) - 1
            step = -1
            feat_type = 'exon-%d' % ( len(sub_feats) - idx_of_feat )
        mutation_pos = pos_in_exon
        # we add the length of the preceding exons
        for i in range (start_range, idx_of_feat, step):
            mutation_pos += len( sub_feats[i].get_feature_sequence() )
    else: # intron
        num_intron = 0
        if feat.get_strand() == '+':
            for i in xrange (0, len(sub_feats)-1):
                num_intron += 1
                if sub_feats[i].get_end() <= snp_pos \
                       and snp_pos < sub_feats[i+1].get_start():
                    break
        else:
            for i in xrange (0, len(sub_feats)-1):
                num_intron += 1
                if sub_feats[i+1].get_end() <= snp_pos \
                       and snp_pos < sub_feats[i].get_start():
                    break
        feat_type    = 'intron-%d' % num_intron
        mutation_pos = -1
    return mutation_pos, feat_type
        
    
def make_cds_variant(snp_pos, ref_base, call_base, feat):
    ##print feat.get_coding_sequence() , snp_pos, ref_base, call_base, feat
    """Build variant protein, create the string containing
    the information about the differences between reference and variant.
    """
    if feat.have_children():
        mutation_pos, feat_type = get_mut_pos_in_composite_cds(snp_pos, feat)
    else: 
        mutation_pos = get_mut_pos (snp_pos, feat.get_start(),
                                    feat.get_end(), feat.get_strand())
        feat_type = feat.get_feature_type()

    indel_len = 0
    to_print  = 'Gene:%s\t%s\t"%s"' % ( feat.get_id(),
                                        feat_type,
                                        feat.get_strand() )
    if mutation_pos >= 0:
        # real mutation position -> 
        # compute the position of the variant in aa
        gene_seq  = feat.get_feature_sequence()
        pos_in_aa = mutation_pos / 3 
##	print feat  
        aaseq     = translate(gene_seq)
        aaref     = AA3[ aaseq[pos_in_aa] ]
        # we stop here if it's an indel
        if ref_base not in INDEL_SIGNS and call_base[0] not in INDEL_SIGNS:
            # if not, we look for the different variants
            aamut = []
            for callb in call_base:
                mut_gene_seq = gene_variant( gene_seq, mutation_pos,
                                             callb, feat.get_strand() )
                aaseq        = translate(mut_gene_seq)
                aamut.append( AA3[ aaseq[pos_in_aa] ] )
            
            to_print = '%s\t%s\t%s\t%d' % ( to_print,
                                            aaref,
                                            '/'.join(aamut),
                                            pos_in_aa+1 )
        else:
            # it was an indel
            to_print = '%s\t%s\tFrameshift\t%d' % ( to_print,
                                                    aaref,
                                                    pos_in_aa+1 )
            indel_len = ref_base in INDEL_SIGNS and len(call_base[0]) \
                or -1*len(ref_base)
    else:    
        # intron case
        to_print = '%s\t\t' % ( to_print )

    return indel_len, to_print

def get_coverage(chr_name, pos, ref_base, bam_fd, indel_len):
    """ 
    Use of the pysam module to compute the coverage
    at every snp position in the alignment 
    """
    if bam_fd is None:
        return 0, {}

    coverage_dict = {ref_base:0}
    coverage      = 0

    try:
        for pileupcolumn in bam_fd.pileup( chr_name, pos-1, pos-1):
            if pileupcolumn.pos != pos-1: 
                # due to the way it's implemented in pysam
                # if I understood correctly
                # we have to do that to avoid memory leaks
                continue
            coverage = pileupcolumn.n
            for pileupread in pileupcolumn.pileups:
                snp_seq = ''
                if indel_len < 0:
                    if pileupread.indel < 0:
                        # deletion
                        snp_seq = INDEL_SIGNS[0]*abs(pileupread.indel)
                    else:
                        snp_seq = ''.join( [ pileupread.alignment.seq[pileupread.qpos +1 + i].upper() 
                                             for i in xrange(0, abs(indel_len))  
                                             if (pileupread.qpos +1 + i) < len(pileupread.alignment.seq) ] )

                    # case when the indel is at the last position of the read
                    snp_seq = snp_seq == '' and INDEL_SIGNS[0] or snp_seq
                elif indel_len > 0:
                    if pileupread.indel > 0:
                        # insertion
                        snp_seq =  ''.join( [ pileupread.alignment.seq[pileupread.qpos + i ].upper() 
                                              for i in xrange(1, pileupread.indel+1)  ] )
                    else:
                        snp_seq = INDEL_SIGNS[0]*indel_len
                else:
                    # no indel
                    snp_seq = pileupread.alignment.seq[pileupread.qpos].upper()

                coverage_dict.setdefault(snp_seq, 0)
                coverage_dict[snp_seq] += 1
    except ValueError, msg:
        # already handle elsewhere: problem of invalid position
        # or invalid chromosome name
        pass

    return coverage, coverage_dict
            

def make_description(snps, seqs, bam_fd=None):
    """ Create a string of description for each snp/indel:
    chromosome | position | refbase | callbase | feature | \
    aminoacid change | amino acid position
    """
    error_dict = {}
    desc       = ''
    for snp in snps:
        chr_name, pos, refb, callb = snp
        coverage_desc = ''

        # assoc feat - replicon
        feat = None
        try:
            feat = get_feature_by_seq(pos, seqs[chr_name])
        except KeyError:
            error_dict[(chr_name, -1)] = \
                "Chromosome %s not available in the provided annotation files" \
                % (chr_name)
            
        if (feat is not None):
            # case the feature is coding
            if isinstance(feat, CDS):
                indel_len, specif_print = make_cds_variant(pos, refb, 
                                                           callb, feat)
            else:
                specif_print = '%s\t\t\t\t' % ( feat.get_feature_type() )

                if isinstance(feat, Producer):
                    desc = '%s%s' % (desc, '')#feat.get_product())
                    
                indel_len = 0
                if refb in INDEL_SIGNS:
                    indel_len = len(callb[0])
                elif callb[0] in INDEL_SIGNS:
                    indel_len = -1*len(refb)

            desc = '%s%s\t%d\t%s\t%s\t%s' % ( desc, 
                                              feat.get_mother().get_id(), 
                                              pos, refb, '/'.join(callb), 
                                              specif_print )
        else:
            indel_len = 0
            desc = '%s%s\t%d\t%s\t%s\t' % ( desc, 
                                            chr_name, pos, refb, '/'.join(callb))
            if seqs.has_key(chr_name) and (chr_name, pos) not in error_dict:
                error_dict[(chr_name, pos)] = "Unmapped snp: can't associate the snp at position %d with the sequence %s" % (pos, chr_name)


        coverage, base_dict = get_coverage( chr_name, pos, refb, 
                                            bam_fd, indel_len )
        if coverage:
            coverage_desc = '%d\t%s:%d (%d' % ( coverage, 
                                                refb, base_dict[refb], 
                                                100*base_dict[refb]/coverage ) \
                                                + '%)'
            del base_dict[refb]
            aux = [ (val, base) for base, val in base_dict.iteritems() ]
            # sort in descending order of the values
            aux.sort()
            aux.reverse()
            for (val, base) in aux:
                coverage_desc = '%s\t%s:%d (%d' % ( coverage_desc,
                                                    base, val, 
                                                    100*val/coverage ) \
                                                    + '%)'

        desc = '%s\t%s%s' % ( desc, coverage_desc, os.linesep )
                                                     
    return desc, error_dict 


# ---------------------------------------------------------------------------
# USEFUL FUNCS
# ---------------------------------------------------------------------------
    
def usage():
    '''
    Print a short help message, then exit.
    '''
    print ''
    print 'Script  : snp2gene.py (Take a list of SNP as input, \
output list of mutations) '
    print 'Version : %s' % __version__
    print 'Usage   : -g <GBK FILE> -s <SNP FILENAME> [-b <BAM FILE>] [-o <OUTPUT FILE>]'
    print ''
    sys.exit(1)


def parse_cmd_line(argv):
    """ Simple function to parse command line arguments and options """
    (genbank_fn, snp_fn, bam_fn, output_fn) = ('undef', 'undef', 
                                               'undef', 'undef')
     
    try:
        opts, _ = getopt.getopt(argv, "hg:s:b:o:", ["help", "genbank-file",
                                                    "snp-file",  "bam-file", 
                                                    "output-file"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        # help 
        if opt in ("-h", "--help"):
            usage()                     
            sys.exit()
            
        elif opt in ("-g", "--genbank-file"): 
            genbank_fn = arg

        elif opt in ("-s", "--snp-file"): 
            snp_fn = arg

        elif opt in ("-b", "--bam-file"): 
            bam_fn = arg
             
        elif opt in ("-o", "--output-file"): 
            output_fn = arg
 
    return  (genbank_fn, snp_fn, bam_fn, output_fn) 


def snp_file_parser(snp_fd):
    """Load snp location from a simple text (tabular) file. 
    """
    ret = []
    
    for line in snp_fd:
        # replicon, position, ref base, variant
        sline    = line.split()
        if len(sline) < 4 or '' in sline[0:4]:
            # incorrect line
            continue

        variants = {}

        ref_base = sline[2]
        if ref_base in INDEL_SIGNS:
            ref_base = '*'
        alleles = sline[3].split("/")
        
        for nucl in alleles:
            if nucl in INDEL_SIGNS: # case of insert/del a la sam
                                    # -> nothing to do
                                    # or case of del a la CLC
                                    # -> handledafter the for-loop
                variants['*'] = True
            elif nucl.startswith('-'):
                # deletion a la sam
                ref_base      = nucl[1:]
                variants['*'] = True
            elif nucl.startswith('+'):
                # insertion a la sam
                variants[ nucl[1:] ] = True
            elif len(nucl) > 1:
                # multiple insertion other than sam's
                variants[nucl] = True
            else: # snp: handling of the degenerated code or insertion a la CLC
                for vbase in iupac2bases(nucl):
                    variants[vbase]  = True
              
        # in heterozygous cases, put the reference
        # base at the beginning of the list
        ref_list = [] 
        if ref_base in variants:
            ref_list = [ref_base]
            del variants[ref_base]
        if len ( variants.keys() ) :
            # we don't want cases such as NC_001133	16467	A	A
            ret.append( (sline[0], int(sline[1]), ref_base, ref_list + variants.keys()) )

    return ret

# ---------------------------------------------------------------------------
# MAIN
# ---------------------------------------------------------------------------

def main():
    """ Main function
    """
    
    # read & parse system's cmd line 
    (feature_fn, snp_fn, bam_fn, output_fn) =  parse_cmd_line( sys.argv[1:] )
    
    # test arguments not null 
    if (snp_fn is 'undef') :
        usage()
        sys.exit(2)
    if ( (feature_fn is 'undef') and (bam_fn is 'undef') ):
        usage()
        sys.exit(2)
        
    if output_fn != 'undef':
        output_fd = open(output_fn,'w')
    else:
        output_fd = sys.stdout

    # bam file
    bam_fd = None    
    if bam_fn != 'undef':
        try:
            import pysam
            bam_fd = pysam.Samfile(bam_fn, "rb")
        except ImportError:
            print >> sys.stderr, "Impossible to import pysam library"
            print >> sys.stderr, "The analysis won't take the bam file into account"
            
    try:            
        # parse snp file
        snp_fd = open(snp_fn,'r') 
        snps   = snp_file_parser(snp_fd)
        snp_fd.close()
        
        # parse genbank files -> retrieved annotations
        seqs = {}
        if feature_fn is not 'undef':
            if os.path.isdir(feature_fn):
                seqs = process_genbank_in_directory(feature_fn, ext='gbf')
            else:
                factory = GenbankFactory(feature_fn)
                seq     = factory.get_sequence()
                seqs[ seq.get_id() ] = seq

## DEBUG
                for seq in seqs.values():
                    for f in seq.get_features():
                        print f.get_feature_type(), f.get_start(), f.get_end(), f.get_strand()
                        print ''.join(translate(f.get_mother().cut_string(f.get_start(), f.get_end(), f.get_strand())))
                sys.exit(0)

##
        # make inter-features region 
        for seq in seqs.values():
            ints = make_intergenic_features(seq)
            [seq.add_feature(f) for f in ints]

        description, error_dict = make_description(snps, seqs, bam_fd)
        print >> output_fd, description
        output_fd.close()
        if len(error_dict.keys()):
            for key, val in error_dict.iteritems():
                print >> sys.stderr, "%s" % ( val)
    except :
        traceback.print_exc()
    
## entry point
if __name__ == '__main__':
    main()
