## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

""" Functions & classes related to fasta and fastq manipulation. """

__author__ = "ES&SDD"
__date__ = "Lun 31 auot 2009 15:07"
__version__ = "2"
__email__ = "sdeclere@gmail.com"

# ---------------------------------------------------------------------------
# DATA MODEL 
# ---------------------------------------------------------------------------

class FastqEntry:
    ''' This class gather informations about illumina's reads.  
    '''
    def __init__(self, ttl, seq, qual):
        self.name = ttl 
        self.sequence = seq
        self.quality = qual     

    def _ascii2qual(self, ascii):
        """Return quality from ascii.
        """
        return ord(ascii)-33

    def get_qualities(self):
        """ Return list of numerical qualities. 
        """
        return [self._ascii2qual(x) for x in self.quality]
    
    def __str__(self):
        return 'id=%s\t%s\t%s' % ( self.name, self.sequence, self.quality ) 

class FastaEntry:
    """ This class gather information about Fasta sequence. 
    """
    def __init__(self, ttl, seq):
        self.name = ttl 
        self.sequence = seq

    def __str__(self):
        return '>%s\n%s' % ( self.name, seq2fasta(self.sequence) ) 

# ---------------------------------------------------------------------------
# FUNCTIONS  
# ---------------------------------------------------------------------------

def eachfastq (fileobj):
    '''
    return a generator object that could iterate over a sequence of fastq formated entries.
    '''    
    
    ttl, seq, qual  = ( fileobj.readline().strip(),'', '')
    
    while True: 
        seq = fileobj.readline().strip()
        fileobj.readline()  # discard '+'
        qual = fileobj.readline().strip()
        next = fileobj.readline().strip()
        
        if next == '':
            break  
        else:
            yield FastqEntry(ttl, seq, qual)
            ttl=next
            
    yield FastqEntry(ttl, seq, qual)

def eachfasta(fileobj):
    '''
    return a generator object that could iterate over a sequence of fasta formated entries.
    This will load the whole the file into memory which can be a "very bad idea"(c) depending on the length of your sequence(s). 
    '''
    lines = fileobj.readlines() # load all im memory 
    seqs = ( ''.join(lines) ).split('>')
    seqs.pop(0)# remove first null element
    
    for s in seqs:
        elines=s.split('\n')
        title = elines[0].strip()
        seq = map(lambda x: str.strip(x), elines[1:]) 
        yield FastaEntry(title, ''.join(seq))

     
def seq2fasta (s):
    """ Raw string to fasta formated sequence.
    """
    res = []
    for cursor in range (0, len(s), 71):
        res.append(s[cursor:cursor + 71])   
    return '\n'.join(res)
