from Bio import GenBank
from fasta import seq2fasta
from seq_classes import DNASequence
import glob
import os
import sys
import time 
import pdb

## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

""" Functions & classes related to genbank files. """

__author__ = "ES&SDD"
__date__ = "Mer  9 dec 2009 17:10:32 CET"
__version__ = "1"
__email__ = "sdeclere@gmail.com"

# ---------------------------------------------------------------------------
# DATA MODEL 
# ---------------------------------------------------------------------------

class GenbankFactory:
    """Factory able to make sequence objects from genbank file
    """
    def __init__(self, filename):
        self._filename = filename 
        self._gb_handle = open (self._filename, 'r')
        self.seq = None

    def do_parse(self):
        parser = GenBank.FeatureParser() 
        record = parser.parse(self._gb_handle)   
        self.seq = DNASequence(record.annotations['organism'], str(record.id), str(record.seq)) 
        
    def get_sequence(self):
        if self.seq is None:
            self.do_parse()
        return self.seq

# ---------------------------------------------------------------------------
# FUNCTIONS  
# ---------------------------------------------------------------------------
def process_genbank_in_directory(dir_name, ext='gbk'):
    seqs = []
    for f in glob.glob('%s/*.%s' % (dir_name,ext)):
        sys.stderr.write ('Processing : %s \n' % f)
        factory = GenbankFactory(f)
        seqs.append(factory.get_sequence())
    return seqs

# ---------------------------------------------------------------------------
# TEST
# ---------------------------------------------------------------------------
if __name__ == "__main__":
    if os.path.isdir(sys.argv[1]):
        seqs = process_genbank_in_directory(sys.argv[1], ext='gbf')
    else:
        factory =  GenbankFactory(sys.argv[1])
        seqs = [factory.get_sequence()]
        
    for seq in seqs:
        id = seq.get_id()
        out = open('%s.fst' % id, 'w')
        out.write('>%s\n' % id)
        out.write(seq2fasta(seq.get_sequence()))
        out.close()
