## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

""" Misc classes and functions """

__author__  = "ES&SDD"
__date__    = "Mar  8 dec 2009 15:33:31 CET"
__version__ = "1"
__email__   = "sdeclere@gmail.com"

# ---------------------------------------------------------------------------
# DATA MODEL 
# ---------------------------------------------------------------------------

class Formatter (object):
    def __init__(self, format=''):
        self._format = format

    def format(self):
        raise NotImplementedError

# ---------------------------------------------------------------------------
# FUNCTIONS  
# ---------------------------------------------------------------------------

        

