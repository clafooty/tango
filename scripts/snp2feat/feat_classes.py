from seq_classes import Segment

## Copyright (C) 2004, 2005 Free Software Foundation
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.


__author__  = "ES&SDD"
__date__    = "Mar  8 dec 2009 15:33:31 CET"
__version__ = "1"
__email__   = "sdeclere@gmail.com"

# ---------------------------------------------------------------------------
# DATA MODEL 
# ---------------------------------------------------------------------------

## # unused code
## class UnStrandedFeature():
##     """Base class for all feature 
##     """
##     def __init__(self):
##         pass

        
class Feature(Segment): 
    """Base class for all simple features.
    """
    def __init__(self, start, end, strand, type_, mother,
                 source='undef', score='.'):
        Segment.__init__(self, start, end )
        self._mother  = mother  # it's a DNASequence
        self._strand  = strand
        self._type    = type_
        self._source  = source
        self._score   = score  
        self._annotations = {}
        self._id      = None
        
    def __str__(self):
        return '%s\t%s\t%d\t%d\t.\t%s\t.\t%s' % (self.get_source(),
                                                 self.get_feature_type(),
                                                 self.get_start(),
                                                 self.get_end(),
                                                 self.get_strand(),
                                                 str(self.get_annotations()))

    def set_id(self, identifier):
        self._id = identifier
        
    def get_id(self):
        return self._id
        
    def get_start(self):
        return self._start

    def set_start(self, start):
        self._start = start 

    def set_end(self, end):
        self._end = end 
        
    def get_end(self):
        return self._end

    def get_strand(self):
        return self._strand
    
    def get_mother(self):
        return self._mother
    
    def set_mother(self, mother):
        self._mother = mother
    
    def get_feature_sequence(self):
        seq = self.get_mother().cut_string(self._start, self._end,
                                          self.get_strand())
        return str(seq)
    
    def get_feature_type(self):
        return self._type
    
    def get_source(self):
        return self._source       

    def get_score(self):
        return self._score
    
    def set_annotations(self, dictionary):
        self._annotations = dictionary
        
    def get_annotations(self):
        return self._annotations
    
    def update_annotations(self, dictionary):
        self._annotations.update(dictionary)    
        
class CompositeFeature(Feature):
    """Composite features are features composed by a list of
    (possibly non contiguous) features.
    """
               
    def __init__(self, start, end, strand,
                 type_, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         type_, mother, source, score)
        self._feats = []
    
    def __str__(self):
        ret = [ Feature.__str__(self) ]
        for feat in self._feats:
            ret.append(str(feat))
        return '\n'.join(ret)
    
    def get_feature_sequence(self):
        """ Retrieves the sequence of the feature and
        complement-reverses it if necessary.
        If it's a composite feature, concatenates in the right order
        the sequences of the sub-features.
        """
        if self.have_children():
            ret = [ feat.get_feature_sequence() for feat in self.get_features() ]
            if self.get_strand() == '-':
                # since the feature are in the order of appearance
                # on the "+" strand, reverse them to start with the AUG codon
                ret.reverse()
            return ''.join(ret)     
        else:
            seq = self.get_mother().cut_string(self.get_start(), self.get_end(),
                                               self.get_strand())
        return str(seq) 
    
    def add_feature(self, ex):
        assert isinstance(ex, Feature)  
        self._feats.append(ex)

    def get_features(self):
        return self._feats
    
    def have_children(self):
        """ True if this composite feature have sub features.
        """
        return (len(self.get_features())>0)
    
class Chunk(Feature):
    """ Piece of feature """
    
    def __init__(self, start, end, strand,
                 type_,  mother, source='undef', score='.', part_of=None):
        Feature.__init__(self, start, end, strand,
                         type_, mother, source, score)        
        self._part_of = part_of
        
    def get_mother_feature(self):
        """ Get the master feature this chunk is part of """
        return self._part_of
    
    def set_mother_feature(self, feat):
        """ Set the master feature this chunk is part of """
        self._part_of = feat
        
# ---------------------------------------------------------------------------
#  INTRF 
# ---------------------------------------------------------------------------

class Producer:
    """Abstract object that could produce an other biological entity.  
    """
    
    def set_product(self, product):
        """ Abstract product setter
        to force implementation in the derived class """
        raise NotImplementedError
    
    def get_product(self):
        """ Abstract product getter
        to force implementation in the derived class """
        raise NotImplementedError
    
# ---------------------------------------------------------------------------
#  BIOLOGICAL OBJECTS 
# ---------------------------------------------------------------------------

## # unused code
## class Gene(CompositeFeature, Producer):
##     """Gene features (composite or not). 
##     """    
##     def __init__(self, start, end, strand, mother,
##                  source='undef', score='.'):
##         CompositeFeature.__init__(self, start, end, strand,
##                                   type_='gene', mother, source, score)
##         self._product = None 
        
##     def set_product(self, product):
##         self._product = product 
    
##     def get_product(self):
##         return self._product

class PseudoGene(CompositeFeature):
    """Gene features (composite or not). 
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        CompositeFeature.__init__(self, start, end, strand,
                                  'pseudogene', mother, source, score)

## # unused code        
## class Exon(Feature):
##     """Exons features.
##     """
##     def __init__(self, start, end, strand, mother,
##                  source='undef', score='.'):
##         Feature.__init__(self, start, end, strand,
##                          'exon', mother, source, score)
        
## class Intron(Feature):
##     """Introns features.
##     """
##     def __init__(self, start, end, strand, mother,
##                  source='undef', score='.'):
##         Feature.__init__(self, start, end, strand,
##                          'intron', mother, source, score)

class CDSChunk(Chunk):
    """Coding fragment of a coding gene.
    """
    def __init__(self, start, end, strand, mother,
                 source='undef', score='.', part_of=None):
        Chunk.__init__(self, start, end, strand,
                       'cds-chunk', mother, source, score, part_of) 
        
class CDS(CompositeFeature, Producer):
    """CDS features aka intron free gene.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        CompositeFeature.__init__(self, start, end, strand,
                                  'CDS', mother, source, score)
        self._product = None 
    
    def set_product(self, product):
        self._product = product 
    
    def get_product(self):
        return self._product
     

class RibosomalBindingSite (Feature):
    """Ribosomal binding site.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'RBS', mother, source, score)

class Terminator (Feature):
    """Transcription terminator.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'terminator', mother, source, score)
        
class ReplicationOrigin(Feature):
    """Origin of replications.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'rep_origin', mother, source, score)

class NonCodingRNA(Feature, Producer):
    """Non coding RNA.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'ncRNA', mother, source, score)
        self._product = None 

    def set_product(self, product):
        self._product = product 
    
    def get_product(self):
        return self._product

class TransferRNA(CompositeFeature, Producer):
    """transfer RNA.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        CompositeFeature.__init__(self, start, end, strand,
                                  'tRNA', mother, source, score)
        self._product = None 

    def set_product(self, product):
        self._product = product 
    
    def get_product(self):
        return self._product

class TRNAChunk(Chunk):
    """ Fragment of a transfer RNA.
    """
    def __init__(self, start, end, strand, mother,
                 source='undef', score='.', part_of=None):
        Chunk.__init__(self, start, end, strand,
                       'tRNA-chunk', mother, source, score, part_of)
        
    def get_coding_sequence(self):
        seq = self.get_mother().cut(self._start, self._end, self._strand)
        return str(seq)
        
class RibosomalRNA(Feature, Producer):
    """Ribosomal RNA.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'rRNA', mother, source, score)
        self._product = None 

    def set_product(self, product):
        self._product = product 
    
    def get_product(self):
        return self._product

class LongTerminalRepeat(Feature):
    """Long Terminal Repeats.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'LTR', mother, source, score)

class RepeatRegion(Feature):
    """Repeated Regions.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'repeat_region', mother, source, score)
        
class IntergenicRegion(Feature):
    """An intergenic Regions.
    """
    def __init__(self, start, end, strand, mother, source='undef', score='.'):
        Feature.__init__(self, start, end, strand,
                         'intergenic', mother, source, score)
        
# ---------------------------------------------------------------------------

