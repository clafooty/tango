#/bin/env python

'''
Script to read generate consensus sequence
'''

import sys, traceback, re

from optparse import OptionParser
from libmap_3 import MaqMappedRead, MappedBase, PositionBases
from mapping_functions import define_input_parser, maq_line_parser, parse_fasta_file, parse_window, set_maximum_range, split_dictionary, alloc_mapped_bases



# ---------------------------------------------------------------------------
# FUNCTIONS
# ---------------------------------------------------------------------------

# Function to read a SNP file
# Returns a dictionary containing the SNP alleles for all positions
def parse_snp_file(snp_fd):
    # Declare variables
    snp_alleles = {}
    # Read each line
    for line in snp_fd:
        line_elts = (line.strip()).split('\t')
        # Line starting with numbers
        if (re.search('^(\d+)', line_elts[0])):
            snp_alleles[int(line_elts[0])] = line_elts[2]
    return (snp_alleles)
    


# Function to loop on the mapping file and generate the consensus sequence
def make_consensus(mapping_fd, input_format, reference_sequence, window_size, read_length, output_format, snp_alleles, minimum_depth):
    
    # Declare variables
    mapped_bases_by_position = {}
    current_mapped_bases_by_position = {}
    consensus = []
    reference_length = len(reference_sequence)

    # Set the maximum range to parse the file by window
    max_range = set_maximum_range(reference_length, read_length, window_size)
    # Read and process mapping file by window
    for current_position in xrange(1, max_range, window_size):
        # Store information of mapped reads in a list of objects
        parsed_lines = parse_window(mapping_fd, input_format, current_position, window_size, reference_length)
        # Retrieve all the bases mapped at all positions of the window (plus read_length-1 positions) and store them in a dictionary
        [ alloc_mapped_bases(mapped_bases_by_position, mapped_read) for mapped_read in parsed_lines ]
        # Split the dictionary in the current one and the one to be used for the next window
        (current_mapped_bases_by_position, mapped_bases_by_position) = split_dictionary(mapped_bases_by_position, current_position, window_size)
        # Process the current dictionary and look for SNPs
        positions = [PositionBases (x, reference_sequence, read_length, current_mapped_bases_by_position[x]) for x in current_mapped_bases_by_position.keys()]
        
        consensus_output(positions, current_position, window_size, reference_sequence, snp_alleles, consensus, minimum_depth)
        # Free memory
        del current_mapped_bases_by_position
        sys.stdout.flush()
        # Print consensus sequence
        if output_format == 'fasta':
            print ''.join(consensus)
            consensus = []

    # Read and process the last portion of the mapping file.
    new_start = current_position + window_size
    for current_position in xrange(new_start, reference_length, window_size):
        # Split the dictionary in the current one and the one to be used for the next window
        (current_mapped_bases_by_position, mapped_bases_by_position) = split_dictionary(mapped_bases_by_position, current_position, window_size)
        # Process the current dictionary and look for SNPs
        positions = [PositionBases (x, reference_sequence, read_length, current_mapped_bases_by_position[x]) for x in current_mapped_bases_by_position.keys()]       

        consensus_output(positions, current_position, window_size, reference_sequence, snp_alleles, consensus, minimum_depth)
        # Free memory
        del current_mapped_bases_by_position
        sys.stdout.flush()
        # Print consensus sequence
        if output_format == 'fasta':
            print ''.join(consensus)
            consensus = []
            
    while (current_position + window_size) < reference_length:
        consensus.append('N')
        #print current_position
        current_position += 1
    # Print consensus sequence
    print ''.join(consensus)
    consensus = []



# ---------------------------------------------------------------------------
# OUTPUT
# ---------------------------------------------------------------------------
# Function to generate the consensus sequence
def consensus_output(positions, current_position, window_size, reference_sequence, snp_alleles, consensus, minimum_depth):
    # Define and initialise variables
    reference_length = len(reference_sequence)
    covered_base = 0
    max_range = 0
    # Set maximum range
    if (current_position + window_size) < reference_length:
        max_range = current_position + window_size
    else:
        max_range = reference_length + 1
    # Retrieve consensus base
    for position in xrange (current_position, max_range):
        covered_base = 0
        for base in positions:
            if position == base.position:
                if position in snp_alleles.keys():
                    consensus.append(snp_alleles[position])
                    covered_base = 1
                elif base.depth >= minimum_depth:
                    consensus.append(reference_sequence[position-1])
                    covered_base = 1
                else:
                    covered_base = 0
        if covered_base == 0:
            consensus.append('N')



# ---------------------------------------------------------------------------
# MAIN
# ---------------------------------------------------------------------------

def main(argv=None):

    # Define and initialise variables
    input_format, output_format, reference_file, mapping_file, minimum_depth, read_length, window_size = ('undef', 'undef', 'undef', 'undef', 0, 0, 0)
    reference_name = ''
    reference_length = 0
    
    # Object to parse the command line
    usage = "usage: %prog -i input_format -o output_format -r reference_file -m mapping_file -d minimum_depth -l read_length -s snp_file"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", "--input-format", dest="input_format", type="string", default='undef', help="Name of the format of the mapping file (maq)")
    parser.add_option("-o", "--output-format", dest="output_format", type="string", default='undef', help="Name of the output format (fasta)")
    parser.add_option("-r", "--reference-file", dest="reference_file", type="string", default='undef', help="Name of the file containing the reference sequence in fasta format")
    parser.add_option("-m", "--mapping-file", dest="mapping_file", type="string", default='undef', help="Name of the file containing the results of the mapping of reads (mapview format for MAQ)")
    parser.add_option("-s", "--snp-file", dest="snp_file", type="string", default='undef', help="Name of the tab delimited file containing the SNPs (the first field is the position and the third one is the mutation)")
    parser.add_option("-d", "--minimum-depth", dest="minimum_depth", type="int", default=0, help="Minimum depth for a position in the genome to be covered")
    parser.add_option("-l", "--read-length", dest="read_length", type="int", default=0, help="Length of the reads")
    parser.add_option("-w", "--window-size", dest="window_size", type="int", default=0, help="Size of the window used to read the mapping file, number of nucleotides displayed by line in the fasta file")
    # Read and parse the command line
    (options, args) = parser.parse_args()        
    input_format, output_format, reference_file, mapping_file, snp_file, minimum_depth, read_length, window_size = (options.input_format.lower(), options.output_format.lower(), options.reference_file, options.mapping_file, options.snp_file, options.minimum_depth, options.read_length, options.window_size)
    
    # Check that arguments are defined and correct
    if input_format is 'undef':
        parser.error("the input format is not defined")
    elif input_format != 'maq':
        parser.error("the input format does not exist")
    if output_format is 'undef':
        parser.error("the output format is not defined")
    elif output_format != 'fasta':
        parser.error("the output format does not exist")
    if reference_file is 'undef':
        parser.error("the file containing the reference sequence in fasta format is not defined")
    if mapping_file is 'undef':
        parser.error("the file containing the results of the mapping of reads is not defined")
    if snp_file is 'undef':
        parser.error("the file containing the SNPs is not defined")
    if minimum_depth == 0:
        parser.error("the minimum depth is not defined")
    if read_length == 0:
        parser.error("the length of the reads is not defined")
    if window_size == 0:
        parser.error("the size of the window is not defined")

    try:
        # Open the reference file
        try:
            reference_fd = open (reference_file, 'r')
        except IOError:
            print 'error: Impossible to open the file %s' % reference_file
            sys.exit(1)
        else:
            # Get the reference sequence
            reference_name, reference_sequence = parse_fasta_file(reference_fd)
            reference_length = len(reference_sequence)
        # Open the SNP file
        try:
            snp_fd = open (snp_file, 'r')
        except IOError:
            print 'error: Impossible to open the file %s' % snp_file
            sys.exit(1)
        else:
            # Get the SNPs position and allele
            snp_alleles = parse_snp_file(snp_fd)
        # Open the mapping file
        try:
            mapping_fd = open (mapping_file, 'r')
        except IOError:
            print 'error: Impossible to open the file %s' % mapping_file
            sys.exit(1)
        else:
            # Check that the size of the window is correct
            if window_size > reference_length:
                print "error: the window size is not correct, it must be smaller than the length of the reference sequence"
                sys.exit(2)
            else:
                # Print title line
                if output_format == 'fasta':
                    print ">Consensus of %s" % mapping_file
                # Go through the mapping file and look for SNPs
                make_consensus(mapping_fd, input_format, reference_sequence, window_size, read_length, output_format, snp_alleles, minimum_depth)

        # Close input files
        reference_fd.close()
        snp_fd.close()
        mapping_fd.close()
        
    except:
        traceback.print_exc()

## entry point
if __name__ == '__main__':
    main()
