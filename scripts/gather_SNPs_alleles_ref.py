#!/usr/bin/env python

"""
Generate a file containing the alleles of all strains at all SNPs positions. If a SNP position is not covered in a strain, the allele is set to N or the SNP is discarded, depending on the -N option.
Inputs: -f File containing the depth of all strains at all SNPs positions
        -r File containing the reference sequence in fasta
        -d Directory containing the output files of the script call-snp.py (whatever version), one file per strain
        -N 'y' or 'n' depending whether positions with Ns should be considered or discarded
"""

import getopt, sys, traceback, os, re


# ---------------------------------------------------------------------------
# FUNCTIONS
# ---------------------------------------------------------------------------

# Function to print help message
def usage():
    '''
    Print a short help message, then exit.
    '''
    print '\n-h\thelp\n-f\tFile with coverage at SNPs positions by strain\n-r\tFile containing the reference sequence in fasta\n-d\tInput directory containing one file per strain with detected SNPs\n-N\ty or n depending whether positions with Ns should be considered or discarded\n\n'
    sys.exit(1)


# Function to parse the command line
def parse_cmd_line(argv):

    # Initialise variables
    depth_file = 'undef'
    reference_file = 'undef'
    snp_dir = 'undef'
    print_Ns = 'undef'

    # Get arguments or exit and print help message if it fails
    try:
        options, arguments = getopt.getopt(argv, "hf:r:d:N:", ["help", "depth_file=", "reference_file=", "snp_directory=", "print_Ns="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    # Store arguments in variables
    for option, argument in options:
        if option in ("-h", "--help"):
            usage()                     
        elif option in ("-f", "--depth_file"): 
            depth_file = argument
        elif option in ("-r", "--reference_file"): 
            reference_file = argument
        elif option in ("-d", "--snp_directory"): 
            snp_dir = argument
        elif option in ("-N", "--print_Ns"): 
            print_Ns = argument.strip()

    # Return arguments
    return ( depth_file, reference_file, snp_dir, print_Ns )


# Function to parse the file containing the depth of the strain at all SNPs positions
def parse_depth_file(depth_fd):

   # Declare vairables
   depth_positions = {}
   count_strains = 0
   index = 0
   strain_nb = {}
   position = 0

   # Open and parse depth file
   for line in depth_fd:
      line_elts = (line.strip()).split('\t')
      # Line does not contain depth but strains names
      if not (re.search('^(\d+)', line_elts[0])):
         count_strains = len(line_elts) - 1
         # Associate a number to each strain
         for index in range (1, count_strains+1):
            strain_nb[index] = line_elts[index]
      # Line contains depths
      else:
         position = line_elts[0]
         # Store the depth in a dictionary with a key that is a tuple strain-SNP position
         for index in range (1, count_strains+1):
            depth_positions[(strain_nb[index], int(position))] = int(line_elts[index])
   # Return a dictionary containing the depth for each tuple strain-SNP position
   return (depth_positions)


# Function to parse the file containing the depth of the strain at all SNPs positions
def parse_fasta_file(reference_fd):

   # Declare vairables
   reference = []

   # Open and parse reference file
   for line in reference_fd:
       if not (re.search('^>', line)):
           #print reference
           #line_elts = (line.strip()).split('')
           reference.extend(list(line.strip()))
           #print reference[0]
           #print reference[10]
   # Return a list containing the reference sequence
   return (reference)



# ---------------------------------------------------------------------------
# MAIN
# ---------------------------------------------------------------------------

def main(argv=None):
      
   # Read and parse system's command line 
   depth_file, reference_file, snp_dir, print_Ns =  parse_cmd_line( sys.argv[1:] )
    
   # Check that arguments are defined and correct
   if ( (depth_file is 'undef') or (reference_file is 'undef') or (snp_dir is 'undef') or (print_Ns is 'undef') ):
      usage()
      sys.exit(2)
   if not ( (print_Ns is 'y') or (print_Ns is 'n') ):
      usage()
      sys.exit(2)

   try:
      # Define variables.
      depth_positions = {}
      snp_positions = {}
      ref_base = {}
      all_strains = []

      # Get the depth of all strains at SNPs positions
      depth_fd = open (depth_file, 'r')
      depth_positions = parse_depth_file(depth_fd)
      # Get the reference sequence
      reference_fd = open (reference_file, 'r')
      reference = parse_fasta_file(reference_fd)      
      
      # Go through the input directory
      for snp_file in os.listdir(snp_dir):
          # Process all SNP files (files with the extension 'snp'
          if (re.search('.snp' ,snp_file)):
              # Create a list with the name of all the strains
              strain = snp_file.split('_')[0]
              all_strains.append(strain)
              # Get SNPs alleles
              # Parse SNPs files
              snp_fd = open ( ('%s/%s' % (snp_dir, snp_file)), 'r' )
              for line in snp_fd:
                  line_elts = (line.strip()).split('\t')
                  # Line to uncomment for genes
                  #line_elts = (line.strip()).split(' ')
                  # Select lines containing SNPs (starting by position)
                  if (re.search('^(\d+)', line_elts[0])):
                      position = int(line_elts[0])
                      ref_base[position] = line_elts[1]
                      mutation = line_elts[2]
                      # Store strain name and mutation (as a tuple) in a disctionary
                      try:
                      ## () defines a list that can't be modified
                          (snp_positions[position]).append( (strain, mutation) )
                      except KeyError:
                          snp_positions[position] = [(strain, mutation)]

      # Print header
      sorted_all_strains = sorted(all_strains)
      print '%s\t%s\t%s' % ('position' , '\t'.join(sorted_all_strains), 'Ref')
      
      # Create dictionary containing a nucleotide for all SNPs positions
      for pos in sorted (snp_positions.keys()):
         allele = {}
         contain_Ns = 0
         # Assign the mutation to all polymorphic strains
         for value in snp_positions[pos]:
             allele[value[0]] = value[1]
         # Check all strains
         for key_strain in sorted_all_strains:
             # The strain is not polymorphic at the considered position
             if not (key_strain in  allele):
                 # The considered position is not enough covered to detect a SNP, assign a 'N'
                 if (depth_positions[(key_strain, pos)] < 3):
                     allele[key_strain] = 'N'
                     contain_Ns = 1
                 # The considered position is enough covered to detect a SNP, assign the reference base
                 else:
                     allele[key_strain] = ref_base[pos]

         # Print alleles
         if (print_Ns is 'y'):
             print '%d\t%s\t%s' % ( pos, '\t'.join ([ allele[key_strain] for key_strain in sorted_all_strains ]), reference[pos-1] )
         elif (print_Ns is 'n'):
             if (contain_Ns != 1):
                 print '%d\t%s\t%s' % ( pos, '\t'.join ([ allele[key_strain] for key_strain in sorted_all_strains ]), reference[pos-1] )

      snp_fd.close()
      depth_fd.close()
   except:
      traceback.print_exc()

if __name__ == "__main__":
   main ()
